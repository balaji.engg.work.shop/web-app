import './App.css';
import { ChakraProvider } from '@chakra-ui/react';
import { RouterPage } from './routes';
import { GoogleOAuthProvider } from '@react-oauth/google';

function App() {
  return (
    <GoogleOAuthProvider clientId="509348548420-rrkqee9s78ke31j5lsfv95be38mvfgoe.apps.googleusercontent.com">
      <ChakraProvider>
        <RouterPage />
      </ChakraProvider>
    </GoogleOAuthProvider>
  );
}

export default App;

import Axios from 'axios';
import { getAuthorization } from '../utils/axiosUtils';
import {
  ADD_NEW_BANK_ACCOUNT_FAIL,
  ADD_NEW_BANK_ACCOUNT_REQUEST,
  ADD_NEW_BANK_ACCOUNT_SUCCESS,
  DELETE_BANK_ACCOUNT_FAIL,
  DELETE_BANK_ACCOUNT_REQUEST,
  DELETE_BANK_ACCOUNT_SUCCESS,
  GET_ALL_BANK_ACCOUNT_FAIL,
  GET_ALL_BANK_ACCOUNT_REQUEST,
  GET_ALL_BANK_ACCOUNT_SUCCESS,
  GET_BANK_ACCOUNT_DETAILS_FAIL,
  GET_BANK_ACCOUNT_DETAILS_REQUEST,
  GET_BANK_ACCOUNT_DETAILS_SUCCESS,
  UPDATE_BANK_ACCOUNT_FAIL,
  UPDATE_BANK_ACCOUNT_REQUEST,
  UPDATE_BANK_ACCOUNT_SUCCESS
} from '../constants/bankAccountConstants';

const BASE_URL = `${process.env.REACT_APP_BLINDS_SERVER}/api/v1/bankAccount`;

export const createNewBankAccount = (bankAccount) => async (dispatch, getState) => {
  dispatch({
    type: ADD_NEW_BANK_ACCOUNT_REQUEST,
    payload: bankAccount
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.post(
      `${BASE_URL}/create`,
      bankAccount,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: ADD_NEW_BANK_ACCOUNT_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: ADD_NEW_BANK_ACCOUNT_FAIL,
      payload: message
    });
  }
};

export const getAllBankAccount = () => async (dispatch, getState) => {
  dispatch({
    type: GET_ALL_BANK_ACCOUNT_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/bankAccountList`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_ALL_BANK_ACCOUNT_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_ALL_BANK_ACCOUNT_FAIL,
      payload: message
    });
  }
};

export const getBankAccountDetails = (bankAccountId) => async (dispatch, getState) => {
  dispatch({
    type: GET_BANK_ACCOUNT_DETAILS_REQUEST,
    payload: bankAccountId
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();

    const { data } = await Axios.get(
      `${BASE_URL}/details?bankAccountId=${bankAccountId}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_BANK_ACCOUNT_DETAILS_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_BANK_ACCOUNT_DETAILS_FAIL,
      payload: message
    });
  }
};

export const updateBankAccount = (bankAccount) => async (dispatch, getState) => {
  dispatch({
    type: UPDATE_BANK_ACCOUNT_REQUEST,
    payload: bankAccount
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();

    const { data } = await Axios.put(
      `${BASE_URL}/update?bankAccountId=${bankAccount?.bankAccountId}`,
      bankAccount,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: UPDATE_BANK_ACCOUNT_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: UPDATE_BANK_ACCOUNT_FAIL,
      payload: message
    });
  }
};

export const deleteBankAccount = (bankAccountId) => async (dispatch, getState) => {
  dispatch({
    type: DELETE_BANK_ACCOUNT_REQUEST,
    payload: bankAccountId
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();

    const { data } = await Axios.delete(
      `${BASE_URL}/delete?bankAccountId=${bankAccountId}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: DELETE_BANK_ACCOUNT_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: DELETE_BANK_ACCOUNT_FAIL,
      payload: message
    });
  }
};

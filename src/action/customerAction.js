import Axios from 'axios';
import {
  CREATE_NEW_CUSTOMER_FAIL,
  CREATE_NEW_CUSTOMER_REQUEST,
  CREATE_NEW_CUSTOMER_SUCCESS,
  DELETE_CUSTOMER_FAIL,
  DELETE_CUSTOMER_REQUEST,
  DELETE_CUSTOMER_SUCCESS,
  GET_ALL_CUSTOMER_FAIL,
  GET_ALL_CUSTOMER_REQUEST,
  GET_ALL_CUSTOMER_SUCCESS,
  GET_CUSTOMER_FAIL,
  GET_CUSTOMER_REQUEST,
  GET_CUSTOMER_SUCCESS,
  UPDATE_CUSTOMER_FAIL,
  UPDATE_CUSTOMER_REQUEST,
  UPDATE_CUSTOMER_SUCCESS
} from '../constants/customerConstants';
import { getAuthorization } from '../utils/axiosUtils';

const BASE_URL = `${process.env.REACT_APP_BLINDS_SERVER}/api/v1/customers`;

export const createNewCustomer = (customer) => async (dispatch, getState) => {
  dispatch({
    type: CREATE_NEW_CUSTOMER_REQUEST,
    payload: customer
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.post(
      `${BASE_URL}/create`,
      customer,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: CREATE_NEW_CUSTOMER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: CREATE_NEW_CUSTOMER_FAIL,
      payload: message
    });
  }
};

export const updateCustomer = (customer) => async (dispatch, getState) => {
  dispatch({
    type: UPDATE_CUSTOMER_REQUEST,
    payload: customer
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.put(
      `${BASE_URL}/update?customerId=${customer?.customerId}`,
      customer,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: UPDATE_CUSTOMER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: UPDATE_CUSTOMER_FAIL,
      payload: message
    });
  }
};

export const getAllCustomers = () => async (dispatch, getState) => {
  dispatch({
    type: GET_ALL_CUSTOMER_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(`${BASE_URL}/all`, getAuthorization(userInfo.token));
    dispatch({
      type: GET_ALL_CUSTOMER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_ALL_CUSTOMER_FAIL,
      payload: message
    });
  }
};

export const deleteCustomer = (customerId) => async (dispatch, getState) => {
  dispatch({
    type: DELETE_CUSTOMER_REQUEST,
    payload: { customerId }
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.delete(
      `${BASE_URL}/delete?customerId=${customerId}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: DELETE_CUSTOMER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: DELETE_CUSTOMER_FAIL,
      payload: message
    });
  }
};

export const getCustomerDetails = (mobileNo) => async (dispatch, getState) => {
  dispatch({
    type: GET_CUSTOMER_REQUEST,
    payload: { mobileNo }
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/details?mobileNo=${mobileNo}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_CUSTOMER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_CUSTOMER_FAIL,
      payload: message
    });
  }
};

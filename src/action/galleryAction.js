import Axios from 'axios';
import {
  CREATE_NEW_GALLERY_FAIL,
  CREATE_NEW_GALLERY_REQUEST,
  CREATE_NEW_GALLERY_SUCCESS,
  GALLERY_CATEGORY_LIST_FAIL,
  GALLERY_CATEGORY_LIST_REQUEST,
  GALLERY_CATEGORY_LIST_SUCCESS,
  GET_ALL_GALLERY_FAIL,
  GET_ALL_GALLERY_REQUEST,
  GET_ALL_GALLERY_SUCCESS,
  GET_GALLERY_BY_SHOP_ID_FAIL,
  GET_GALLERY_BY_SHOP_ID_REQUEST,
  GET_GALLERY_BY_SHOP_ID_SUCCESS,
  UPDATE_GALLERY_FAIL,
  UPDATE_GALLERY_REQUEST,
  UPDATE_GALLERY_SUCCESS
} from '../constants/galleryConstants';
import { getAuthorization } from '../utils/axiosUtils';

const BASE_URL = `${process.env.REACT_APP_BLINDS_SERVER}/api/v1/gallery`;

export const createNewGallery = (gallery) => async (dispatch, getState) => {
  dispatch({
    type: CREATE_NEW_GALLERY_REQUEST,
    payload: gallery
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.post(
      `${BASE_URL}/create`,
      gallery,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: CREATE_NEW_GALLERY_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: CREATE_NEW_GALLERY_FAIL,
      payload: message
    });
  }
};

export const updateGallery = (gallery) => async (dispatch, getState) => {
  dispatch({
    type: UPDATE_GALLERY_REQUEST,
    payload: gallery
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.put(
      `${BASE_URL}/update`,
      gallery,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: UPDATE_GALLERY_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: UPDATE_GALLERY_FAIL,
      payload: message
    });
  }
};

export const getAllGallery = (reqParams) => async (dispatch, getState) => {
  dispatch({
    type: GET_ALL_GALLERY_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/all`,
      reqParams,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_ALL_GALLERY_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_ALL_GALLERY_FAIL,
      payload: message
    });
  }
};

export const getGalleryListByShopId = (shopId) => async (dispatch, getState) => {
  dispatch({
    type: GET_GALLERY_BY_SHOP_ID_REQUEST,
    payload: { shopId }
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/galleryListByShopId?shopId=${shopId}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_GALLERY_BY_SHOP_ID_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_GALLERY_BY_SHOP_ID_FAIL,
      payload: message
    });
  }
};

export const getGalleryCategory = (shopId) => async (dispatch, getState) => {
  dispatch({
    type: GALLERY_CATEGORY_LIST_REQUEST,
    payload: { shopId }
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/galleryCategoryList`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GALLERY_CATEGORY_LIST_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GALLERY_CATEGORY_LIST_FAIL,
      payload: message
    });
  }
};

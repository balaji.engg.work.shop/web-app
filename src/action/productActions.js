import {
  ADD_DAILY_MANUFACTURE_FAIL,
  ADD_DAILY_MANUFACTURE_REQUEST,
  ADD_DAILY_MANUFACTURE_SUCCESS,
  ADD_PRODUCTS_IN_BULK_FAIL,
  ADD_PRODUCTS_IN_BULK_REQUEST,
  ADD_PRODUCTS_IN_BULK_SUCCESS,
  CREATE_NEW_PRODUCT_FAIL,
  CREATE_NEW_PRODUCT_REQUEST,
  CREATE_NEW_PRODUCT_SUCCESS,
  DELETE_ALL_PRODUCT_FAIL,
  DELETE_ALL_PRODUCT_REQUEST,
  DELETE_ALL_PRODUCT_SUCCESS,
  DELETE_PRODUCT_FAIL,
  DELETE_PRODUCT_REQUEST,
  DELETE_PRODUCT_SUCCESS,
  GET_ALL_PRODUCT_FAIL,
  GET_ALL_PRODUCT_REQUEST,
  GET_ALL_PRODUCT_SUCCESS,
  GET_DAILY_MANUFACTURE_FAIL,
  GET_DAILY_MANUFACTURE_REQUEST,
  GET_DAILY_MANUFACTURE_SUCCESS,
  GET_PRODUCT_FAIL,
  GET_PRODUCT_REQUEST,
  GET_PRODUCT_SUCCESS,
  UPDATE_PRODUCT_FAIL,
  UPDATE_PRODUCT_REQUEST,
  UPDATE_PRODUCT_SUCCESS
} from '../constants/productConstants';
import Axios from 'axios';
import { getAuthorization } from '../utils/axiosUtils';

const BASE_URL = `${process.env.REACT_APP_BLINDS_SERVER}/api/v1/products`;

export const createNewProduct = (product) => async (dispatch, getState) => {
  dispatch({
    type: CREATE_NEW_PRODUCT_REQUEST,
    payload: product
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.post(
      `${BASE_URL}/create`,
      product,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: CREATE_NEW_PRODUCT_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: CREATE_NEW_PRODUCT_FAIL,
      payload: message
    });
  }
};

export const getAllProduct = () => async (dispatch, getState) => {
  dispatch({
    type: GET_ALL_PRODUCT_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/productListByShopId?shopId=${userInfo.shopId}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_ALL_PRODUCT_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_ALL_PRODUCT_FAIL,
      payload: message
    });
  }
};

export const getProductDetails = (productId) => async (dispatch, getState) => {
  dispatch({
    type: GET_PRODUCT_REQUEST,
    payload: productId
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();

    const { data } = await Axios.get(
      `${BASE_URL}/details/${productId}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_PRODUCT_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_PRODUCT_FAIL,
      payload: message
    });
  }
};

export const updateProduct = (product) => async (dispatch, getState) => {
  dispatch({
    type: UPDATE_PRODUCT_REQUEST,
    payload: product
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();

    const { data } = await Axios.put(
      `${BASE_URL}/update?productId=${product?._id}`,
      product,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: UPDATE_PRODUCT_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: UPDATE_PRODUCT_FAIL,
      payload: message
    });
  }
};

export const deleteProduct = (productId) => async (dispatch, getState) => {
  dispatch({
    type: DELETE_PRODUCT_REQUEST,
    payload: productId
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();

    const { data } = await Axios.delete(
      `${BASE_URL}/delete?productId=${productId}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: DELETE_PRODUCT_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: DELETE_PRODUCT_FAIL,
      payload: message
    });
  }
};

export const deleteAllProducts = () => async (dispatch, getState) => {
  dispatch({
    type: DELETE_ALL_PRODUCT_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();

    const { data } = await Axios.delete(`${BASE_URL}/deleteAll`, getAuthorization(userInfo.token));
    dispatch({
      type: DELETE_ALL_PRODUCT_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: DELETE_ALL_PRODUCT_FAIL,
      payload: message
    });
  }
};

export const addProductInBulk = (products) => async (dispatch, getState) => {
  dispatch({
    type: ADD_PRODUCTS_IN_BULK_REQUEST,
    payload: products
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.post(
      `${BASE_URL}/addInBulk`,
      products,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: ADD_PRODUCTS_IN_BULK_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: ADD_PRODUCTS_IN_BULK_FAIL,
      payload: message
    });
  }
};

export const addDailyManufactureProductInBulk = (requestBody) => async (dispatch, getState) => {
  dispatch({
    type: ADD_DAILY_MANUFACTURE_REQUEST,
    payload: requestBody
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.put(
      `${BASE_URL}/dailyManufacture`,
      requestBody,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: ADD_DAILY_MANUFACTURE_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: ADD_DAILY_MANUFACTURE_FAIL,
      payload: message
    });
  }
};

export const getDailyManufactureProductList = () => async (dispatch, getState) => {
  dispatch({
    type: GET_DAILY_MANUFACTURE_REQUEST,
    payload: ''
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/dailyManufactureList`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_DAILY_MANUFACTURE_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_DAILY_MANUFACTURE_FAIL,
      payload: message
    });
  }
};

import Axios from 'axios';
import { getAuthorization } from '../utils/axiosUtils';
import {
  ADD_NEW_PRODUCT_CATEGORY_FAIL,
  ADD_NEW_PRODUCT_CATEGORY_REQUEST,
  ADD_NEW_PRODUCT_CATEGORY_SUCCESS,
  GET_PRODUCT_CATEGORY_FAIL,
  GET_PRODUCT_CATEGORY_REQUEST,
  GET_PRODUCT_CATEGORY_SUCCESS
} from '../constants/productCategoryConstant';

const BASE_URL = `${process.env.REACT_APP_BLINDS_SERVER}/api/v1/productCategory`;

export const addNewProductCategory = (requestBody) => async (dispatch, getState) => {
  dispatch({
    type: ADD_NEW_PRODUCT_CATEGORY_REQUEST,
    payload: requestBody
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.post(
      `${BASE_URL}/create`,
      requestBody,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: ADD_NEW_PRODUCT_CATEGORY_SUCCESS,
      payload: data
    });
    dispatch({
      type: GET_PRODUCT_CATEGORY_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: ADD_NEW_PRODUCT_CATEGORY_FAIL,
      payload: message
    });
  }
};

export const getAllProductCategory = () => async (dispatch, getState) => {
  dispatch({
    type: GET_PRODUCT_CATEGORY_REQUEST,
    payload: ''
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(`${BASE_URL}`, getAuthorization(userInfo.token));
    dispatch({
      type: GET_PRODUCT_CATEGORY_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_PRODUCT_CATEGORY_FAIL,
      payload: message
    });
  }
};

import Axios from 'axios';
import {
  ADD_NEW_PURCHASE_PRODUCTS_DATA_FAIL,
  ADD_NEW_PURCHASE_PRODUCTS_DATA_REQUEST,
  ADD_NEW_PURCHASE_PRODUCTS_DATA_SUCCESS,
  GET_PURCHASE_PRODUCTS_DETAILS_FAIL,
  GET_PURCHASE_PRODUCTS_DETAILS_REQUEST,
  GET_PURCHASE_PRODUCTS_DETAILS_SUCCESS
} from '../constants/purchaseProductConstants';
import { getAuthorization } from '../utils/axiosUtils';

const BASE_URL = `${process.env.REACT_APP_BLINDS_SERVER}/api/v1/purchaseBill`;

export const addNewPurchaseProducts = (requestBody) => async (dispatch, getState) => {
  dispatch({
    type: ADD_NEW_PURCHASE_PRODUCTS_DATA_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.post(
      `${BASE_URL}/create`,
      requestBody,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: ADD_NEW_PURCHASE_PRODUCTS_DATA_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: ADD_NEW_PURCHASE_PRODUCTS_DATA_FAIL,
      payload: message
    });
  }
};

export const getPurchaseProductsDetails = (billNo) => async (dispatch, getState) => {
  dispatch({
    type: GET_PURCHASE_PRODUCTS_DETAILS_REQUEST,
    payload: { billNo }
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/purchaseDetails?billNo=${billNo}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_PURCHASE_PRODUCTS_DETAILS_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_PURCHASE_PRODUCTS_DETAILS_FAIL,
      payload: message
    });
  }
};

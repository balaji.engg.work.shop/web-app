import Axios from 'axios';
import {
  ADD_NEW_SELL_PRODUCTS_DATA_FAIL,
  ADD_NEW_SELL_PRODUCTS_DATA_REQUEST,
  ADD_NEW_SELL_PRODUCTS_DATA_SUCCESS,
  DELETE_SELL_PRODUCTS_DETAIL_FAIL,
  DELETE_SELL_PRODUCTS_DETAIL_REQUEST,
  DELETE_SELL_PRODUCTS_DETAIL_SUCCESS,
  GET_COUNT_DATA_FAIL,
  GET_COUNT_DATA_REQUEST,
  GET_COUNT_DATA_SUCCESS,
  GET_SELL_PRODUCTS_DETAIL_FAIL,
  GET_SELL_PRODUCTS_DETAIL_REQUEST,
  GET_SELL_PRODUCTS_DETAIL_SUCCESS
} from '../constants/sellProductConstants';
import { getAuthorization } from '../utils/axiosUtils';

const BASE_URL = `${process.env.REACT_APP_BLINDS_SERVER}/api/v1/bill`;

export const addNewSellProducts = (requestBody) => async (dispatch, getState) => {
  dispatch({
    type: ADD_NEW_SELL_PRODUCTS_DATA_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.post(
      `${BASE_URL}/create`,
      requestBody,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: ADD_NEW_SELL_PRODUCTS_DATA_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: ADD_NEW_SELL_PRODUCTS_DATA_FAIL,
      payload: message
    });
  }
};

export const getCountData = () => async (dispatch, getState) => {
  dispatch({
    type: GET_COUNT_DATA_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(`${BASE_URL}/getCount`, getAuthorization(userInfo.token));
    dispatch({
      type: GET_COUNT_DATA_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_COUNT_DATA_FAIL,
      payload: message
    });
  }
};

export const getSellProductDetails = (billNo) => async (dispatch, getState) => {
  dispatch({
    type: GET_SELL_PRODUCTS_DETAIL_REQUEST,
    payload: { billNo }
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/billDetails?billNo=${billNo}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_SELL_PRODUCTS_DETAIL_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_SELL_PRODUCTS_DETAIL_FAIL,
      payload: message
    });
  }
};

export const deleteSellProductDetails = (transactionId) => async (dispatch, getState) => {
  dispatch({
    type: DELETE_SELL_PRODUCTS_DETAIL_REQUEST,
    payload: { transactionId }
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.delete(
      `${BASE_URL}/deleteSellBill?transactionId=${transactionId}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: DELETE_SELL_PRODUCTS_DETAIL_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: DELETE_SELL_PRODUCTS_DETAIL_FAIL,
      payload: message
    });
  }
};

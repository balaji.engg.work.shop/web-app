import Axios from 'axios';
import { getAuthorization } from '../utils/axiosUtils';
import {
  CREATE_NEW_SELL_RETURN_FAIL,
  CREATE_NEW_SELL_RETURN_REQUEST,
  CREATE_NEW_SELL_RETURN_SUCCESS,
  GET_SELL_RETURN_DETAILS_FAIL,
  GET_SELL_RETURN_DETAILS_REQUEST,
  GET_SELL_RETURN_DETAILS_SUCCESS
} from '../constants/sellReturnConstants';

const BASE_URL = `${process.env.REACT_APP_BLINDS_SERVER}/api/v1/sellReturn`;

export const createNewSellReturnAction = (input) => async (dispatch, getState) => {
  dispatch({
    type: CREATE_NEW_SELL_RETURN_REQUEST,
    payload: input
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.post(
      `${BASE_URL}/addProductsReturn`,
      input,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: CREATE_NEW_SELL_RETURN_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: CREATE_NEW_SELL_RETURN_FAIL,
      payload: message
    });
  }
};

export const getSellReturnDetails = (billNo) => async (dispatch, getState) => {
  dispatch({
    type: GET_SELL_RETURN_DETAILS_REQUEST,
    payload: { billNo }
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/getSellReturnDetails?billNo=${billNo}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_SELL_RETURN_DETAILS_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_SELL_RETURN_DETAILS_FAIL,
      payload: message
    });
  }
};

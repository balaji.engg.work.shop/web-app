import Axios from 'axios';
import {
  CREATE_NEW_SHOP_FAIL,
  CREATE_NEW_SHOP_REQUEST,
  CREATE_NEW_SHOP_SUCCESS,
  SHOP_DETAILS_FAIL,
  SHOP_DETAILS_REQUEST,
  SHOP_DETAILS_SUCCESS,
  UPDATE_SHOP_DETAILS_REQUEST,
  UPDATE_SHOP_DETAILS_RESET,
  UPDATE_SHOP_DETAILS_SUCCESS
} from '../constants/shopConstants';
import { getAuthorization } from '../utils/axiosUtils';

const BASE_URL = `${process.env.REACT_APP_BLINDS_SERVER}/api/v1/shops`;

export const createNewShop = (shopDetails) => async (dispatch) => {
  dispatch({
    type: CREATE_NEW_SHOP_REQUEST,
    payload: shopDetails
  });

  try {
    const { data } = await Axios.post(`${BASE_URL}/create`, shopDetails);
    dispatch({
      type: CREATE_NEW_SHOP_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: CREATE_NEW_SHOP_FAIL,
      payload: message
    });
  }
};

export const getShopDetails = () => async (dispatch, getState) => {
  dispatch({
    type: SHOP_DETAILS_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(`${BASE_URL}`, getAuthorization(userInfo.token));
    dispatch({
      type: SHOP_DETAILS_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: SHOP_DETAILS_FAIL,
      payload: message
    });
  }
};

export const updateShopDetails = (shopDetails) => async (dispatch, getState) => {
  dispatch({
    type: UPDATE_SHOP_DETAILS_REQUEST,
    payload: shopDetails
  });
  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.put(
      `${BASE_URL}/update`,
      shopDetails,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: UPDATE_SHOP_DETAILS_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: UPDATE_SHOP_DETAILS_RESET,
      payload: message
    });
  }
};

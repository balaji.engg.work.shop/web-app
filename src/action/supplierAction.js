import Axios from 'axios';
import {
  CREATE_NEW_SUPPLIER_FAIL,
  CREATE_NEW_SUPPLIER_REQUEST,
  CREATE_NEW_SUPPLIER_SUCCESS,
  GET_ALL_SUPPLIER_FAIL,
  GET_ALL_SUPPLIER_REQUEST,
  GET_ALL_SUPPLIER_SUCCESS,
  GET_SUPPLIER_FAIL,
  GET_SUPPLIER_REQUEST,
  GET_SUPPLIER_SUCCESS,
  UPDATE_SUPPLIER_FAIL,
  UPDATE_SUPPLIER_REQUEST,
  UPDATE_SUPPLIER_SUCCESS
} from '../constants/supplierConstants';
import { getAuthorization } from '../utils/axiosUtils';

const BASE_URL = `${process.env.REACT_APP_BLINDS_SERVER}/api/v1/suppliers`;

export const createNewSupplier = (supplier) => async (dispatch, getState) => {
  dispatch({
    type: CREATE_NEW_SUPPLIER_REQUEST,
    payload: supplier
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.post(
      `${BASE_URL}/create`,
      supplier,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: CREATE_NEW_SUPPLIER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: CREATE_NEW_SUPPLIER_FAIL,
      payload: message
    });
  }
};

export const updateSupplier = (supplier) => async (dispatch, getState) => {
  dispatch({
    type: UPDATE_SUPPLIER_REQUEST,
    payload: supplier
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.put(
      `${BASE_URL}/update?supplierId=${supplier?.supplierId}`,
      supplier,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: UPDATE_SUPPLIER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: UPDATE_SUPPLIER_FAIL,
      payload: message
    });
  }
};

export const getAllSuppliers = () => async (dispatch, getState) => {
  dispatch({
    type: GET_ALL_SUPPLIER_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(`${BASE_URL}/all`, getAuthorization(userInfo.token));
    dispatch({
      type: GET_ALL_SUPPLIER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_ALL_SUPPLIER_FAIL,
      payload: message
    });
  }
};

export const getSupplierDetails = (mobileNo) => async (dispatch, getState) => {
  dispatch({
    type: GET_SUPPLIER_REQUEST,
    payload: { mobileNo }
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/details?mobileNo=${mobileNo}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_SUPPLIER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_SUPPLIER_FAIL,
      payload: message
    });
  }
};

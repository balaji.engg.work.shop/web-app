import Axios from 'axios';
import {
  GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_FAIL,
  GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_REQUEST,
  GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_SUCCESS,
  GET_CUSTOMER_TRANSACTION_LIST_FAIL,
  GET_CUSTOMER_TRANSACTION_LIST_REQUEST,
  GET_CUSTOMER_TRANSACTION_LIST_SUCCESS,
  GET_PURCHASE_TRANSACTION_LIST_FAIL,
  GET_PURCHASE_TRANSACTION_LIST_REQUEST,
  GET_PURCHASE_TRANSACTION_LIST_SUCCESS,
  GET_SELL_RETURN_TRANSACTION_LIST_FAIL,
  GET_SELL_RETURN_TRANSACTION_LIST_REQUEST,
  GET_SELL_RETURN_TRANSACTION_LIST_SUCCESS,
  GET_SUPPLIER_TRANSACTION_LIST_FAIL,
  GET_SUPPLIER_TRANSACTION_LIST_REQUEST,
  GET_SUPPLIER_TRANSACTION_LIST_SUCCESS,
  GET_TRANSACTION_LIST_FAIL,
  GET_TRANSACTION_LIST_REQUEST,
  GET_TRANSACTION_LIST_SUCCESS
} from '../constants/transactionConstant';
import { getAuthorization } from '../utils/axiosUtils';

const BASE_URL = `${process.env.REACT_APP_BLINDS_SERVER}/api/v1/transaction`;

export const getTransactionList = () => async (dispatch, getState) => {
  dispatch({
    type: GET_TRANSACTION_LIST_REQUEST,
    payload: ''
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(`${BASE_URL}`, getAuthorization(userInfo.token));
    dispatch({
      type: GET_TRANSACTION_LIST_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_TRANSACTION_LIST_FAIL,
      payload: message
    });
  }
};

export const getPurchaseTransactionList = () => async (dispatch, getState) => {
  dispatch({
    type: GET_PURCHASE_TRANSACTION_LIST_REQUEST,
    payload: ''
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/purchaseTransactionList`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_PURCHASE_TRANSACTION_LIST_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_PURCHASE_TRANSACTION_LIST_FAIL,
      payload: message
    });
  }
};

export const getCustomerTransactionList = (customerId) => async (dispatch, getState) => {
  dispatch({
    type: GET_CUSTOMER_TRANSACTION_LIST_REQUEST,
    payload: customerId
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/customer?customerId=${customerId}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_CUSTOMER_TRANSACTION_LIST_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_CUSTOMER_TRANSACTION_LIST_FAIL,
      payload: message
    });
  }
};

export const getSupplierTransactionList = (supplierId) => async (dispatch, getState) => {
  dispatch({
    type: GET_SUPPLIER_TRANSACTION_LIST_REQUEST,
    payload: supplierId
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/supplier?supplierId=${supplierId}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_SUPPLIER_TRANSACTION_LIST_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_SUPPLIER_TRANSACTION_LIST_FAIL,
      payload: message
    });
  }
};

export const getSellReturnTransactionList = (supplierId) => async (dispatch, getState) => {
  dispatch({
    type: GET_SELL_RETURN_TRANSACTION_LIST_REQUEST,
    payload: supplierId
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(`${BASE_URL}/sellReturn`, getAuthorization(userInfo.token));
    dispatch({
      type: GET_SELL_RETURN_TRANSACTION_LIST_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_SELL_RETURN_TRANSACTION_LIST_FAIL,
      payload: message
    });
  }
};

export const getCustomerSellReturnTransactionList = (customerId) => async (dispatch, getState) => {
  dispatch({
    type: GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_REQUEST,
    payload: customerId
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/customerSellReturn?customerId=${customerId}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_FAIL,
      payload: message
    });
  }
};

import Axios from 'axios';
import {
  CREATE_NEW_USER_FAIL,
  CREATE_NEW_USER_REQUEST,
  CREATE_NEW_USER_SUCCESS,
  DELETE_USER_FAIL,
  DELETE_USER_REQUEST,
  DELETE_USER_SUCCESS,
  GET_USERS_FAIL,
  GET_USERS_REQUEST,
  GET_USERS_SUCCESS,
  SIGN_IN_USER_FAIL,
  SIGN_IN_USER_REQUEST,
  SIGN_IN_USER_SUCCESS,
  UPDATE_USER_PROFILE_FAIL,
  UPDATE_USER_PROFILE_REQUEST,
  UPDATE_USER_PROFILE_SUCCESS,
  UPDATE_USER_FAIL,
  UPDATE_USER_REQUEST,
  UPDATE_USER_SUCCESS,
  USER_SIGN_OUT,
  USER_UPDATE_PASSWORD_FAIL,
  USER_UPDATE_PASSWORD_REQUEST,
  USER_UPDATE_PASSWORD_SUCCESS,
  SEND_EMAIL_TO_USER_REQUEST,
  SEND_EMAIL_TO_USER_SUCCESS,
  SEND_EMAIL_TO_USER_FAIL
} from '../constants/userConstants';
import { getAuthorization } from '../utils/axiosUtils';
import { BALA_JI_USER } from '../constants/localStorageConstatnt';

const BASE_URL = `${process.env.REACT_APP_BLINDS_SERVER}/api/v1/users`;

export const updateUserProfile = (user) => async (dispatch, getState) => {
  dispatch({
    type: UPDATE_USER_PROFILE_REQUEST,
    payload: user
  });
  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.put(
      `${BASE_URL}/updateProfile`,
      user,
      getAuthorization(userInfo?.token)
    );
    dispatch({
      type: UPDATE_USER_PROFILE_SUCCESS,
      payload: data
    });
    dispatch({
      type: SIGN_IN_USER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: UPDATE_USER_PROFILE_FAIL,
      payload: message
    });
  }
};

export const signIn = (userName, password) => async (dispatch) => {
  dispatch({
    type: SIGN_IN_USER_REQUEST,
    payload: { userName, password }
  });

  try {
    const { data } = await Axios.post(`${BASE_URL}/signin`, {
      userName,
      password
    });
    dispatch({
      type: SIGN_IN_USER_SUCCESS,
      payload: data
    });
    localStorage.setItem(BALA_JI_USER, JSON.stringify(data));
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: SIGN_IN_USER_FAIL,
      payload: message
    });
  }
};

export const googleSignIn =
  ({ email }) =>
  async (dispatch) => {
    dispatch({
      type: SIGN_IN_USER_REQUEST,
      payload: { email }
    });
    try {
      const { data } = await Axios.post(`${BASE_URL}/googleSignIn`, {
        email
      });
      dispatch({
        type: SIGN_IN_USER_SUCCESS,
        payload: data
      });

      localStorage.setItem('userInfo', JSON.stringify(data));
    } catch (error) {
      dispatch({
        type: SIGN_IN_USER_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
      });
    }
  };

export const signOut = () => (dispatch) => {
  localStorage.removeItem(BALA_JI_USER);
  dispatch({
    type: USER_SIGN_OUT
  });
};

export const createNewUser = (user) => async (dispatch, getState) => {
  dispatch({
    type: CREATE_NEW_USER_REQUEST,
    payload: user
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.post(
      `${BASE_URL}/create`,
      user,
      getAuthorization(userInfo?.token)
    );
    dispatch({
      type: CREATE_NEW_USER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: CREATE_NEW_USER_FAIL,
      payload: message
    });
  }
};

export const getAllUsers = () => async (dispatch, getState) => {
  dispatch({
    type: GET_USERS_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.get(
      `${BASE_URL}/userListByShopId/?shopId=${userInfo?.shopId}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: GET_USERS_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: GET_USERS_FAIL,
      payload: message
    });
  }
};

export const updateUserDetailsByAdmin = (user) => async (dispatch, getState) => {
  dispatch({
    type: UPDATE_USER_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.put(
      `${BASE_URL}/updateByAdmin`,
      user,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: UPDATE_USER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: UPDATE_USER_FAIL,
      payload: message
    });
  }
};

export const deleteUserDetails = (user) => async (dispatch, getState) => {
  dispatch({
    type: DELETE_USER_REQUEST,
    payload: {}
  });

  try {
    const {
      userSignIn: { userInfo }
    } = getState();
    const { data } = await Axios.delete(
      `${BASE_URL}/deleteTemp?userId=${user?._id}`,
      getAuthorization(userInfo.token)
    );
    dispatch({
      type: DELETE_USER_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: DELETE_USER_FAIL,
      payload: message
    });
  }
};

export const updateUserPassword = (body) => async (dispatch, getState) => {
  dispatch({
    type: USER_UPDATE_PASSWORD_REQUEST,
    payload: body
  });
  const {
    userSignIn: { userInfo }
  } = getState();
  try {
    const { data } = await Axios.put(`${BASE_URL}/changePassword`, body, {
      headers: {
        Authorization: `Bearer ${userInfo?.token}`
      }
    });
    dispatch({
      type: USER_UPDATE_PASSWORD_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: USER_UPDATE_PASSWORD_FAIL,
      payload: message
    });
  }
};

export const sendEmailToUser =
  ({ email }) =>
  async (dispatch) => {
    dispatch({
      type: SEND_EMAIL_TO_USER_REQUEST,
      payload: { email }
    });
    try {
      const { data } = await Axios.post(`${BASE_URL}/sendEmailToSetPassword`, {
        email
      });
      dispatch({
        type: SEND_EMAIL_TO_USER_SUCCESS,
        payload: data
      });
    } catch (error) {
      dispatch({
        type: SEND_EMAIL_TO_USER_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
      });
    }
  };

export const changePassword = (body) => async (dispatch, getState) => {
  dispatch({
    type: USER_UPDATE_PASSWORD_REQUEST,
    payload: body
  });
  try {
    const { data } = await Axios.put(`${BASE_URL}/setNewPassword`, body);
    dispatch({
      type: USER_UPDATE_PASSWORD_SUCCESS,
      payload: data
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message ? error.response.data.message : error.message;
    dispatch({
      type: USER_UPDATE_PASSWORD_FAIL,
      payload: message
    });
  }
};

import React from 'react';

export const CustomTab = ({ data, current, setCurrent }) => {
  return (
    <div className="flex gap-4 py-2 text-[14px] font-bold">
      {data?.map((value, index) => (
        <div
          className={
            current === value
              ? 'flex flex-col gap-1 text-[#129BFF]'
              : 'flex flex-col gap-1 text-[#888888]'
          }
          key={index}
          onClick={() => setCurrent(value)}>
          <h1>{value}</h1>
          {current === value && <div className="border border-1 border-[#129BFF] "></div>}
        </div>
      ))}
    </div>
  );
};

import { Button } from '@chakra-ui/react';
import ExcelJS from 'exceljs';
import { saveAs } from 'file-saver';

export const ExportRecord = (props) => {
  const { data, columns, fileName, label } = props;
  const handleExportData = async () => {
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Valid Data');

    // Add headers
    const headerRow = worksheet.addRow(columns?.map((column) => column.header));

    // Style headers
    headerRow.eachCell((cell) => {
      cell.font = { bold: true };
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { rgb: 'FFB0E0E6' } // Light blue background
      };
      cell.border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' }
      };
      cell.alignment = { vertical: 'middle', horizontal: 'center' };
    });

    // Set column widths
    worksheet.columns = columns;

    // Add data rows
    data.forEach((row) => {
      worksheet.addRow(row);
    });

    const buffer = await workbook.xlsx.writeBuffer();
    saveAs(new Blob([buffer]), `${fileName}.xlsx`);
  };
  return (
    <Button bgColor="#82CD47" _hover={{ color: '#FFFFFF' }} onClick={handleExportData}>
      {label}
    </Button>
  );
};

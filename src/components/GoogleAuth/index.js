import { Box } from '@chakra-ui/react';
import { GoogleLogin } from '@react-oauth/google';
import { message } from 'antd';
import { jwtDecode } from 'jwt-decode';

export function LoginWithGoogle(props) {
  return (
    <Box>
      <GoogleLogin
        onSuccess={(credentialResponse) => {
          props.onSuccess(jwtDecode(credentialResponse?.credential || ''));
        }}
        onError={() => {
          message.error('Something wend wrong, Please contact to admin');
        }}
        useOneTap
      />
    </Box>
  );
}

import React from 'react';
import { Box, Button, Modal, ModalOverlay, ModalContent, ModalHeader, ModalBody, ModalFooter, Text, List, ListItem } from '@chakra-ui/react';

const ErrorModal = ({ isOpen, onClose, error, workbookWithErrors, handleDownloadErrorsFile }) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} size="lg">
      <ModalOverlay />
      <ModalContent>
        <ModalHeader color="red.500">Validation Errors:</ModalHeader>
        <ModalBody
          display="flex"
          flexDirection="column"
          maxH="400px" // Set a maximum height for the modal body
          overflowY="auto" // Add vertical scrolling when content overflows
        >
          {/* Scrollable list of errors */}
          <Box flex="1" overflowY="auto" mb={4}>
            <List spacing={3}>
              {error.map((errorMsg, index) => (
                <ListItem key={index} color="red.500">
                  {errorMsg}
                </ListItem>
              ))}
            </List>
          </Box>
          {/* Fixed text at the bottom */}
          <Box>
            <Text fontWeight="bold">
              Please download the error-highlighted file, correct the issues, and upload again.
            </Text>
          </Box>
        </ModalBody>
        <ModalFooter>
          <Button colorScheme="green" mr={3} onClick={handleDownloadErrorsFile}>
            Download Excel with Errors Highlighted
          </Button>
          <Button colorScheme="red" onClick={onClose}>
            Close
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default ErrorModal;

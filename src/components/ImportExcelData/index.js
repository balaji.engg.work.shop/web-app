import React, { useEffect, useState, useRef } from 'react';
import ExcelJS from 'exceljs';
import { Button, useDisclosure } from '@chakra-ui/react';
import { saveAs } from 'file-saver';
import ErrorModal from './ErrorModal'; // Adjust the import path as necessary

export const ImportExcelData = ({ columnsConfig, handelSave, hiddenFileInput }) => {
  const [data, setData] = useState([]);
  const [error, setError] = useState([]);
  const [workbookWithErrors, setWorkbookWithErrors] = useState(null);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const expectedHeaders = ['HSN SAC Code', 'Category', 'Product Name', 'Brand Name', 'Unit', 'Available Quantity', 'Purchasing price', 'Marketing price', 'Selling price', 'Weight Per Piece(Gram)'];
  const maxSize = 5 * 1024 * 1024; // 5MB

  const handleClick = () => {
    hiddenFileInput.current?.click();
  };

  const applyHeaderStyle = (headerRow) => {
    headerRow.eachCell((cell) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFCCCCFF' }, // Light purple background
      };
      cell.font = {
        bold: true,
        color: { argb: 'FF000000' }, // Black text color
      };
      cell.alignment = {
        vertical: 'middle',
        horizontal: 'center',
      };
      cell.border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' },
      };
    });
  };


  const handleFileUpload = async (event) => {
    const file = event.target.files[0];
    if (!file) return;

    const errorMessage = validateFile(file);
    if (errorMessage) {
      setError([errorMessage]);
      onOpen();
      return;
    }

    const workbook = new ExcelJS.Workbook();
    try {
      await workbook.xlsx.load(file);
    } catch (error) {
      setError(['Failed to read the Excel file. Please ensure it is a valid .xlsx file.']);
      onOpen();
      return;
    }

    const worksheet = workbook.worksheets[0];
    if (!worksheet) {
      setError(['The file is empty. Please upload a file with data.']);
      onOpen();
      return;
    }

    // Apply header style explicitly
    const headerRow = worksheet.getRow(1);
    applyHeaderStyle(headerRow);

    const validationErrors = [
      ...validateHeadersInWorksheet(worksheet),
      ...validateDataRowsInWorksheet(worksheet),
    ];

    if (validationErrors.length > 0) {
      setError(validationErrors);
      setData([]);
      setWorkbookWithErrors(workbook);
      onOpen();
    } else {
      try {
        const rows = parseDataFromWorksheet(worksheet);
        setData(rows);
        handelSave(rows);
      } catch (err) {
        setError([`Error reading file: ${err.message}`]);
        onOpen();
      }
    }
  };

  // Function implementations here...
  const validateFile = (file) => {
    if (!file.name.endsWith('.xlsx')) return 'Please upload a valid Excel file with .xlsx extension.';
    if (file.size < 1024 || file.size > maxSize) return 'File size should be between 1KB and 5MB.';
    return null;
  };

  const validateHeadersInWorksheet = (worksheet) => {
    const errors = [];
    const headerRow = worksheet.getRow(1);
    const headers = headerRow.values.slice(1);
    if (!validateHeaders(headers)) {
      errors.push('Headers do not match the expected format.');
      highlightHeaderErrors(headerRow, headers);
    }
    return errors;
  };

  // Function to validate headers
const validateHeaders = (headers) => {
  const expectedHeaders = [
    'HSN SAC Code',
    'Category',
    'Product Name',
    'Brand Name',
    'Unit',
    'Available Quantity',
    'Purchasing price',
    'Marketing price',
    'Selling price',
    'Weight Per Piece(Gram)',
  ];
  return expectedHeaders.every((header) => headers.includes(header));
};

// Function to highlight header errors
const highlightHeaderErrors = (headerRow, headers) => {
  headers.forEach((header, index) => {
    if (!expectedHeaders.includes(header)) {
      const cell = headerRow.getCell(index + 1);
      applyErrorStyle(cell); // Assuming applyErrorStyle is already defined
    }
  });
};

  const validateDataRowsInWorksheet = (worksheet) => {
    const errors = [];
    worksheet.eachRow({ includeEmpty: false }, (row, rowNumber) => {
      if (rowNumber === 1) return; // Skip header row
      const rowErrors = validateRow(row, rowNumber);
      if (rowErrors.length > 0) {
        errors.push(...rowErrors);
      }
    });
    return errors;
  };

  const parseDataFromWorksheet = (worksheet) => {
    const rows = [];
    const headerMap = {};

    worksheet.getRow(1).eachCell({ includeEmpty: true }, (cell, colNumber) => {
      const header = cell.value;
      if (header) {
        const colConfig = columnsConfig.find((col) => col.header === header);
        if (colConfig) {
          headerMap[colConfig.key] = colNumber;
        }
      }
    });

    worksheet.eachRow({ includeEmpty: true }, (row, rowNumber) => {
      if (rowNumber === 1) return; // Skip header row
      const rowData = {};
      columnsConfig.forEach((col) => {
        const colIndex = headerMap[col.key];
        if (colIndex) {
          rowData[col.key] = row.getCell(colIndex).value;
        }
      });
      rows.push(rowData);
    });

    return rows;
  };

  const validateRow = (row, rowNumber) => {
    const rowErrors = [];
    const cells = [
      { index: 2, name: 'Category', type: 'string' },
      { index: 3, name: 'Product Name', type: 'string' },
      { index: 5, name: 'Unit', type: 'string' },
      { index: 6, name: 'Quantity', type: 'number' },
      { index: 7, name: 'Purchasing Price', type: 'number' },
      { index: 8, name: 'Marketing Price', type: 'number' },
      { index: 9, name: 'Selling Price', type: 'number' }
    ];

    cells.forEach(({ index, name, type }) => {
      const cell = row.getCell(index);
      const value = cell.value ? cell.value.toString().trim() : '';

      if (type === 'string') {
        if (!value) {
          rowErrors.push(`Row ${rowNumber}: ${name} is required.`);
          applyErrorStyle(cell);
        }
      } else if (type === 'number') {
        if (value === '') {
          rowErrors.push(`Row ${rowNumber}: ${name} is required.`);
          applyErrorStyle(cell);
        } else if (isNaN(value)) {
          rowErrors.push(`Row ${rowNumber}: ${name} must be a number.`);
          applyErrorStyle(cell);
        } else if (Number(value) < 0) {
          rowErrors.push(`Row ${rowNumber}: ${name} must be greater than 0.`);
          applyErrorStyle(cell);
        }
      }
    });

    return rowErrors;
  };

  const applyErrorStyle = (cell) => {
    cell.fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'FFFFC7CE' }, // Light red background
    };
    cell.font = {
      color: { argb: 'FF9C0006' }, // Dark red font
    };
    cell.border = {
      top: { style: 'thin', color: { argb: 'FF9C0006' } },
      left: { style: 'thin', color: { argb: 'FF9C0006' } },
      bottom: { style: 'thin', color: { argb: 'FF9C0006' } },
      right: { style: 'thin', color: { argb: 'FF9C0006' } },
    };
  };

  const handleDownloadErrorsFile = async () => {
    if (workbookWithErrors) {
      const buffer = await workbookWithErrors.xlsx.writeBuffer();
      saveAs(new Blob([buffer]), 'Excel_Errors.xlsx');
    }
  };

  return (
    <div>
      <Button colorScheme="green" onClick={handleClick}>
        Upload file
      </Button>
      <input
        title="select media"
        type="file"
        accept=".xlsx"
        onChange={handleFileUpload}
        ref={hiddenFileInput}
        style={{ display: 'none' }} // Make the file input element invisible
        multiple={false}
      />
      
      {/* Modal for Displaying Errors */}
      <ErrorModal
        isOpen={isOpen}
        onClose={onClose}
        error={error}
        workbookWithErrors={workbookWithErrors}
        handleDownloadErrorsFile={handleDownloadErrorsFile}
      />
    </div>
  );
};

import React, { useState } from 'react';
import { Select, Divider, Modal, Input, Space } from 'antd';
import { Flex } from '@chakra-ui/react';

const { Option } = Select;

export function CategoryManager(props) {
  const { options, setOption, placeholder, componentName, value, onChange, style, size } = props;

  const onSearch = (value) => {
    console.log('search:', value);
  };

  // Step 1: State to store the list of DB configurations and the selected option
  const [modalVisible, setModalVisible] = useState(false);
  const [newConfig, setNewConfig] = useState('');
  const [editingConfig, setEditingConfig] = useState(null);
  const [editedConfigName, setEditedConfigName] = useState('');

  // Step 2: Open modal to add a new database configuration
  const openModal = () => {
    setModalVisible(true);
  };

  // Step 3: Handle adding a new database configuration
  const handleAddConfig = () => {
    if (newConfig && !options.includes(newConfig)) {
      setOption([...options, newConfig]);
      setNewConfig(''); // Clear the input field after adding
      setModalVisible(false);
    } else {
      alert('This configuration already exists or is invalid.');
    }
  };

  // Step 4: Open modal for editing a selected configuration
  const openEditModal = (config) => {
    setEditingConfig(config);
    setEditedConfigName(config);
  };

  // Step 5: Handle editing a database configuration
  const handleEditConfig = () => {
    if (editedConfigName && !options?.includes(editedConfigName)) {
      const updatedConfigList = options?.map((config) =>
        config === editingConfig ? editedConfigName : config
      );
      setOption(updatedConfigList);
      setEditingConfig(null);
      setEditedConfigName('');
    } else {
      alert('This name already exists or is invalid.');
    }
  };

  // Step 6: Handle deleting a configuration
  const handleDeleteConfig = (config) => {
    const confirmDelete = window.confirm(`Are you sure you want to delete "${config}"?`);
    if (confirmDelete) {
      setOption(options.filter((item) => item !== config));
    }
  };

  return (
    <div>
      <Select
        name={componentName}
        showSearch
        style={style}
        size={size || 'large'}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        optionFilterProp="children"
        onSearch={onSearch}
        filterOption={(input, option) =>
          (option?.value ?? '').toLowerCase().includes(input.toLowerCase())
        }
        dropdownRender={(menu) => (
          <div>
            {menu}
            <Divider style={{ margin: '4px 0' }} />
            <div
              style={{ padding: '4px 8px', cursor: 'pointer' }}
              onMouseDown={(e) => e.preventDefault()}
              onClick={openModal}>
              <i className="fi fi-br-plus"></i> Add Database
            </div>
          </div>
        )}>
        {options?.map((item, index) => (
          <Option key={index} value={item}>
            {value !== item ? (
              <Flex justifyContent={'space-between'}>
                {item}
                <Space>
                  <i className="fi fi-sr-pencil" onClick={() => openEditModal(item)} />
                  <i className="fi fi-rs-trash" onClick={() => handleDeleteConfig(item)}></i>
                </Space>
              </Flex>
            ) : null}
          </Option>
        ))}
      </Select>

      {/* Modal for Adding a New Configuration */}
      <Modal
        title="Add New Database Configuration"
        visible={modalVisible}
        onOk={handleAddConfig}
        onCancel={() => setModalVisible(false)}>
        <Input
          value={newConfig}
          onChange={(e) => setNewConfig(e.target.value?.toUpperCase())}
          placeholder="Enter new database configuration name"
        />
      </Modal>

      {/* Modal for Editing an Existing Configuration */}
      <Modal
        title="Edit Database Configuration"
        visible={editingConfig !== null}
        onOk={handleEditConfig}
        onCancel={() => setEditingConfig(null)}>
        <Input
          value={editedConfigName}
          onChange={(e) => setEditedConfigName(e.target.value?.toUpperCase())}
          placeholder="Enter new name for this configuration"
        />
      </Modal>
    </div>
  );
}

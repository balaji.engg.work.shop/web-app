import { Button, Text } from '@chakra-ui/react';
import { BiSave } from 'react-icons/bi';
import { FaPlus } from 'react-icons/fa';
import { GrUpdate } from 'react-icons/gr';
import { RiDeleteBin5Fill } from 'react-icons/ri';

export function MyButton(props) {
  const { handelClick, label, icon, color, bgColor, isDisabled, isLoading, loadingText } = props;

  return (
    <Button
      variant="outline"
      borderRadius="md"
      color={color || '#000000'}
      bg={bgColor || '#FFFFFF'}
      height="37px"
      px="4"
      _hover={{ color: bgColor || '#FFFFFF', bgColor: color || '#000000' }}
      onClick={handelClick}
      leftIcon={icon || null}
      isDisabled={isDisabled || false}
      isLoading={isLoading || false}
      loadingText={loadingText || ''}>
      <Text m="0" fontSize="md">
        {label}
      </Text>
    </Button>
  );
}

export function AddButton(props) {
  const { onClick } = props;
  return (
    <Button
      leftIcon={<FaPlus />}
      onClick={onClick}
      color="#FFFFFF"
      background="#06D001"
      _hover={{ color: '#06D001', bgColor: '#FFFFFF', border: '2px', borderColor: '#06D001' }}>
      {props.label}
    </Button>
  );
}

export function SaveButton(props) {
  const { handelClick, loading, loadingText } = props;
  return (
    <Button
      type="submit"
      leftIcon={<BiSave />}
      onClick={props.onClick}
      color="#FFFFFF"
      background="#06D001"
      isLoading={loading}
      loadingText={loadingText}
      _hover={{ color: '#06D001', bgColor: '#FFFFFF', border: '2px', borderColor: '#06D001' }}>
      {props.label}
    </Button>
  );
}
export function UpdateButton(props) {
  const { handelClick, loading, loadingText } = props;
  return (
    <Button
      type="submit"
      leftIcon={<GrUpdate />}
      onClick={props.onClick}
      color="#FFFFFF"
      background="#06D001"
      isLoading={loading}
      loadingText={loadingText}
      _hover={{ color: '#06D001', bgColor: '#FFFFFF', border: '2px', borderColor: '#06D001' }}>
      {props.label}
    </Button>
  );
}

export function CloseButton(props) {
  const { onClick } = props;
  return (
    <Button
      onClick={onClick}
      color="#FFFFFF"
      background="red"
      _hover={{ color: 'red', bgColor: '#FFFFFF', border: '2px', borderColor: 'red' }}>
      Close
    </Button>
  );
}

export function DeleteButton(props) {
  const { onClick, label } = props;
  return (
    <Button
      onClick={onClick}
      leftIcon={<RiDeleteBin5Fill size={20} />}
      color="#FFFFFF"
      background="red"
      _hover={{ color: 'red', bgColor: '#FFFFFF', border: '2px', borderColor: 'red' }}>
      {label}
    </Button>
  );
}

export function DownLoadButton(props) {
  const { onClick, label } = props;
  return (
    <Button
      onClick={onClick}
      leftIcon={<i className="fi fi-ss-file-download"></i>}
      className="border border-2 border-green-500 bg-white text-green-400 hover:test-white hover:bg-green-500 ">
      {label || 'Download File'}
    </Button>
  );
}

import { Button, Divider, Input, message, Select, Space } from 'antd';
import { useRef, useState } from 'react';
import { FiPlus } from 'react-icons/fi';
import { addValueOnLocalStorage } from '../../utils/localStorageUtils';

export const SelectWithAddInputField = (props) => {
  const { options, setOption, placeholder, componentName, value, onChange, style, size } = props;
  const [name, setName] = useState('');
  const inputRef = useRef(null);

  const onSearch = (value) => {
    console.log('search:', value);
  };

  const onNameChange = (event) => {
    setName(event.target.value?.toUpperCase());
  };
  const addItem = (e) => {
    e.preventDefault();
    if (name?.length == 0) {
      message.error('Please add value');
    } else {
      setOption([...options, name]);
      setName('');
      setTimeout(() => {
        inputRef.current?.focus();
      }, 0);
    }
  };
  return (
    <Select
      name={componentName}
      showSearch
      style={style}
      size={size || 'large'}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      optionFilterProp="children"
      onSearch={onSearch}
      filterOption={(input, option) =>
        (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
      }
      options={options?.map((data) => {
        return {
          value: data,
          label: data
        };
      })}
      dropdownRender={(menu) => (
        <>
          {menu}
          <Divider
            style={{
              margin: '8px 0'
            }}
          />
          <Space
            style={{
              padding: '0 8px 4px'
            }}>
            <Input
              placeholder="Please enter item"
              ref={inputRef}
              value={name}
              onChange={onNameChange}
              onKeyDown={(e) => e.stopPropagation()}
            />
            <Button type="text" icon={<FiPlus />} onClick={addItem}>
              Add item
            </Button>
          </Space>
        </>
      )}
    />
  );
};

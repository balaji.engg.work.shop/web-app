import React, { useState } from 'react';

export const SuggestionInput = ({ suggestions, onChange, placeholder, value }) => {
  const [filteredSuggestions, setFilteredSuggestions] = useState([]);
  const [showSuggestions, setShowSuggestions] = useState(false);

  const handleInputChange = (e) => {
    const inputValue = e.target.value;
    onChange(inputValue);

    if (inputValue.trim() !== '') {
      const filtered = suggestions?.filter((suggestion) =>
        suggestion?.toLowerCase()?.includes(inputValue?.toLowerCase())
      );
      setFilteredSuggestions(filtered);
      setShowSuggestions(true);
    } else {
      setFilteredSuggestions([]);
      setShowSuggestions(false);
    }
  };

  const handleSuggestionClick = (suggestion) => {
    onChange(suggestion);
    setShowSuggestions(false);
  };

  const handleBlur = () => {
    setTimeout(() => setShowSuggestions(false), 100);
  };

  return (
    <div className="relative w-full">
      <input
        type="text"
        value={value}
        onChange={handleInputChange}
        onBlur={handleBlur}
        onFocus={() => value && setShowSuggestions(true)}
        placeholder={placeholder}
        className="w-full h-12 px-4 py-2 border focus:outline-none focus:ring focus:ring-indigo-300"
      />
      {showSuggestions && filteredSuggestions?.length > 0 && (
        <ul className="absolute top-full left-0 right-0 bg-white border border-gray-300 shadow-md mt-1 max-h-36 overflow-y-auto z-50">
          {filteredSuggestions?.map((suggestion, index) => (
            <li
              key={index}
              onMouseDown={(e) => e.preventDefault()}
              onClick={() => handleSuggestionClick(suggestion)}
              className="px-4 py-2 cursor-pointer hover:bg-indigo-100 border-b last:border-b-0">
              {suggestion}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

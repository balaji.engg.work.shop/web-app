import { useCallback, useState } from 'react';
import './index.css';
import { AddEditCustomerModal } from '../modal/AddEditCustomerModal';
import { AddEditProductModal } from '../modal/AddEditProductModal';
import { AddEditSupplierModel } from '../modal/AddEditSupplierModel';
import { AddEditBankAccountModal } from '../modal/AddEditBankAccountModal';

export const MyMenu = (props) => {
  const [openProduct, setOpenProduct] = useState(false);
  const [openCustomer, setOpenCustomer] = useState(false);
  const [openSupplier, setOpenSupplier] = useState(false);
  const [openBankAccount, setOpenBankAccount] = useState(false);

  const handleCloseProduct = useCallback(() => {
    setOpenProduct(false);
  }, [openProduct]);

  const handleCloseCustomer = useCallback(() => {
    setOpenCustomer(false);
  }, [openCustomer]);

  const handleCloseBankAccount = useCallback(() => {
    setOpenBankAccount(false);
  }, [openBankAccount]);

  const handleCloseSupplier = useCallback(() => {
    setOpenSupplier(false);
  }, [openSupplier]);

  const handleOpenProduct = useCallback(() => {
    setOpenProduct(true);
  }, [openProduct]);

  const handleOpenCustomer = useCallback(() => {
    setOpenCustomer(true);
  }, [openCustomer]);

  const handleOpenBankAccount = useCallback(() => {
    setOpenBankAccount(true);
  }, [openBankAccount]);

  const handleOpenSupplier = useCallback(() => {
    setOpenSupplier(true);
  }, [openSupplier]);

  return (
    <>
      <AddEditCustomerModal
        open={openCustomer}
        onCancel={handleCloseCustomer}
        option={'Add'}
        customer={{}}
        setCurrentCustomer={() => {}}
      />
      <AddEditBankAccountModal
        open={openBankAccount}
        onCancel={handleCloseBankAccount}
        option={'Add'}
        bankAccount={{}}
        setCurrentBankAccount={() => {}}
      />
      <AddEditProductModal
        open={openProduct}
        onCancel={handleCloseProduct}
        option={'Add'}
        product={{}}
      />
      <AddEditSupplierModel
        open={openSupplier}
        onCancel={handleCloseSupplier}
        option={'Add'}
        supplier={{}}
        setCurrentSupplier={() => {}}
      />
      <div className="dropdown">
        <button className="w-full h-9 border border-solid px-2 py-2 bg-sky-500 rounded-md text-white hover:bg-sky-700">
          <i class="fi fi-br-menu-burger"></i>
        </button>
        <div className="dropdown-content">
          <div
            onClick={handleOpenProduct}
            className="flex flex-row gap-4 border border-1 items-center py-2 px-2 hover:bg-sky-600 hover:text-white">
            <i class="fi fi-br-plus"></i>
            <h1 className="text-black-1000">Product</h1>
          </div>
          <div
            onClick={handleOpenCustomer}
            className="flex flex-row gap-4 border border-1 items-center py-2 px-2 hover:bg-sky-600 hover:text-white">
            <i class="fi fi-br-plus"></i>
            <h1 className="text-black-1000">Customer</h1>
          </div>
          <div
            onClick={handleOpenSupplier}
            className="flex flex-row gap-4 border border-1 items-center py-2 px-2 hover:bg-sky-600 hover:text-white">
            <i class="fi fi-br-plus"></i>
            <h1 className="text-black-1000">Supplier</h1>
          </div>
          <div
            onClick={handleOpenBankAccount}
            className="flex flex-row gap-4 border border-1 items-center py-2 px-2 hover:bg-sky-600 hover:text-white">
            <i class="fi fi-br-plus"></i>
            <h1 className="text-black-1000">Bank Account</h1>
          </div>
        </div>
      </div>
    </>
  );
};

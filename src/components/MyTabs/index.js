import { Box, Flex, Stack, Text } from '@chakra-ui/react';

export function MyTabs(props) {
  const tabOptions = props?.options?.map((label, index) => {
    return { key: index + 1, label: label };
  });
  const { selectedTab, setSelectedTab } = props;
  return (
    <Flex
      px="5"
      align="center"
      border="1px"
      borderColor="#EBEBEB"
      pr="10"
      gap="10"
      bgColor="#FFFFFF"
      width="100%">
      {tabOptions?.map((option, index) => {
        return (
          <Stack key={index}>
            <Flex
              gap="2"
              py="2"
              style={{ cursor: 'pointer' }}
              color={selectedTab === option.key ? '#000000' : '#7C7C7C'}
              fontWeight={selectedTab === option.key ? '700' : '400'}
              align="center"
              onClick={() => setSelectedTab(option.key)}>
              <Text m="0" fontSize={{ base: '16px', lg: '16px' }}>
                {option.label}
              </Text>
            </Flex>
            <Box height="2px" background={selectedTab === option.key ? '#000000' : ''}></Box>
          </Stack>
        );
      })}
    </Flex>
  );
}

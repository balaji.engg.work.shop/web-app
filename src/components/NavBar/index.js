import { Button, Stack, Box, Text, Flex, Image } from '@chakra-ui/react';
import { useState } from 'react';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { signOut } from '../../action/userActions';
import { useDispatch, useSelector } from 'react-redux';
import { logo } from '../../accets';
import { DASHBOARD, SIGN_IN } from '../../constants/routeConstant';

export function NavBar() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const btnRef = React.useRef();

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const signOutHandler = () => {
    dispatch(signOut());
    navigate(SIGN_IN);
    window.location.reload();
  };

  return (
    <Box px="5" py="2" backgroundColor="#FFFFFF">
      {userInfo ? (
        <Stack justifyContent={'space-between'} flexDir={'row'}>
          <Flex align="center" gap="10">
            <Image src={logo} alt="logo" height="50px" onClick={() => navigate(DASHBOARD)} />
          </Flex>
          <Flex justifyContent="space-between" gap={{ base: '2', lg: '10' }}>
            <Text color="red" fontSize={{ base: 'sm', lg: 'lg' }}>
              {userInfo?.shopName}
            </Text>
            <Text color="#000000" fontSize={{ base: 'sm', lg: 'lg' }}>
              {userInfo?.fullName}
            </Text>
            <Button onClick={signOutHandler}>Log Out</Button>
          </Flex>
        </Stack>
      ) : (
        <Flex justifyContent="flex-end">
          <Button
            _hover={{ color: '#000000', bgColor: '#FFFFFF' }}
            onClick={() => navigate(SIGN_IN)}>
            Sign In
          </Button>
        </Flex>
      )}
    </Box>
  );
}

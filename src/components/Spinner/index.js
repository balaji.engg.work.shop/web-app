import { Center, Image } from '@chakra-ui/react';
import { Loading } from '../../accets';

const override = {
  display: 'block',
  margin: '0 auto',
  borderColor: 'red'
};

export function Spinner(props) {
  return (
    <div className="sweet-loading">
      {/* <ClipLoader
        color={'#FFFFFF'}
        loading={props?.loading}
        cssOverride={override}
        size={150}
        aria-label="Loading Spinner"
        data-testid="loader"
      /> */}

      <Center>
        <Image src={Loading} />
      </Center>
    </div>
  );
}

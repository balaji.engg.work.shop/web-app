import { Flex, Box, Text, SimpleGrid, useStatStyles } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { Form, Input, message, Modal, Select } from 'antd';
import { useForm } from 'antd/es/form/Form';
import { CloseButton, SaveButton } from '../InputComponents/MyButton';
import { useDispatch, useSelector } from 'react-redux';
import { accountType, bankName } from '../../constantData/index';
import {
  getAllBankAccount,
  updateBankAccount,
  createNewBankAccount
} from '../../action/bankAccountAction';
import { UPDATE_BANK_ACCOUNT_RESET } from '../../constants/bankAccountConstants';
import { ADD_NEW_BANK_ACCOUNT_RESET } from '../../constants/bankAccountConstants';

export const AddEditBankAccountModal = ({
  setCurrentBankAccount,
  bankAccount,
  option = 'Add',
  onCancel,
  open
}) => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const createBankAccount = useSelector((state) => state.createBankAccount);
  const {
    error: errorBankAccountCreate,
    loading: loadingBankAccountCreate,
    success: successBankAccountCreated
  } = createBankAccount;

  const bankAccountUpdate = useSelector((state) => state.bankAccountUpdate);
  const { error, loading, success } = bankAccountUpdate;

  const onFinish = (values) => {
    if (option === 'Add') dispatch(createNewBankAccount(values));
    else dispatch(updateBankAccount({ ...values, bankAccountId: bankAccount?.bankAccountId }));
  };

  const handleCloseModel = () => {
    setCurrentBankAccount({});
    form.resetFields();
    onCancel();
  };

  useEffect(() => {
    if (errorBankAccountCreate) {
      message.error(errorBankAccountCreate);
    }
    if (successBankAccountCreated) {
      message.success('BankAccount created successFully!');
      dispatch({ type: ADD_NEW_BANK_ACCOUNT_RESET });
      dispatch(getAllBankAccount());
      handleCloseModel();
    }
  }, [errorBankAccountCreate, successBankAccountCreated]);

  useEffect(() => {
    if (error) {
      message.error(error);
      dispatch({ type: UPDATE_BANK_ACCOUNT_RESET });
    }
    if (success) {
      message.success('BANK account updated successFully!');
      dispatch({ type: UPDATE_BANK_ACCOUNT_RESET });
      dispatch(getAllBankAccount());
      handleCloseModel();
    }
  }, [error, success]);

  useEffect(() => {
    if (bankAccount) {
      form.setFieldsValue(bankAccount);
    }
  }, [bankAccount, form]);

  return (
    <Modal
      style={{
        top: 20
      }}
      footer={[]}
      open={open}
      onCancel={onCancel}
      width={'70vw'}>
      <Box>
        <Text color="#000000" fontSize="28px" align="center" fontWeight="600">
          {option === 'Add' ? ' Add Bank Account' : 'Edit Bank Account'}
        </Text>
        <Form layout="vertical" form={form} autoComplete="off" onFinish={onFinish}>
          <SimpleGrid columns={[1, 2, 3]} gap={{ base: '1', lg: '4' }}>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Bank Name</label>}
              name="bankName"
              fontSize="40px"
              rules={[
                {
                  required: true,
                  message: 'Select bank name!'
                }
              ]}>
              <Select
                showSearch
                size="large"
                placeholder="Select bank name"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                }
                options={bankName}
              />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Account Number</label>}
              name="accountNumber"
              rules={[
                {
                  required: true,
                  message: 'Please input account number!'
                }
              ]}>
              <Input size="large" type="number" />
            </Form.Item>

            <Form.Item
              label={<label style={{ color: '#000000' }}>IFSC Code</label>}
              name="ifscCode"
              rules={[{ required: true, message: 'Please input your email!' }]}>
              <Input size="large" placeholder="SBI000034" />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Holder Name</label>}
              name="accountHolderName"
              rules={[
                {
                  required: true,
                  message: 'Please input your fullName!'
                }
              ]}>
              <Input size="large" />
            </Form.Item>

            <Form.Item
              label={<label style={{ color: '#000000' }}>Current Balance</label>}
              name="currentBalance"
              rules={[
                {
                  required: true,
                  message: 'Please input current balance!',
                  maxLength: 10
                }
              ]}>
              <Input type="number" size="large" />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Account Type</label>}
              name="accountType"
              rules={[
                {
                  required: true,
                  message: 'Please account type!'
                }
              ]}>
              <Select
                showSearch
                size="large"
                placeholder="Select Type"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                }
                options={accountType}
              />
            </Form.Item>
          </SimpleGrid>
          <Form.Item>
            <Flex justifyContent="end" gap="4">
              <SaveButton
                label="Save"
                loading={loadingBankAccountCreate || loading}
                loadingText={'Saving..'}
              />
              <CloseButton onClick={handleCloseModel} />
            </Flex>
          </Form.Item>
        </Form>
      </Box>
    </Modal>
  );
};

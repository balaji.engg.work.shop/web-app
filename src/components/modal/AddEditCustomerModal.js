import { Flex, Box, Text, SimpleGrid } from '@chakra-ui/react';
import { useEffect } from 'react';
import { Form, Input, message, Modal } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import {
  CREATE_NEW_CUSTOMER_RESET,
  UPDATE_CUSTOMER_RESET
} from '../../constants/customerConstants';
import { createNewCustomer, getAllCustomers, updateCustomer } from '../../action/customerAction';
import { IoMdCall } from 'react-icons/io';
import { MdOutlineMail } from 'react-icons/md';
import { CloseButton, SaveButton } from '../InputComponents/MyButton';
import {
  isValidEmail,
  isValidGST,
  isValidMobileNo,
  isValidPinCode
} from '../../utils/validateData';

export const AddEditCustomerModal = ({
  customer,
  setCurrentCustomer,
  option = 'Add',
  onCancel,
  open
}) => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const handleCloseModel = () => {
    form.resetFields();
    setCurrentCustomer();
    onCancel();
  };

  const createCustomer = useSelector((state) => state.createCustomer);
  const {
    error: errorInCreateCustomer,
    loading: loadingCreateCustomer,
    success: successCreateCustomer
  } = createCustomer;

  const customerUpdate = useSelector((state) => state.customerUpdate);
  const {
    error: errorInUpdateCustomer,
    loading: loadingUpdateCustomer,
    success: successUpdateCustomer
  } = customerUpdate;

  useEffect(() => {
    if (errorInCreateCustomer) {
      message.error(errorInCreateCustomer);
      dispatch({ type: CREATE_NEW_CUSTOMER_RESET });
    }
    if (successCreateCustomer) {
      message.success('Customer created successfully!');
      dispatch({ type: CREATE_NEW_CUSTOMER_RESET });
      dispatch(getAllCustomers());
      handleCloseModel();
    }
  }, [errorInCreateCustomer, successCreateCustomer]);

  useEffect(() => {
    if (errorInUpdateCustomer) {
      message.error(errorInUpdateCustomer);
      dispatch({ type: UPDATE_CUSTOMER_RESET });
    }
    if (successUpdateCustomer) {
      message.success('SuccessFully Updated!');
      dispatch({ type: UPDATE_CUSTOMER_RESET });
      dispatch(getAllCustomers());
      handleCloseModel();
    }
  }, [errorInUpdateCustomer, successUpdateCustomer]);

  useEffect(() => {
    if (customer) {
      form.setFieldsValue(customer);
    }
  }, [customer, form]);

  const onFinish = (values) => {
    if (option === 'Add') dispatch(createNewCustomer(values));
    else dispatch(updateCustomer({ ...values, customerId: customer?.customerId }));
  };

  const validateNumber = (_, value) => {
    return isValidMobileNo(value);
  };

  const validateGSTIN = (_, value) => {
    return isValidGST(value);
  };

  const validateEmail = (_, value) => {
    return isValidEmail(value);
  };
  const validatePinCode = (_, value) => {
    return isValidPinCode(value);
  };

  return (
    <div>
      <Modal
        style={{
          top: 20
        }}
        footer={[]}
        open={open}
        onCancel={handleCloseModel}
        width={'70vw'}>
        <Box>
          <Text color="#000000" fontSize="28px" align="center" fontWeight="600">
            {option === 'Add' ? ' Add New Customer' : 'Edit Customer'}
          </Text>
          <Form layout="vertical" form={form} autoComplete="off" onFinish={onFinish}>
            <SimpleGrid columns={[1, 2, 3]} gap={{ base: '1', lg: '4' }}>
              <Form.Item
                label={<label style={{ color: '#000000' }}>Customer Name</label>}
                name="customerName"
                fontSize="40px"
                rules={[
                  {
                    required: true,
                    message: 'Please input your user name!'
                  }
                ]}>
                <Input placeholder="Customer name" size="large" />
              </Form.Item>
              <Form.Item
                label={<label style={{ color: '#000000' }}>Mobile No</label>}
                name="mobileNo"
                rules={[
                  {
                    required: true,
                    message: 'Please input your mobileNo!',
                    maxLength: 10
                  },
                  {
                    validator: validateNumber
                  }
                ]}>
                <Input
                  addonBefore={<IoMdCall color="#000000" />}
                  maxLength={10}
                  size="large"
                  showCount={true}
                />
              </Form.Item>

              <Form.Item
                label={<label style={{ color: '#000000' }}>Email</label>}
                name="email"
                rules={[
                  {
                    validator: validateEmail
                  }
                ]}>
                <Input
                  type="email"
                  addonBefore={<MdOutlineMail color="#000000" />}
                  size="large"
                  placeholder="cccdcdcc@gmail.com"
                />
              </Form.Item>
              <Form.Item label={<label style={{ color: '#000000' }}>Address</label>} name="address">
                <Input size="large" />
              </Form.Item>
              <Form.Item
                label={<label style={{ color: '#000000' }}>City</label>}
                name="city"
                fontSize="40">
                <Input size="large" />
              </Form.Item>

              <Form.Item
                label={<label style={{ color: '#000000' }}>State</label>}
                name="state"
                fontSize="40">
                <Input size="large" />
              </Form.Item>
              <Form.Item
                label={<label style={{ color: '#000000' }}>PinCode</label>}
                name="pinCode"
                fontSize="40"
                rules={[
                  {
                    validator: validatePinCode
                  }
                ]}>
                <Input size="large" maxLength="6" showCount />
              </Form.Item>

              <Form.Item
                label={<label style={{ color: '#000000' }}>GST No.</label>}
                name="gst"
                rules={[
                  {
                    validator: validateGSTIN
                  }
                ]}>
                <Input maxLength={15} size="large" showCount={true} />
              </Form.Item>
            </SimpleGrid>
            <Form.Item>
              <Flex justifyContent="end" gap="4">
                <SaveButton
                  label="Save"
                  loading={loadingCreateCustomer || loadingUpdateCustomer}
                  loadingText={'Saving..'}
                />
                <CloseButton onClick={handleCloseModel} />
              </Flex>
            </Form.Item>
          </Form>
        </Box>
      </Modal>
    </div>
  );
};

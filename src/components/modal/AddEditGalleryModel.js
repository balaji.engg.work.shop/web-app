import React, { useEffect, useState } from 'react';
import { message, Modal } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { MultipleFileUploader } from '../molecule/MultipleFileUploader';
import { getSignedUrl, uploadFileToSignedUrl } from '../../utils/s3Utils';
import ImageView from '../molecule/ImageView';
import { CREATE_NEW_GALLERY_RESET, UPDATE_GALLERY_RESET } from '../../constants/galleryConstants';
import {
  createNewGallery,
  getAllGallery,
  getGalleryCategory,
  getGalleryListByShopId,
  updateGallery
} from '../../action/galleryAction';
import { CloseButton, SaveButton } from '../InputComponents/MyButton';
import { SuggestionInput } from '../InputComponents';

export const AddEditGalleryModel = ({ gallery, option = 'Add', onCancel, open }) => {
  const dispatch = useDispatch();

  // State management
  const [category, setGalleryCategory] = useState('');
  const [notes, setNotes] = useState('');
  const [mediaFiles, setMediaFiles] = useState([]);
  const [workingRate, setWorkingRate] = useState('');

  console.log('mediaFiles', mediaFiles);

  // Redux state
  const { data: galleryCategoryData } = useSelector((state) => state.getGalleryCategoryReducer);
  const { error: errorUpdate, success: successUpdate } = useSelector(
    (state) => state.updateGalleryReducer
  );
  const { error: errorCreate, success: successCreate } = useSelector(
    (state) => state.createNewGalleryReducer
  );

  // ✅ Validate form before submitting
  const handleValidate = () => {
    if (!category) {
      message.error('Please select or add a new category');
      return false;
    }
    if (!notes) {
      message.error('Please add notes between 30-100 characters');
      return false;
    }
    if (mediaFiles.length === 0) {
      message.error('Please add at least one image');
      return false;
    }
    return true;
  };

  // ✅ Handle file data mapping
  const getFileData = (files) =>
    files?.filter((data) => data.awsURL)?.map(({ awsURL, type }) => ({ awsURL, type }));

  // ✅ Upload files to S3
  const uploadAllFiles = async () => {
    const updatedFiles = [...mediaFiles];
    for (let file of updatedFiles) {
      if (!file.awsURL && file.file) {
        try {
          const content_type = file.file.type.split('/')[1];
          const response = await getSignedUrl({ key: 'gallery', content_type });
          await uploadFileToSignedUrl(response.signedUrl, file.file, content_type);
          file.awsURL = response.fileLink;
        } catch (error) {
          console.error('File upload failed:', error);
        }
      }
    }
    return updatedFiles;
  };

  // ✅ Handle Create / Edit logic
  const handleSubmit = async () => {
    if (!handleValidate()) return;
    const uploadedFiles = await uploadAllFiles();

    const payload = {
      notes: `${notes} _${workingRate}`,
      category: category.toUpperCase(),
      files: getFileData(uploadedFiles)
    };

    if (option === 'Add') {
      dispatch(createNewGallery(payload));
    } else {
      dispatch(updateGallery({ ...payload, id: gallery._id }));
    }
  };

  // ✅ Handle Reset form data
  const handleReset = () => {
    setGalleryCategory('');
    setNotes('');
    setMediaFiles([]);
  };

  // ✅ Handle closing modal
  const handleCloseModal = () => {
    onCancel();
    handleReset();
  };

  // ✅ Handle file upload
  const handleFilesUploader = (files) => {
    const newFiles = Array.from(files)?.map((file) => ({
      url: URL.createObjectURL(file),
      awsURL: '',
      file: file,
      type: file.type.split('/')[0]
    }));
    setMediaFiles((prev) => [...prev, ...newFiles]);
  };

  // ✅ Handle file remove
  const handleRemoveImage = (index) => {
    setMediaFiles((prev) => prev.filter((_, i) => i !== index));
  };

  // ✅ Pre-fill form on Edit
  useEffect(() => {
    if (gallery) {
      setMediaFiles(
        gallery?.files?.length > 0
          ? gallery.files.map((file) => ({
              url: file.awsURL,
              awsURL: file.awsURL,
              file: null,
              type: file.type
            }))
          : []
      );
      setWorkingRate(gallery?.notes?.split('_')[1] || '');
      setNotes(gallery?.notes?.split('_')[0] || '');
      setGalleryCategory(gallery.category);
    }
  }, [gallery]);

  // ✅ Handle success/failure for Create
  useEffect(() => {
    if (successCreate) {
      message.success('Gallery added successfully!');
      dispatch({ type: CREATE_NEW_GALLERY_RESET });
      dispatch(getGalleryCategory());
      dispatch(getAllGallery());
      // dispatch(getGalleryListByShopId());
      handleReset();
    }

    if (errorCreate) {
      message.error(errorCreate);
      dispatch({ type: CREATE_NEW_GALLERY_RESET });
    }
  }, [successCreate, errorCreate]);

  // ✅ Handle success/failure for Update
  useEffect(() => {
    if (successUpdate) {
      message.success('Gallery updated successfully!');
      dispatch({ type: UPDATE_GALLERY_RESET });
      dispatch(getGalleryCategory());
      dispatch(getAllGallery());
      // dispatch(getGalleryListByShopId());
      handleCloseModal();
    }

    if (errorUpdate) {
      message.error(errorUpdate);
      dispatch({ type: UPDATE_GALLERY_RESET });
    }
  }, [successUpdate, errorUpdate]);

  // ✅ Fetch categories
  useEffect(() => {
    dispatch(getGalleryCategory());
  }, [dispatch]);

  return (
    <Modal footer={null} open={open} onCancel={handleCloseModal} width={'70vw'}>
      <div className="w-full">
        <h2 className="text-2xl font-semibold text-center">
          {option === 'Add' ? 'Add New Gallery' : 'Edit Gallery'}
        </h2>

        <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
          <div className="flex flex-col gap-2">
            <label className="block text-gray-700">Category</label>
            <SuggestionInput
              suggestions={galleryCategoryData || []}
              onChange={(value) => setGalleryCategory(value?.toUpperCase())}
              value={category || ''}
              placeholder="Gallery category"
            />
          </div>
          <div className="flex flex-col gap-2">
            <label className="block text-gray-700">Rate</label>
            <input
              type="text"
              name="rate"
              value={notes}
              onChange={(e) => setNotes(e.target.value)}
              className="w-full border rounded p-2 outline-none h-12"
              required
              placeholder="2,500/sq ft"
            />
          </div>
          <div className="flex flex-col gap-2">
            <label className="block text-gray-700">Add Notes</label>
            <input
              type="text"
              name="notes"
              placeholder={'Please Enter some notes'}
              value={workingRate}
              onChange={(e) => setWorkingRate(e.target.value)}
              className="w-full border rounded p-2 outline-none h-12"
              required
            />
          </div>
        </div>
        <div className="my-4">
          <label>Add Files</label>
          <div className="flex gap-4 flex-wrap">
            {mediaFiles?.map((file, index) => (
              <ImageView key={index} url={file.url} removeImage={() => handleRemoveImage(index)} />
            ))}
            <MultipleFileUploader handleFilesUploader={handleFilesUploader} fileType="both" />
          </div>
        </div>

        <div className="flex justify-end gap-4">
          <SaveButton label="Save" loading={false} onClick={handleSubmit} />
          <CloseButton onClick={handleCloseModal} />
        </div>
      </div>
    </Modal>
  );
};

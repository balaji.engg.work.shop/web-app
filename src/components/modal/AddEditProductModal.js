import React from 'react';
import { CloseButton } from '../InputComponents/MyButton';
import { Box, Flex, SimpleGrid, Text } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { Form, Input, message, Modal, Select } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { createNewProduct, getAllProduct, updateProduct } from '../../action/productActions';
import { CREATE_NEW_PRODUCT_RESET, UPDATE_PRODUCT_RESET } from '../../constants/productConstants';
import { addNewProductCategory, getAllProductCategory } from '../../action/productCategoryAction';
import { SaveButton } from '../InputComponents/MyButton';
import { productUnits } from '../../utils/dataUtiles';
import { CategoryManager } from '../InputComponents/CategoryManager';
import { weightTypeOptions } from '../../constantData';
import { MultipleFileUploader } from '../molecule/MultipleFileUploader';
import { getSignedUrl, uploadFileToSignedUrl } from '../../utils/s3Utils';
import ImageView from '../molecule/ImageView';

export const AddEditProductModal = ({ product, option = 'Add', onCancel, open }) => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const [productCategory, setProductCategory] = useState([]);
  const [priceType, setPriceType] = useState([]);
  const [mediaFiles, setMediaFiles] = useState([]);

  const getProductCategory = useSelector((state) => state.getProductCategory);
  const {
    error: errorProductCategory,
    loading: loadingProductCategory,
    success: successProductCategory,
    data: productCategoryData
  } = getProductCategory;

  const productUpdate = useSelector((state) => state.productUpdate);
  const {
    error: errorInUpdateProduct,
    loading: loadingUpdateProduct,
    success: successUpdateProduct
  } = productUpdate;

  const createProduct = useSelector((state) => state.createProduct);
  const {
    error: errorInCreateProduct,
    loading: loadingCreateProduct,
    success: successCreateProduct
  } = createProduct;

  const addProductCategory = (data) => {
    setProductCategory(data);
    dispatch(
      addNewProductCategory({
        productCategory: data,
        priceType: priceType
      })
    );
  };

  const addNewProduct = async (values) => {
    dispatch(
      createNewProduct({
        ...values,
        images: []
      })
    );
  };

  const getAllImagesURL = async () => {
    let myData = [...mediaFiles];
    for (let data of myData) {
      try {
        if (data.awsURL === '') {
          const content_type = data.file.type.split('/')[1];
          const response = await getSignedUrl({ key: 'productImages', content_type });
          const { fileLink, signedUrl } = response;
          await uploadFileToSignedUrl(signedUrl, data.file, content_type, () => {
            console.log('File uplaoding on AWS');
          });
          data.awsURL = fileLink;
        }
      } catch (error) {
        console.log('Error in upload file :', error);
      }
    }
    return myData;
  };

  const editProduct = async (values) => {
    let allImagesURL = await getAllImagesURL();
    allImagesURL = allImagesURL
      ?.filter((data) => data.awsURL?.length > 0)
      ?.map((data) => data.awsURL);
    dispatch(
      updateProduct({
        ...values,
        description: values.description.trim().toUpperCase(),
        _id: product?._id,
        images: allImagesURL
      })
    );
  };

  const onFinish = (values) => {
    if (option === 'Add') addNewProduct(values);
    else editProduct(values);
  };

  const onReset = () => {
    form.resetFields();
  };

  const handleCloseModel = () => {
    form.resetFields();
    onCancel();
  };

  useEffect(() => {
    if (product) {
      form.setFieldsValue(product);
    }
    setMediaFiles(
      product?.images?.map((url) => {
        return {
          url,
          awsURL: url,
          file: null
        };
      })
    );
  }, [product, form]);

  useEffect(() => {
    if (errorInUpdateProduct) {
      message.error(errorInUpdateProduct);
      dispatch({ type: UPDATE_PRODUCT_RESET });
    }
    if (successUpdateProduct) {
      message.success('Product updated successfully!');
      dispatch({ type: UPDATE_PRODUCT_RESET });
      dispatch(getAllProduct());
      handleCloseModel();
    }
  }, [errorInUpdateProduct, successUpdateProduct]);

  useEffect(() => {
    setProductCategory(productCategoryData?.productCategory);
    setPriceType(productCategoryData?.priceType);
  }, [successProductCategory, productCategoryData]);

  useEffect(() => {
    dispatch(getAllProductCategory());
  }, []);

  useEffect(() => {
    if (errorInCreateProduct) {
      message.error(errorInCreateProduct);
      dispatch({ type: CREATE_NEW_PRODUCT_RESET });
    }
    if (successCreateProduct) {
      onReset();
      message.success('Product added successfully!, add more products');
      dispatch({ type: CREATE_NEW_PRODUCT_RESET });
      dispatch(getAllProduct());
    }
  }, [successCreateProduct, errorInCreateProduct, dispatch]);

  const handleFilesUploader = async (files) => {
    if (files) {
      const mediaFilesArray = Array.isArray(mediaFiles) ? [...mediaFiles] : [];

      for (let file of Array.from(files)) {
        const url = URL.createObjectURL(file);
        const awsURL = '';
        mediaFilesArray.push({ url, awsURL, file });
      }
      setMediaFiles(mediaFilesArray);
    }
  };

  const handleRemoveImage = (index) => {
    setMediaFiles((pre) => pre.filter((_, i) => i != index));
  };

  return (
    <Modal
      style={{
        top: 20
      }}
      footer={[]}
      open={open}
      onCancel={handleCloseModel}
      width={'70vw'}>
      <Box>
        <Text color="#000000" fontSize="28px" align="center" fontWeight="600">
          {option === 'Add' ? 'Add New Product' : 'Edit Product'}
        </Text>
        <Form layout="vertical" form={form} onFinish={onFinish} autoComplete="off">
          <SimpleGrid columns={[1, 2, 3]} gap="4">
            <Form.Item
              label={<label style={{ color: '#000000' }}>HSN SAC Code</label>}
              name="HSN_SAC_Code"
              style={{ fontWeight: 'bold' }}>
              <Input placeholder="Enter unique HSN SAC CODE" size="large" type="number" />
            </Form.Item>
            <Form.Item
              style={{ fontWeight: 'bold' }}
              label={<label style={{ color: '#000000' }}>Category</label>}
              name="category"
              rules={[
                {
                  required: true,
                  message: 'Please Select category'
                }
              ]}>
              <CategoryManager
                options={productCategory}
                setOption={addProductCategory}
                placeholder="Select Product Category"
                componentName="category"
                name="category"
              />
            </Form.Item>
            <Form.Item
              style={{ fontWeight: 'bold' }}
              label={<label style={{ color: '#000000' }}>Product Name</label>}
              name="description"
              rules={[
                {
                  required: true,
                  message: 'Please input your description!'
                }
              ]}>
              <Input size="large" placeholder="Enter Product name" />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Brand Name</label>}
              name="brand"
              style={{ fontWeight: 'bold' }}>
              <Input placeholder="Enter product brand name" size="large" />
            </Form.Item>
            <Form.Item
              style={{ fontWeight: 'bold' }}
              label={<label style={{ color: '#000000' }}>Unit</label>}
              name="priceType"
              rules={[
                {
                  required: true,
                  message: 'Please enter product Per'
                }
              ]}>
              <Select
                size="large"
                showSearch
                options={productUnits}
                placeholder="Select product unit"
                filterOption={(input, option) =>
                  (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                }
              />
            </Form.Item>
            <Form.Item
              style={{ fontWeight: 'bold' }}
              label={<label style={{ color: '#000000' }}>Quantity</label>}
              name="quantity"
              rules={[
                {
                  required: true,
                  message: 'Please enter product quantity'
                }
              ]}>
              <Input type="number" size="large" />
            </Form.Item>
            <Form.Item
              style={{ fontWeight: 'bold' }}
              label={<label style={{ color: '#000000' }}>Purchasing Price</label>}
              name="purchasingPrice"
              rules={[
                {
                  required: false,
                  message: 'Please enter product price'
                }
              ]}>
              <Input type="number" size="large" />
            </Form.Item>
            <Form.Item
              style={{ fontWeight: 'bold' }}
              label={<label style={{ color: '#000000' }}>MRP</label>}
              name="MRP"
              rules={[
                {
                  required: false,
                  message: 'Please enter product price'
                }
              ]}>
              <Input type="number" size="large" />
            </Form.Item>
            <Form.Item
              style={{ fontWeight: 'bold' }}
              label={<label style={{ color: '#000000' }}>Selling Price</label>}
              name="sellingPrice"
              rules={[
                {
                  required: true,
                  message: 'Please enter product price'
                }
              ]}>
              <Input type="number" size="large" />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Weight Per Piece </label>}
              name="weightPerPiece"
              style={{ fontWeight: 'bold' }}>
              <Input type="number" size="large" />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Weight Unit </label>}
              name="weightUnit"
              style={{ fontWeight: 'bold' }}>
              <Select
                showSearch
                size="large"
                options={weightTypeOptions}
                placeholder="Select weight type "
                filterOption={(input, option) =>
                  (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                }
              />
            </Form.Item>
          </SimpleGrid>
          <div className="flex gap-4 flex-wrap">
            {mediaFiles?.map((data, index) => (
              <ImageView url={data.url} key={index} index={index} removeImage={handleRemoveImage} />
            ))}
            <MultipleFileUploader handleFilesUploader={handleFilesUploader} />
          </div>
          <Form.Item>
            <Flex justifyContent="end" gap="4">
              <SaveButton
                label="Save"
                loading={loadingCreateProduct || loadingUpdateProduct}
                loadingText={'Saving..'}
              />
              <CloseButton onClick={handleCloseModel} />
            </Flex>
          </Form.Item>
        </Form>
      </Box>
    </Modal>
  );
};

import { Flex, Box, Text, SimpleGrid } from '@chakra-ui/react';
import { useEffect } from 'react';
import { Form, Input, message, Modal, Select } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { IoMdCall } from 'react-icons/io';
import { MdOutlineMail } from 'react-icons/md';
import { CloseButton, SaveButton } from '../InputComponents/MyButton';
import { isValidEmail, isValidMobileNo } from '../../utils/validateData';
import { CREATE_NEW_USER_RESET, UPDATE_USER_RESET } from '../../constants/userConstants';
import { createNewUser, getAllUsers, updateUserDetailsByAdmin } from '../../action/userActions';

export const AddEditShopUserModal = ({ user, setCurrentUser, option = 'Add', onCancel, open }) => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const handleCloseModel = () => {
    form.resetFields();
    setCurrentUser();
    onCancel();
  };

  const updateUser = useSelector((state) => state.updateUser);
  const { error, loading, success } = updateUser;

  const createUser = useSelector((state) => state.createUser);
  const {
    error: errorUserCreate,
    loading: loadingUserCreate,
    success: successUserCreated
  } = createUser;

  //["Admin", "User", "SU"]
  const userRoleOptions = [
    {
      value: 'Admin',
      label: 'Admin'
    },
    {
      value: 'User',
      label: 'User'
    }
  ];

  useEffect(() => {
    if (error) {
      message.error(error);
      dispatch({ type: UPDATE_USER_RESET });
    }
    if (success) {
      message.success('User updated successFully!');
      dispatch({ type: UPDATE_USER_RESET });
      dispatch(getAllUsers());
      handleCloseModel();
    }
  }, [error, success]);

  useEffect(() => {
    if (errorUserCreate) {
      message.error(errorUserCreate);
    }
    if (successUserCreated) {
      message.success('User created successFully!');
      dispatch({ type: CREATE_NEW_USER_RESET });
      dispatch(getAllUsers());
      handleCloseModel();
    }
  }, [errorUserCreate, successUserCreated]);

  useEffect(() => {
    if (user) {
      form.setFieldsValue(user);
    }
  }, [user, form]);

  const onFinish = (values) => {
    if (option === 'Add') dispatch(createNewUser(values));
    else dispatch(updateUserDetailsByAdmin({ ...values, userId: user?.userId }));
  };

  const validateNumber = (_, value) => {
    return isValidMobileNo(value);
  };

  const validateEmail = (_, value) => {
    return isValidEmail(value);
  };

  return (
    <div>
      <Modal
        style={{
          top: 20
        }}
        footer={[]}
        open={open}
        onCancel={handleCloseModel}
        width={'70vw'}>
        <Box>
          <Text color="#000000" fontSize="28px" align="center" fontWeight="600">
            {option === 'Add' ? ' Add New Customer' : 'Edit Customer'}
          </Text>
          <Form layout="vertical" form={form} autoComplete="off" onFinish={onFinish}>
            <SimpleGrid columns={[1, 2, 3]} gap={{ base: '1', lg: '4' }}>
              <Form.Item
                label={<label style={{ color: '#000000' }}>Username</label>}
                name="userName"
                fontSize="40px"
                rules={[
                  {
                    required: true,
                    message: 'Please input your user name!'
                  }
                ]}>
                <Input placeholder="Enter unique user name" size="large" />
              </Form.Item>
              <Form.Item
                label={<label style={{ color: '#000000' }}>Password</label>}
                name="password"
                rules={[
                  {
                    required: true,
                    message: 'Please input your password!'
                  }
                ]}>
                <Input.Password size="large" disabled={option !== 'Add'} />
              </Form.Item>

              <Form.Item
                label={<label style={{ color: '#000000' }}>Email</label>}
                name="email"
                rules={[
                  { required: true, message: 'Please input your email!' },
                  { validator: validateEmail }
                ]}>
                <Input
                  type="email"
                  addonBefore={<MdOutlineMail />}
                  size="large"
                  placeholder="cccdcdcc@gmail.com"
                />
              </Form.Item>
              <Form.Item
                label={<label style={{ color: '#000000' }}>Full Name</label>}
                name="fullName"
                rules={[
                  {
                    required: true,
                    message: 'Please input your fullName!'
                  }
                ]}>
                <Input size="large" />
              </Form.Item>

              <Form.Item
                label={<label style={{ color: '#000000' }}>Mobile No</label>}
                name="mobileNo"
                rules={[
                  {
                    required: true,
                    message: 'Please input your mobileNo!',
                    maxLength: 10
                  },
                  {
                    validator: validateNumber
                  }
                ]}>
                <Input
                  addonBefore={<IoMdCall color="#000000" />}
                  maxLength={10}
                  size="large"
                  showCount={true}
                />
              </Form.Item>
              <Form.Item
                label={<label style={{ color: '#000000' }}>Aadhaar Number</label>}
                name="aadhaarNo"
                rules={[
                  {
                    required: true,
                    message: 'Please input your mobileNo!',
                    maxLength: 16
                  }
                ]}>
                <Input maxLength={16} size="large" showCount={true} />
              </Form.Item>
              <Form.Item
                label={<label style={{ color: '#000000' }}>User Role</label>}
                name="userRole"
                rules={[
                  {
                    required: true,
                    message: 'Please Select user roll',
                    maxLength: 10
                  }
                ]}>
                <Select
                  size="large"
                  showSearch
                  placeholder="Select a person"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                  }
                  options={userRoleOptions}
                />
              </Form.Item>
            </SimpleGrid>
            <Form.Item>
              <Flex justifyContent="end" gap="4">
                <SaveButton
                  label="Save"
                  loading={loadingUserCreate || loading}
                  loadingText={'Saving..'}
                />
                <CloseButton onClick={handleCloseModel} />
              </Flex>
            </Form.Item>
          </Form>
        </Box>
      </Modal>
    </div>
  );
};

import { Flex, Box, Text, SimpleGrid } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { Form, Input, message, Modal } from 'antd';
import { useForm } from 'antd/es/form/Form';
import { CloseButton, SaveButton } from '../InputComponents/MyButton';
import { useDispatch, useSelector } from 'react-redux';
import { IoMdCall } from 'react-icons/io';
import { MdOutlineMail } from 'react-icons/md';
import {
  isValidEmail,
  isValidGST,
  isValidMobileNo,
  isValidPinCode
} from '../../utils/validateData';
import { CREATE_NEW_SUPPLIER_RESET } from '../../constants/supplierConstants';
import { createNewSupplier, getAllSuppliers, updateSupplier } from '../../action/supplierAction';
import { UPDATE_SUPPLIER_RESET } from '../../constants/supplierConstants';

export const AddEditSupplierModel = ({
  setCurrentSupplier,
  supplier,
  option = 'Add',
  onCancel,
  open
}) => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const [fields, setFields] = useState([]);

  const createSupplier = useSelector((state) => state.createSupplier);
  const {
    error: errorInCreateSupplier,
    loading: loadingCreateSupplier,
    success: successCreateSupplier
  } = createSupplier;

  const supplierUpdate = useSelector((state) => state.supplierUpdate);
  const {
    error: errorInUpdateSupplier,
    loading: loadingUpdateSupplier,
    success: successUpdateSupplier
  } = supplierUpdate;

  const handleCloseModel = () => {
    setCurrentSupplier({});
    onCancel();
  };

  const onFinish = (values) => {
    if (option === 'Add') dispatch(createNewSupplier(values));
    else dispatch(updateSupplier({ ...values, supplierId: supplier?.supplierId }));
  };

  const validateNumber = (_, value) => {
    return isValidMobileNo(value);
  };

  const validateGSTIN = (_, value) => {
    return isValidGST(value);
  };

  const validateEmail = (_, value) => {
    return isValidEmail(value);
  };
  const validatePinCode = (_, value) => {
    return isValidPinCode(value);
  };

  useEffect(() => {
    if (errorInCreateSupplier) {
      message.error(errorInCreateSupplier);
      dispatch({ type: CREATE_NEW_SUPPLIER_RESET });
    }
    if (successCreateSupplier) {
      message.success('Supplier created successfully!');
      dispatch({ type: CREATE_NEW_SUPPLIER_RESET });
      dispatch(getAllSuppliers());
      handleCloseModel();
    }
  }, [errorInCreateSupplier, successCreateSupplier]);

  useEffect(() => {
    if (errorInUpdateSupplier) {
      message.error(errorInUpdateSupplier);
      dispatch({ type: UPDATE_SUPPLIER_RESET });
    }
    if (successUpdateSupplier) {
      message.success('SuccessFully Updated!');
      dispatch({ type: UPDATE_SUPPLIER_RESET });
      handleCloseModel();
    }
  }, [dispatch, errorInUpdateSupplier, successUpdateSupplier]);

  useEffect(() => {
    if (supplier) {
      form.setFieldsValue(supplier);
    }
  }, [supplier, form]);

  return (
    <Modal
      style={{
        top: 20
      }}
      footer={[]}
      open={open}
      onCancel={onCancel}
      width={'70vw'}>
      <Box>
        <Text color="#000000" fontSize="28px" align="center" fontWeight="600">
          {option === 'Add' ? ' Add Supplier' : 'Edit Supplier'}
        </Text>
        <Form autoComplete="off" onFinish={onFinish} layout="vertical" form={form}>
          <SimpleGrid columns={[1, 2, 3]} gap={{ base: '1', lg: '4' }}>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Supplier Name</label>}
              name="supplierName"
              fontSize="40px"
              rules={[
                {
                  required: true,
                  message: 'Please input your user name!'
                }
              ]}>
              <Input placeholder="Supplier name" size="large" />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Mobile No</label>}
              name="mobileNo"
              rules={[
                {
                  required: true,
                  message: 'Please input your mobileNo!',
                  maxLength: 10
                },
                {
                  validator: validateNumber
                }
              ]}>
              <Input
                addonBefore={<IoMdCall color="#000000" />}
                maxLength={10}
                size="large"
                showCount={true}
              />
            </Form.Item>

            <Form.Item
              label={<label style={{ color: '#000000' }}>Email</label>}
              name="email"
              rules={[
                {
                  validator: validateEmail
                }
              ]}>
              <Input
                type="email"
                addonBefore={<MdOutlineMail color="#000000" />}
                size="large"
                placeholder="cccdcdcc@gmail.com"
              />
            </Form.Item>
            <Form.Item label={<label style={{ color: '#000000' }}>Address</label>} name="address">
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>City</label>}
              name="city"
              fontSize="40">
              <Input size="large" />
            </Form.Item>

            <Form.Item
              label={<label style={{ color: '#000000' }}>State</label>}
              name="state"
              fontSize="40">
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>PinCode</label>}
              name="pinCode"
              fontSize="40"
              rules={[
                {
                  validator: validatePinCode
                }
              ]}>
              <Input size="large" maxLength="6" showCount />
            </Form.Item>

            <Form.Item
              label={
                <label
                  style={{ color: '#000000' }}
                  rules={[
                    {
                      validator: validateGSTIN
                    }
                  ]}>
                  GST No.
                </label>
              }
              name="gst">
              <Input maxLength={15} size="large" showCount={true} />
            </Form.Item>
          </SimpleGrid>
          <Form.Item>
            <Flex justifyContent="end" gap="4">
              <SaveButton
                label="Save"
                loading={loadingCreateSupplier || loadingUpdateSupplier}
                loadingText={'Saving..'}
              />
              <CloseButton onClick={handleCloseModel} />
            </Flex>
          </Form.Item>
        </Form>
      </Box>
    </Modal>
  );
};

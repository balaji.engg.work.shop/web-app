import { Button, Flex, Text } from '@chakra-ui/react';
import { Input, message, Modal, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import { addNewProductCategory, getAllProductCategory } from '../../action/productCategoryAction';
import { useDispatch, useSelector } from 'react-redux';
import { IoMdAdd } from 'react-icons/io';
import { RiDeleteBin6Fill } from 'react-icons/ri';
import { addProductInBulk } from '../../action/productActions';
import { ADD_PRODUCTS_IN_BULK_RESET } from '../../constants/productConstants';
import { productUnits } from '../../utils/dataUtiles';
import { CategoryManager } from '../InputComponents/CategoryManager';

const data = [
  {
    title: 'S.N.',
    width: '50px',
    required: false
  },
  {
    title: 'HSN SAC Code',
    width: '100px',
    required: true
  },
  {
    title: 'Category',
    width: '200px',
    required: true
  },
  {
    title: 'Product Name',
    width: '300px',
    required: true
  },
  {
    title: 'Brand Name',
    width: '200px',
    required: true
  },

  {
    title: 'Unit',
    width: '100px',
    required: true
  },
  {
    title: 'Quantity',
    width: '100px',
    required: true
  },
  {
    title: 'Purchasing Price',
    width: '100px',
    required: true
  },
  {
    title: 'MRP',
    width: '100px',
    required: true
  },
  {
    title: 'Selling Price',
    width: '100px',
    required: true
  },

  {
    title: 'Action',
    width: '100px',
    required: false
  }
];

export const AddProductInBulkModal = (props) => {
  const { open, onCancel } = props;
  const dispatch = useDispatch();
  const [productCategory, setProductCategory] = useState([]);
  const [priceType, setPriceType] = useState([]);
  const [products, setMyProducts] = useState([
    {
      sellingPrice: '',
      category: '',
      MRP: '',
      purchasingPrice: '',
      HSN_SAC_Code: '',
      description: '',
      priceType: '',
      quantity: '',
      brand: ''
    }
  ]);

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const addProducts = useSelector((state) => state.addProducts);
  const {
    error: errorAddProducts,
    loading: loadingAddProducts,
    success: successAddProducts,
    data: productList
  } = addProducts;

  const getProductCategory = useSelector((state) => state.getProductCategory);
  const {
    error: errorProductCategory,
    loading: loadingProductCategory,
    success: successProductCategory,
    data: productCategoryData
  } = getProductCategory;

  useEffect(() => {
    setProductCategory(
      productCategoryData?.productCategory?.map((data) => {
        return {
          label: data,
          value: data
        };
      })
    );
    setPriceType(
      productCategoryData?.priceType?.map((data) => {
        return {
          label: data,
          value: data
        };
      })
    );
  }, [successProductCategory, productCategoryData]);

  useEffect(() => {
    dispatch(getAllProductCategory());
  }, [props]);

  const handleRemoveRow = (index) => {
    const updatedRows = [...products];
    updatedRows.splice(index, 1);
    setMyProducts(updatedRows);
  };
  const handleAddRow = () => {
    setMyProducts([
      ...products,
      {
        sellingPrice: '',
        category: '',
        MRP: '',
        purchasingPrice: '',
        HSN_SAC_Code: '',
        description: '',
        priceType: '',
        quantity: '',
        brand: ''
      }
    ]);
  };

  function handleInputChange(event, index, field) {
    const updatedRows = [...products];
    updatedRows[index][field] = event.target.value;
    setMyProducts(updatedRows);
  }
  function handleSelectChange(value, index, field) {
    const updatedRows = [...products];
    updatedRows[index][field] = value;
    setMyProducts(updatedRows);
  }

  useEffect(() => {
    if (errorAddProducts) {
      message.error(errorAddProducts);
      dispatch({ type: ADD_PRODUCTS_IN_BULK_RESET });
    }
    if (productList) {
      message.success('Successfully added products');
      setTimeout(() => {
        onCancel();
      }, 0);
    }
  }, [errorAddProducts, productList]);

  useEffect(() => {
    setProductCategory(productCategoryData?.productCategory);
    setPriceType(productCategoryData?.priceType);
  }, [successProductCategory, productCategoryData]);

  useEffect(() => {
    dispatch(getAllProductCategory());
  }, [props]);

  const addProductCategory = (data) => {
    setProductCategory(data);
    dispatch(
      addNewProductCategory({
        productCategory: data,
        priceType: priceType
      })
    );
  };
  const addPriceType = (data) => {
    setPriceType(data);
    dispatch(
      addNewProductCategory({
        productCategory: productCategory,
        priceType: data
      })
    );
  };

  const handelSave = () => {
    dispatch(addProductInBulk(products));
  };

  return (
    <Modal
      title="Add multiple product at once"
      style={{
        top: 20
      }}
      open={open}
      onCancel={() => {
        onCancel();
      }}
      onOk={handelSave}
      okText="Save"
      cancelText="Close"
      width={'100vw'}>
      <div>
        <table>
          <tr>
            {data?.map((d) => (
              <th>
                <Text
                  width={d.width}
                  height="50px"
                  fontSize="sm"
                  color="#FFFFFF"
                  fontWeight="500"
                  border="1px"
                  borderColor="#000000"
                  align="center"
                  justifyContent="center"
                  px="2px"
                  py="2px"
                  bgColor="gray.600">
                  {d.title}
                </Text>
              </th>
            ))}
          </tr>

          {products.map((product, index) => (
            <tr>
              <td>
                <Input value={index + 1} style={{ width: data[0].width, borderRadius: 0 }} />
              </td>
              <td>
                <Input
                  style={{ width: data[1].width, borderRadius: 0 }}
                  value={product.HSN_SAC_Code}
                  onChange={(value) => handleInputChange(value, index, 'HSN_SAC_Code')}
                />
              </td>
              <td>
                <CategoryManager
                  style={{ width: data[2].width }}
                  size={'medium'}
                  options={productCategory}
                  setOption={addProductCategory}
                  value={products[index].category}
                  onChange={(value) => handleSelectChange(value, index, 'category')}
                  placeholder="Select Product Category"
                  componentName="category"
                  name="category"
                />
              </td>
              <td>
                <Input
                  value={product.description}
                  onChange={(value) => handleInputChange(value, index, 'description')}
                  style={{ width: data[3].width, borderRadius: 0 }}
                />
              </td>
              <td>
                <Input
                  style={{ width: data[4].width, borderRadius: 0 }}
                  value={product.brand}
                  onChange={(value) => handleInputChange(value, index, 'brand')}
                />
              </td>
              <td>
                <Select
                  style={{ width: data[5].width, borderRadius: 0 }}
                  showSearch
                  options={productUnits}
                  placeholder="Select product unit"
                  filterOption={(input, option) =>
                    (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                  }
                  value={products[index].priceType}
                  onChange={(value) => handleSelectChange(value, index, 'priceType')}
                />
              </td>
              <td>
                <Input
                  value={product.quantity}
                  onChange={(value) => handleInputChange(value, index, 'quantity')}
                  style={{ width: data[6].width, borderRadius: 0 }}
                />
              </td>{' '}
              <td>
                <Input
                  value={product.purchasingPrice}
                  onChange={(value) => handleInputChange(value, index, 'purchasingPrice')}
                  style={{ width: data[7].width, borderRadius: 0 }}
                />
              </td>
              <td>
                <Input
                  value={product.MRP}
                  onChange={(value) => handleInputChange(value, index, 'MRP')}
                  style={{ width: data[8].width, borderRadius: 0 }}
                />
              </td>
              <td>
                <Input
                  value={product.sellingPrice}
                  onChange={(value) => handleInputChange(value, index, 'sellingPrice')}
                  style={{ width: data[9].width, borderRadius: 0 }}
                />
              </td>
              <td>
                <Flex gap="2" align="center" p="1">
                  <Button
                    style={{ height: '32px' }}
                    size="md"
                    bgColor="green"
                    title="add more"
                    onClick={handleAddRow}>
                    <IoMdAdd color="#FFFFFF" />
                  </Button>
                  {products?.length > 1 && index != 0 ? (
                    <Button
                      style={{ height: '32px' }}
                      bgColor="red"
                      title="remove"
                      onClick={() => handleRemoveRow(index)}>
                      <RiDeleteBin6Fill color="#FFFFFF" size="16px" />
                    </Button>
                  ) : (
                    <Button
                      style={{ height: '32px' }}
                      bgColor="red"
                      title="remove"
                      isDisabled={true}
                      onClick={() => handleRemoveRow(index)}>
                      <RiDeleteBin6Fill color="#FFFFFF" size="16px" />
                    </Button>
                  )}
                </Flex>
              </td>
            </tr>
          ))}
        </table>
      </div>
    </Modal>
  );
};

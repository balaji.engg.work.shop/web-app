import { Button, Modal } from 'antd';
import { CloseButton, DownLoadButton } from '../InputComponents/MyButton';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { useReactToPrint } from 'react-to-print';
import { useRef } from 'react';

export const EstimateFormModel = ({ open, onCancel, products, customer, shop, sellDetails }) => {
  // console.log('products, customer, shop,sellDetails', products, customer, shop, sellDetails);
  const contentRef = useRef(null);
  const reactToPrintFn = useReactToPrint({ contentRef });

  const printDocument = () => {
    const input = document.getElementById('invoice');
    html2canvas(input).then((canvas) => {
      const imgData = canvas.toDataURL('image/png');
      const pdf = new jsPDF('p', 'mm', 'a4');
      const imgWidth = 210; // A4 width in mm
      const pageHeight = 295; // A4 height in mm
      const imgHeight = (canvas.height * imgWidth) / canvas.width;
      let heightLeft = imgHeight;

      let position = 0;
      pdf.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        pdf.addPage();
        pdf.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }

      pdf.save(`${customer?.customerName}-Estimate-${sellDetails?.billDate}.pdf`);
    });
  };

  const handlePrint = () => {
    var content = document.getElementById('invoice');
    var pri = document.getElementById('ifmcontentstoprint').contentWindow;
    pri.document.open();
    pri.document.write(content.innerHTML);
    pri.document.close();
    pri.focus();
    pri.print();
  };

  return (
    <div>
      <Modal
        style={{
          top: 20
        }}
        footer={[]}
        open={open}
        onCancel={onCancel}
        width={'70vw'}>
        <div className="p-4">
          <div className="flex gap-4 py-2">
            <CloseButton onClick={onCancel} />
            <DownLoadButton onClick={printDocument} />
            <Button onClick={reactToPrintFn}>Print</Button>
          </div>
          <div id="invoice" className="p-4" ref={contentRef}>
            <div className="border border-2 py-2 items-center">
              <h1 className="flex justify-center text-[24px] font-bold ">Estimate</h1>
            </div>
            <div className="border border-1 py-2">
              <h1 className="flex justify-center text-[20px] font-bold p-0 m-0">
                {shop?.shopName}
              </h1>
              <h1 className="flex justify-center text-[18px] font-semibold p-0 m-0">
                {shop?.address} , {shop?.city}, {shop?.state}, {shop?.pinCode}
              </h1>
            </div>
            <div className="border border-1 py-2 px-4">
              <div className="flex gap-4 ">
                <div className="flex flex-col">
                  <h1>Customer name:</h1>
                  <h1>Address:</h1>
                  <h1>Generated Date:</h1>
                </div>
                <div className="flex flex-col">
                  <h1>{customer?.customerName}</h1>
                  <h1>
                    {customer.address},{customer?.city}, {customer?.state}, {customer?.pinCode}
                  </h1>
                  <h1>{sellDetails?.billDate}</h1>
                </div>
              </div>
            </div>
            <div className="border border-1">
              <div className="grid grid-cols-6 bg-gray-200 py-2 px-2  text-[16px] font-semibold">
                <h1>S.N</h1>
                <h1>Product details</h1>
                <h1>Quantity</h1>
                <h1>Rate</h1>
                <h1>Per</h1>
                <h1>Total</h1>
              </div>

              {products?.map((product, index) => (
                <div className="grid grid-cols-6 py-2 px-2  text-[14px]">
                  <h1>{index + 1}</h1>
                  <h1>{product.description}</h1>
                  <h1>{product.quantity}</h1>
                  <h1>{product.price}</h1>
                  <h1>{product.priceType}</h1>
                  <h1>{product.total}</h1>
                </div>
              ))}
            </div>
            {new Array(10 - products?.length).map((value, index) => (
              <div className="grid grid-cols-6 py-2 px-2  text-[14px]"></div>
            ))}
            <div className="border border-1 grid grid-cols-3">
              <div className="flex flex-col">
                <div className="border border-1 items-center px-2 py-1">Total</div>
                <div className="border border-1 items-center px-2 py-1">{sellDetails?.total}</div>
              </div>
              {sellDetails?.isGST && (
                <div className="flex flex-col">
                  <div className="border border-1 items-center px-2 py-1">GST- 18%</div>
                  <div className="border border-1 items-center px-2 py-1">
                    {sellDetails?.gstAmount}
                  </div>
                </div>
              )}
              <div className="flex flex-col">
                <div className="border border-1 items-center px-2 py-1">Net Total</div>
                <div className="border border-1 items-center px-2 py-1">
                  {sellDetails?.netTotal}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
};

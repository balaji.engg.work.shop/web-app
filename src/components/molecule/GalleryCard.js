import React from 'react';
import { FiEdit2 } from 'react-icons/fi';

export const GalleryCard = ({ item, onEdit }) => {
  const defaultImage =
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTNNLEL-qmmLeFR1nxJuepFOgPYfnwHR56vcw&s';

  return (
    <div
      key={item._id}
      className="border rounded-lg shadow-sm p-3 bg-white transform transition-all hover:scale-105">
      <div
        className="absolute top-2 right-2 bg-white rounded-full p-2 shadow-md cursor-pointer hover:bg-gray-100 transition"
        onClick={() => onEdit(item)}>
        <FiEdit2 className="text-gray-600 hover:text-blue-500" size={18} />
      </div>
      <img
        src={item.files[0]?.awsURL || defaultImage}
        alt="Gallery"
        className="w-full h-40 object-cover rounded"
        style={{
          width: '100%',
          height: '170px',
          objectFit: 'cover',
          objectPosition: 'center'
        }}
        onError={(e) => (e.target.src = '/no-image.png')}
      />
      <div className="mt-2">
        <h3 className="text-lg font-semibold text-gray-800">{item.shop.shopName}</h3>
        <p className="text-sm text-gray-500">📞 {item.shop.shopMobileNo}</p>
        <p className="text-sm text-gray-500">
          📍 {item.shop.address}, {item?.shop?.city?.toUpperCase()},{' '}
          {item?.shop?.state?.toUpperCase()}
        </p>
        <p className="text-sm text-gray-600 mt-1">Category: {item.category}</p>
        <p className="text-sm text-gray-600">Notes: {item.notes}</p>
        <p className="text-sm text-gray-400 text-xs mt-1">
          📅 Created: {new Date(item.createdAt).toLocaleDateString()}
        </p>
      </div>
    </div>
  );
};

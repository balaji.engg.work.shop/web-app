import { Tooltip } from 'antd';
import React from 'react';

const ImageView = ({ url, removeImage, index }) => {
  return (
    <div className="relative inline-block rounded shadow-md ">
      <Tooltip title="remove image">
        <i
          onClick={() => removeImage(index)}
          className="fi fi-ss-circle-xmark absolute top-0 right-0 text-[#e74c3c] text-[20px] p-0 m-1"></i>
      </Tooltip>
      <img src={url} alt="image" className="h-24 w-24 rounded" />
    </div>
  );
};

export default ImageView;

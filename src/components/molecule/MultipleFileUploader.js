import { Tooltip } from 'antd';
import React, { useRef } from 'react';

export const MultipleFileUploader = ({ handleFilesUploader, fileType = 'image' }) => {
  const hiddenFileInput = useRef(null);

  const handleClick = () => {
    hiddenFileInput.current?.click();
  };

  const handleChange = (event) => {
    const fileUploaded = event.target.files || [];
    handleFilesUploader(fileUploaded);
  };

  const getContentType = () => {
    let type = '';
    if (fileType == 'image') {
      type = 'image/*';
    } else if (fileType == 'video') {
      type = 'video/*';
    } else {
      return type;
    }
    return type;
  };

  return (
    <div className="py-2">
      <Tooltip title="Upload Picture">
        <button
          type="button"
          className="p-2 border rounded bg-[#D3D3D3] text-white h-24 w-24 "
          onClick={handleClick}>
          <i className="fi fi-br-plus text-[20px]"></i>
        </button>
      </Tooltip>
      <input
        accept={getContentType()}
        title="upload"
        type="file"
        multiple
        onChange={handleChange}
        ref={hiddenFileInput}
        style={{ display: 'none' }}
      />
    </div>
  );
};

import { Popconfirm, Space, Skeleton } from 'antd';
import { AiFillEdit } from 'react-icons/ai';

import { RiDeleteBin5Fill } from 'react-icons/ri';
import React from 'react';

export const BankAccountTable = ({ handelEditBankAccount, data, confirm, loading }) => {
  return (
    <div className="h-[80vh] overflow-y-auto">
      {loading ? (
        <Skeleton />
      ) : (
        <table className="w-full">
          <thead className="">
            <tr className="bg-blue-500 text-white  text-[14px] font-bold px-2 ">
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-center">Sl.No.</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Bank Name</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Account No.</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">IFSC</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Holder Name</h1>
              </th>

              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Account Type</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Current Balance</h1>
              </th>

              <th className="py-2 font-semibold">
                <h1>Action</h1>
              </th>
            </tr>
          </thead>
          <tbody className="overflow-scroll">
            {data?.map((product, index) => (
              <tr key={index} className="border border-1 hover:bg-white hover:text-blue-600">
                <td className="py-2">
                  <h1 className=" flex justify-center">{index + 1}</h1>
                </td>

                <td className="py-2">
                  <h1 className=" flex justify-start">{product.bankName}</h1>
                </td>
                <td className="py-2">
                  <h1 className=" flex justify-start">{product.accountNumber}</h1>
                </td>
                <td className="py-2">
                  <h1 className=" flex justify-start">{product.ifscCode}</h1>
                </td>
                <td className="py-2">
                  <h1 className=" flex justify-start">{product.accountHolderName}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className=" flex justify-start truncate">{product.accountType}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className=" font-bold flex justify-start truncate">
                    {product.currentBalance}
                  </h1>
                </td>

                <td className="py-2 flex justify-center">
                  <Space size="middle">
                    <AiFillEdit size={20} onClick={() => handelEditBankAccount(product)} />
                    <Popconfirm
                      title="Delete product"
                      description="Are you sure to delete this product?"
                      onConfirm={(e) => confirm(e, product)}
                      okText="Yes"
                      cancelText="No">
                      <RiDeleteBin5Fill size={20} color="red" />
                    </Popconfirm>
                  </Space>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

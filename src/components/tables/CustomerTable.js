import { Popconfirm, Space, Skeleton, Button } from 'antd';
import { AiFillEdit } from 'react-icons/ai';

import { RiDeleteBin5Fill } from 'react-icons/ri';
import React from 'react';
import { CUSTOMERS_TRANSACTION } from '../../constants/routeConstant';
import { useNavigate } from 'react-router-dom';

export const CustomerTable = ({ handelEditCustomer, data, confirm, loading }) => {
  const navigate = useNavigate();
  return (
    <div className="h-[80vh] overflow-y-auto">
      {loading ? (
        <Skeleton />
      ) : (
        <table className="w-full">
          <thead className="">
            <tr className="bg-blue-500 text-white  text-[14px] font-bold px-2 ">
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-center">Sl.No.</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Customer Name</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Email</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">mobileNo</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">GST</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Address</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Total purchase amount</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1>Action</h1>
              </th>
            </tr>
          </thead>
          <tbody className="overflow-scroll">
            {data?.map((customer, index) => (
              <tr key={index} className="border border-1 hover:bg-white hover:text-blue-600">
                <td className="py-2">
                  <h1 className=" flex justify-center">{index + 1}</h1>
                </td>

                <td className="py-2 truncate">
                  <h1 className=" flex justify-start">{customer.customerName}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className=" flex justify-start">{customer.email || 'N/A'}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className=" flex justify-start">{customer.mobileNo || 'N/A'}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className=" flex justify-start">{customer.gst || 'N/A'}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className="font-bold flex justify-start truncate">
                    {customer.address || 'N/A'}
                  </h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className=" flex justify-start truncate">
                    {customer.totalPurchaseAmount || 0}
                  </h1>
                </td>
                <td className="py-2 flex justify-center">
                  <Space size="middle">
                    <Button
                      onClick={() => {
                        navigate(CUSTOMERS_TRANSACTION, {
                          state: { customer }
                        });
                      }}>
                      Details
                    </Button>
                    <AiFillEdit size={20} onClick={() => handelEditCustomer(customer)} />
                    <Popconfirm
                      title="Delete customer"
                      description="Are you sure to delete this customer?"
                      onConfirm={(e) => confirm(e, customer)}
                      okText="Yes"
                      cancelText="No">
                      <RiDeleteBin5Fill size={20} color="red" />
                    </Popconfirm>
                  </Space>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

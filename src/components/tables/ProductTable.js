import { Popconfirm, Space, Skeleton } from 'antd';
import { AiFillEdit } from 'react-icons/ai';
import { message } from 'antd';
import { RiDeleteBin5Fill } from 'react-icons/ri';
import React from 'react';

export const ProductTable = ({ handelEditProduct, data, confirm, loading }) => {
  return (
    <div className="max-h-[80vh] overflow-y-auto">
      {loading ? (
        <Skeleton />
      ) : (
        <table className="w-full">
          <thead className="">
            <tr className="bg-blue-500 text-white  text-[14px] font-bold px-2 ">
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-center">Sl.No.</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Product code</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Image</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Category</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Product Name</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Current Stock</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Per</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className="flex justify-start">Price</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1>Action</h1>
              </th>
            </tr>
          </thead>

          <tbody className="h-[80vh] overflow-y-auto">
            {data?.map((product, index) => (
              <tr key={index} className="border border-1 hover:bg-white hover:text-blue-600 ">
                <td className="py-2">
                  <h1 className=" flex justify-center">{index + 1}</h1>
                </td>

                <td className="py-2">
                  <h1 className=" flex justify-start">{product.productCode}</h1>
                </td>
                <td className="py-2">
                  <i
                    onClick={() => {
                      if (product.images.length > 0) window.open(product.images[0]);
                      else message.error('Image not upload yet');
                    }}
                    className={`fi  ${product.images.length ? `fi-br-picture text-[#228B22]` : `fi-rr-picture text-[#B2BEB5]`}`}></i>
                </td>
                <td className="py-2">
                  <h1 className=" flex justify-start">{product.category}</h1>
                </td>
                <td className="py-2">
                  <h1 className=" flex justify-start">{product.description}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className=" font-bold flex justify-start truncate">{product.quantity}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className=" flex justify-start truncate">{product.priceType}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className=" flex justify-start truncate">{product.sellingPrice}</h1>
                </td>
                <td className="py-2 flex justify-center">
                  <Space size="middle">
                    <AiFillEdit size={20} onClick={() => handelEditProduct(product)} />
                    <Popconfirm
                      title="Delete product"
                      description="Are you sure to delete this product?"
                      onConfirm={(e) => confirm(e, product)}
                      okText="Yes"
                      cancelText="No">
                      <RiDeleteBin5Fill size={20} color="red" />
                    </Popconfirm>
                  </Space>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

import { Popconfirm, Space, Skeleton } from 'antd';
import { AiFillEdit } from 'react-icons/ai';
import { RiDeleteBin5Fill } from 'react-icons/ri';
import React from 'react';

export const ShopUserTable = ({ handelEditUser, data, confirm, loading }) => {
  return (
    <div className="h-[80vh] overflow-y-auto">
      {loading ? (
        <Skeleton />
      ) : (
        <table className="w-full">
          <thead className="">
            <tr className="bg-blue-500 text-white  text-[14px] font-bold px-2 ">
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-center">Sl.No.</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Full Name</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">User Name</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Email</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Mobile No</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">User Role</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Status</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1>Action</h1>
              </th>
            </tr>
          </thead>
          <tbody className="overflow-scroll">
            {data?.map((user, index) => (
              <tr key={index} className="border border-1 hover:bg-white hover:text-blue-600">
                <td className="py-2">
                  <h1 className=" flex justify-center">{index + 1}</h1>
                </td>

                <td className="py-2">
                  <h1 className=" flex justify-start">{user.fullName}</h1>
                </td>
                <td className="py-2">
                  <h1 className=" flex justify-start">{user.userName || 'N/A'}</h1>
                </td>
                <td className="py-2">
                  <h1 className=" flex justify-start">{user.email}</h1>
                </td>
                <td className="py-2">
                  <h1 className=" flex justify-start">{user.mobileNo}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className=" font-bold flex justify-start truncate">{user.userRole}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className="flex justify-start truncate">
                    {user.isActive ? 'Active' : 'In-Active'}
                  </h1>
                </td>
                <td className="py-2 flex justify-center">
                  <Space size="middle">
                    <AiFillEdit size={20} color="#000000" onClick={() => handelEditUser(user)} />

                    <Popconfirm
                      disabled={user?.userRole == 'Admin'}
                      title="Delete user"
                      description="Are you sure to delete this user?"
                      onConfirm={(e) => confirm(e, user)}
                      okText="Yes"
                      cancelText="No">
                      <RiDeleteBin5Fill size={20} color={user?.userRole == 'Admin' ? '' : 'red'} />
                    </Popconfirm>
                  </Space>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

import { Space, Skeleton, Button } from 'antd';
import { AiFillEdit } from 'react-icons/ai';
import React from 'react';
import { SUPPLIER_TRANSACTION } from '../../constants/routeConstant';
import { useNavigate } from 'react-router-dom';

export const SupplierTable = ({ handleEditSupplier, data, loading }) => {
  const navigate = useNavigate();
  return (
    <div className="h-[80vh] overflow-y-auto">
      {loading ? (
        <Skeleton />
      ) : (
        <table className="w-full">
          <thead className="">
            <tr className="bg-blue-500 text-white  text-[14px] font-bold px-2 ">
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-center">Sl.No.</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Supplier Name</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Email</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">mobileNo</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">GST</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Address</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-start">Total purchase amount</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1>Action</h1>
              </th>
            </tr>
          </thead>
          <tbody className="overflow-scroll">
            {data?.map((supplier, index) => (
              <tr key={index} className="border border-1 hover:bg-white hover:text-blue-600">
                <td className="py-2">
                  <h1 className=" flex justify-center">{index + 1}</h1>
                </td>

                <td className="py-2">
                  <h1 className=" flex justify-start">{supplier.supplierName}</h1>
                </td>
                <td className="py-2">
                  <h1 className=" flex justify-start">{supplier.email || 'N/A'}</h1>
                </td>
                <td className="py-2">
                  <h1 className=" flex justify-start">{supplier.mobileNo}</h1>
                </td>
                <td className="py-2">
                  <h1 className=" flex justify-start">{supplier.gst}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className=" font-bold flex justify-start truncate">{supplier.address}</h1>
                </td>
                <td className="py-2 truncate">
                  <h1 className=" flex justify-start truncate">{supplier.totalPurchaseAmount}</h1>
                </td>
                <td className="py-2 flex justify-center">
                  <Space size="middle">
                    <AiFillEdit size={20} onClick={() => handleEditSupplier(supplier)} />
                    <Button
                      onClick={() =>
                        navigate(SUPPLIER_TRANSACTION, {
                          state: {
                            supplierId: supplier.supplierId,
                            supplier: supplier
                          }
                        })
                      }>
                      Details
                    </Button>
                  </Space>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

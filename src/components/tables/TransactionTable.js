import { Skeleton, Button } from 'antd';
import React from 'react';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { SELL_PRODUCT_DETAILS } from '../../constants/routeConstant';
import { getSellProductDetails } from '../../action/sellProductAction';
import { RiDeleteBin5Fill } from 'react-icons/ri';

export const TransactionTable = ({ data, loading, deleteTransaction, getDetail }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  return (
    <div className="h-[80vh] overflow-y-auto">
      {loading ? (
        <Skeleton />
      ) : (
        <table className="w-full">
          <thead>
            <tr className="bg-blue-500 text-white  text-[14px] font-bold ">
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-center">Sl.No.</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-center">Transaction Date</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-center">Bill No.</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-end">Bill Amount</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-end">Paid Amount</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-center">Mode of payment</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-center">Transaction Type</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1 className=" flex justify-center">Staff Name</h1>
              </th>
              <th className="py-2 font-semibold">
                <h1>Action</h1>
              </th>
            </tr>
          </thead>
          <tbody className="overflow-scroll">
            {data?.map((singleData, index) => (
              <tr key={index} className="border border-1 hover:bg-white hover:text-blue-600">
                <td className="p-2">
                  <h1 className=" flex justify-center">{index + 1}</h1>
                </td>

                <td className="p-2">
                  <h1 className=" flex justify-center">
                    {moment(singleData.transactionDate).format('DD-MM-YYYY')}
                  </h1>
                </td>
                <td className="p-2">
                  <h1 className=" flex justify-center">{singleData.billNo}</h1>
                </td>
                <td className="p-2">
                  <h1 className=" flex justify-end">{singleData.billAmount}</h1>
                </td>
                <td className="p-2">
                  <h1 className=" flex justify-end">{singleData.paidAmount}</h1>
                </td>
                <td className="p-2 truncate">
                  <h1 className=" font-bold flex justify-center truncate">{singleData.mot}</h1>
                </td>
                <td className="p-2 truncate">
                  <h1 className=" flex justify-center truncate">{singleData.transactionType}</h1>
                </td>
                <td className="p-2 truncate">
                  <h1 className=" flex justify-center truncate">{singleData.userName}</h1>
                </td>
                <td className="p-2 flex justify-center gap-2">
                  <Button onClick={() => getDetail(singleData?.billNo)}>Details</Button>
                  <RiDeleteBin5Fill
                    size={20}
                    color="red"
                    onClick={() => deleteTransaction(singleData?._id)}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

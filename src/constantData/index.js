export const accountType = [
  {
    value: 'Saving',
    label: 'Saving'
  },
  {
    value: 'Current',
    label: 'Current'
  }
];

export const bankName = [
  {
    value: 'ICICI',
    label: 'ICICI'
  },
  {
    value: 'PNB',
    label: 'PNB'
  },
  {
    value: 'SBI',
    label: 'SBI'
  },
  {
    value: 'HDFC',
    label: 'HDFC'
  },
  {
    value: 'AXIS',
    label: 'AXIS'
  },
  {
    value: 'BANDHAN',
    label: 'BANDHAN'
  },
  {
    value: 'IDBI',
    label: 'IDBI'
  }
];

export const weightTypeOptions = [
  {
    value: 'Gram',
    label: 'Gram'
  },
  {
    value: 'Kg',
    label: 'Kg'
  },
  {
    value: 'Quintal',
    label: 'Quintal (100 Kg)'
  },
  {
    value: 'Tone',
    label: 'Tone (1000 Kg)'
  },
  {
    value: 'ML',
    label: 'ML'
  },
  {
    value: 'Litter',
    label: 'Litter'
  }
];

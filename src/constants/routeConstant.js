export const SIGN_IN = '/sign-in';
export const SIGN_UP = '/sign-up';
export const DASHBOARD = '/dashboard';
export const ROOT = '/';
export const USERS = '/user';
export const Edit_USER = '/user/edit';
export const PRODUCTS = '/products';
export const ACCOUNT = '/account';
export const SHOP = '/shop';
export const BANK_ACCOUNT = '/back-account';
export const GALLERY = '/gallery';

export const DAILY_MANUFACTURE = '/daily-manufacture';
export const CUSTOMERS = '/customers';
export const CUSTOMERS_TRANSACTION = '/customers/transaction';
export const SUPPLIER = '/supplier';
export const EDIT_SUPPLIER = '/supplier/edit';
export const SUPPLIER_TRANSACTION = '/supplier/transaction';

export const EDIT_CUSTOMER = '/edit-customer';
export const SELL_PRODUCTS = '/sell-products';
export const BUY_PRODUCTS = '/buy-products';

export const ESTIMATE = '/estimate';

export const SELL_PRODUCT_DETAILS = '/sell-details';
export const PURCHASE_PRODUCT_DETAILS = '/purchase-details';
export const SELL_RETURN_PRODUCT_DETAILS = '/sellReturn-details';
export const FORGET_PASSWORD = '/forget-password';
export const CHANGE_PASSWORD = '/change-password/:email/:name';

export const SELL_TRANSACTION = '/sell-transactions';
export const SELL_RETURN = '/sell-return';

export const BUY_TRANSACTION = 'buy-transaction';
export const SETTING = '/setting';
export const NOT_FOUND = '*';

export const ADD_NEW_SELL_PRODUCTS_DATA_REQUEST = 'ADD_NEW_SELL_PRODUCTS_DATA_REQUEST';
export const ADD_NEW_SELL_PRODUCTS_DATA_SUCCESS = 'ADD_NEW_SELL_PRODUCTS_DATA_SUCCESS';
export const ADD_NEW_SELL_PRODUCTS_DATA_FAIL = 'ADD_NEW_SELL_PRODUCTS_DATA_FAIL';
export const ADD_NEW_SELL_PRODUCTS_DATA_RESET = 'ADD_NEW_SELL_PRODUCTS_DATA_RESET';

export const GET_COUNT_DATA_REQUEST = 'GET_COUNT_DATA_REQUEST';
export const GET_COUNT_DATA_SUCCESS = 'GET_COUNT_DATA_SUCCESS';
export const GET_COUNT_DATA_FAIL = 'GET_COUNT_DATA_FAIL';
export const GET_COUNT_DATA_RESET = 'GET_COUNT_DATA_RESET';

export const GET_SELL_PRODUCTS_DETAIL_REQUEST = 'GET_SELL_PRODUCTS_DETAIL_REQUEST';
export const GET_SELL_PRODUCTS_DETAIL_SUCCESS = 'GET_SELL_PRODUCTS_DETAIL_SUCCESS';
export const GET_SELL_PRODUCTS_DETAIL_FAIL = 'GET_SELL_PRODUCTS_DETAIL_FAIL';
export const GET_SELL_PRODUCTS_DETAIL_RESET = 'GET_SELL_PRODUCTS_DETAIL_RESET';

export const DELETE_SELL_PRODUCTS_DETAIL_REQUEST = 'DELETE_SELL_PRODUCTS_DETAIL_REQUEST';
export const DELETE_SELL_PRODUCTS_DETAIL_SUCCESS = 'DELETE_SELL_PRODUCTS_DETAIL_SUCCESS';
export const DELETE_SELL_PRODUCTS_DETAIL_FAIL = 'DELETE_SELL_PRODUCTS_DETAIL_FAIL';
export const DELETE_SELL_PRODUCTS_DETAIL_RESET = 'DELETE_SELL_PRODUCTS_DETAIL_RESET';

import React, { useEffect, useState } from 'react';
import { Box, Text, Button } from '@chakra-ui/react';
import { Form, Input, message } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { changePassword } from '../../action/userActions';
import { SIGN_IN } from '../../constants/routeConstant';
import { USER_UPDATE_PASSWORD_RESET } from '../../constants/userConstants';

export function ChangePassword(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const name = window.location.pathname.split('/')[3];
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const passwordChange = useSelector((state) => state.passwordChange);
  const { loading, error, success } = passwordChange;

  useEffect(() => {
    if (success) {
      message.success('Password changed, Sign in with new password!');
      dispatch({ type: USER_UPDATE_PASSWORD_RESET });
      setTimeout(() => {
        navigate(SIGN_IN);
      }, 1000);
    }
    if (error) {
      message.error(error);
      dispatch({ type: USER_UPDATE_PASSWORD_RESET });
      setPassword('');
      setConfirmPassword('');
    }
  }, [success, error, navigate]);

  const onFinish = (values) => {
    if (password === confirmPassword) {
      if (password.length >= 8) {
        dispatch(changePassword({ email, newPassword: password }));
      } else {
        message.error('Password length atleast 8 characters');
      }
    } else {
      message.error('Password mismatched!, try again');
    }
  };

  const onFinishFailed = (errorInfo) => {};

  useEffect(() => {
    const email = window.location.pathname.split('/')[2];
    setEmail(email);
  }, [dispatch]);

  return (
    <Box
      minH="100vh"
      display="flex"
      alignItems="center"
      justifyContent="center"
      bgGradient="linear(to-br, #E0C3FC, #8EC5FC)" // Matching gradient background
    >
      <Box
        width={{ base: '90%', lg: '500px' }}
        borderRadius="15px"
        boxShadow="lg"
        bgColor="#FFFFFF"
        px={{ base: '5', lg: '8' }}
        py={{ base: '6', lg: '10' }}>
        <Text color="#333333" fontSize={{ base: 'xl', lg: '2xl' }} fontWeight="semibold" m="0">
          Create new password
        </Text>
        <Text color="#5B5B5B" fontSize={{ base: 'sm', lg: 'md' }}>
          Password must be at least 8 characters long.
        </Text>
        <Form
          layout="vertical"
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          fields={[
            {
              name: ['password'],
              value: password
            },
            {
              name: ['confirmPassword'],
              value: confirmPassword
            }
          ]}>
          <Form.Item
            label={
              <Text fontSize={{ base: 'lg', lg: 'xl' }} m="0" color="#333333">
                New password
              </Text>
            }
            name="password"
            rules={[{ required: true, message: 'Please input your password!' }]}>
            <Input.Password
              placeholder="New password"
              onChange={(e) => setPassword(e.target.value)}
              size="large"
              minLength={8}
            />
          </Form.Item>
          <Form.Item
            label={
              <Text fontSize={{ base: 'lg', lg: 'xl' }} m="0" color="#333333">
                Confirm password
              </Text>
            }
            name="confirmPassword"
            rules={[
              {
                required: true,
                message: 'Please input your confirm password!'
              }
            ]}>
            <Input.Password
              placeholder="Confirm password"
              onChange={(e) => setConfirmPassword(e.target.value)}
              size="large"
              minLength={8}
            />
          </Form.Item>

          <Form.Item>
            <Button
              type="submit"
              py="3"
              bgColor="#6A0572" // Consistent button color with Sign In page
              color="#FFFFFF"
              _hover={{ bgColor: '#540465' }}
              width="100%"
              isLoading={loading}
              loadingText="Saving password">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Box>
    </Box>
  );
}

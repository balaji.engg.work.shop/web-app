import { AbsoluteCenter, Box, Button, Divider, Flex, Stack, Text } from '@chakra-ui/react';
import { Form, Input, message } from 'antd';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { createNewShop } from '../../action/shopActions';
import { CREATE_NEW_SHOP_RESET } from '../../constants/shopConstants';
import { SIGN_IN } from '../../constants/routeConstant';
import { isValidEmail, isValidMobileNo } from '../../utils/validateData';

export function CreateAccount(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const addShop = useSelector((state) => state.addShop);
  const { error, success, loading: loadingAddShop } = addShop;

  useEffect(() => {
    if (success) {
      message.success('Shop created successfully!');
      dispatch({ type: CREATE_NEW_SHOP_RESET });
      setTimeout(() => {
        navigate(SIGN_IN);
      }, 1000);
    }
    if (error) {
      message.error(error);
      dispatch({ type: CREATE_NEW_SHOP_RESET });
    }
  }, [success, error, dispatch, navigate]);

  const handleSignUp = (values) => {
    if (values.password !== values.confirmPassword) {
      message.error('Password and confirm password mismatch!');
    } else {
      dispatch(createNewShop(values));
    }
  };

  const validateNumber = (_, value) => {
    return isValidMobileNo(value);
  };

  const validateEmail = (_, value) => {
    return isValidEmail(value);
  };

  return (
    <Box
      minH="100vh"
      display="flex"
      alignItems="center"
      justifyContent="center"
      bgGradient="linear(to-br, #E0C3FC, #8EC5FC)" // Matching gradient background
    >
      <Box
        width={{ base: '90%', lg: '500px' }}
        borderRadius="15px"
        boxShadow="lg"
        bgColor="#FFFFFF"
        px={{ base: '5', lg: '8' }}
        py={{ base: '6', lg: '10' }}>
        <Stack spacing={4}>
          <Text
            align="center"
            fontSize={{ base: 'lg', lg: '2xl' }}
            fontWeight="bold"
            color="#333333">
            Create New Account
          </Text>
          <Form
            name="basic"
            initialValues={{ remember: true }}
            autoComplete="off"
            layout="vertical"
            onFinish={handleSignUp}
            style={{ gap: '10px' }}>
            <Form.Item
              label={
                <Text color="#555555" fontSize="md" m="0" fontWeight="600">
                  Shop Name:
                </Text>
              }
              name="shopName"
              rules={[{ required: true, message: 'Please input your Shop Name!' }]}
              style={{ marginBottom: '12px' }}>
              <Input size="large" placeholder="Enter Shop Name" />
            </Form.Item>
            <Form.Item
              label={
                <Text color="#555555" fontSize="md" m="0" fontWeight="600">
                  Owner Name:
                </Text>
              }
              name="ownerName"
              rules={[{ required: true, message: 'Please input the Owner Name!' }]}
              style={{ marginBottom: '12px' }}>
              <Input size="large" placeholder="Enter Owner Name" />
            </Form.Item>
            <Form.Item
              label={
                <Text color="#555555" fontSize="md" m="0" fontWeight="600">
                  Mobile Number:
                </Text>
              }
              name="mobileNo"
              rules={[
                { required: true, message: 'Please input your Mobile number!' },
                { validator: validateNumber }
              ]}
              style={{ marginBottom: '12px' }}>
              <Input size="large" placeholder="Enter Mobile Number" maxLength="10" />
            </Form.Item>
            <Form.Item
              label={
                <Text color="#555555" fontSize="md" m="0" fontWeight="600">
                  Email:
                </Text>
              }
              name="email"
              rules={[
                { required: true, message: 'Please input your Email' },
                { validator: validateEmail }
              ]}
              style={{ marginBottom: '12px' }}>
              <Input size="large" placeholder="Enter Email" />
            </Form.Item>
            <Form.Item
              label={
                <Text color="#555555" fontSize="md" m="0" fontWeight="600">
                  Password:
                </Text>
              }
              name="password"
              rules={[{ required: true, message: 'Please input your password!' }]}
              style={{ marginBottom: '12px' }}>
              <Input.Password size="large" placeholder="Enter Password" />
            </Form.Item>
            <Form.Item
              label={
                <Text color="#555555" fontSize="md" m="0" fontWeight="600">
                  Confirm Password:
                </Text>
              }
              name="confirmPassword"
              rules={[{ required: true, message: 'Please input your confirm password!' }]}
              style={{ marginBottom: '20px' }} // Added more space for better visual balance
            >
              <Input.Password size="large" placeholder="Confirm Password" />
            </Form.Item>
            <Form.Item>
              <Button
                type="submit"
                py="3"
                bgColor="#6A0572" // Consistent button color with Sign In page
                color="#FFFFFF"
                _hover={{ bgColor: '#540465' }}
                width="100%"
                isLoading={loadingAddShop}
                loadingText="Creating...">
                Sign Up
              </Button>
            </Form.Item>
            <Flex align="center" justify="center" gap="2" pt="4">
              <Text color="#000000" fontWeight="600" fontSize="sm">
                Already have an account ?
              </Text>
              <Text
                color="blue.600"
                fontWeight="600"
                fontSize="sm"
                textUnderlineOffset="true"
                cursor="pointer"
                onClick={() => navigate(SIGN_IN)}>
                Login Now
              </Text>
            </Flex>
          </Form>
        </Stack>
      </Box>
    </Box>
  );
}

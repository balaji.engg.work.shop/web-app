import React, { useEffect, useState } from 'react';
import { Box, Center, Stack, Text, Button, Link, Flex } from '@chakra-ui/react';
import { Form, Input, message } from 'antd';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { sendEmailToUser } from '../../action/userActions';
import { SEND_EMAIL_TO_USER_RESET } from '../../constants/userConstants';
import { SIGN_IN } from '../../constants/routeConstant';

export function ForgetPassword(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');

  //userSignup;
  const sendEmail = useSelector((state) => state.sendEmail);
  const { message: resMessage, loading, error, success } = sendEmail;

  useEffect(() => {
    if (success) {
      message.success(resMessage);
      dispatch({ type: SEND_EMAIL_TO_USER_RESET });
    }
    if (error) {
      message.error(error);
      setEmail('');
      dispatch({ type: SEND_EMAIL_TO_USER_RESET });
    }
  }, [success, error]);

  const onFinish = (values) => {
    dispatch(
      sendEmailToUser({
        email
      })
    );
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Box
      minH="100vh"
      display="flex"
      alignItems="center"
      justifyContent="center"
      bgGradient="linear(to-br, #E0C3FC, #8EC5FC)">
      <Box
        width={{ base: '90%', lg: '400px' }}
        borderRadius="15px"
        boxShadow="lg"
        bgColor="#FFFFFF"
        px={{ base: '5', lg: '8' }}
        py={{ base: '6', lg: '10' }}>
        <Text align="center" fontSize={{ base: 'lg', lg: '2xl' }} fontWeight="bold" color="#333333">
          Forget password
        </Text>

        {success && (
          <Link color="teal.500" href="https://mail.google.com/mail/u/0/?tab=rm&ogbl#inbox">
            click hear to inbox
          </Link>
        )}

        <Form
          layout="vertical"
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          fields={[
            {
              name: ['email'],
              value: email
            }
          ]}>
          <Form.Item
            label={
              <Text fontSize={{ base: 'lg', lg: 'xl' }} m="0" color="#333333">
                Enter email
              </Text>
            }
            name="email"
            rules={[{ required: true, message: 'Please input your email!' }, { type: 'email' }]}>
            <Input
              placeholder="Enter your email"
              onChange={(e) => setEmail(e.target.value)}
              size="large"
            />
          </Form.Item>

          <Form.Item>
            <Button
              width="100%"
              type="submit"
              bgColor="#6A0572"
              color="#FFFFFF"
              _hover={{ bgColor: '#540465' }}
              fontSize="lg"
              isLoading={loading}
              loadingText="Please wait...">
              Send email
            </Button>
          </Form.Item>
        </Form>

        <Stack pt="5">
          <Button
            py="3"
            width="100%"
            bgColor="#FFFFFF"
            color="#2C2C2C"
            borderRadius="12px"
            fontWeight="700"
            border="1px"
            onClick={() => navigate('/signUp')}
            _hover={{ color: '#FFFFFF', bgColor: '#2C2C2C' }}>
            Create a new account
          </Button>
        </Stack>
        <Flex align="center" justify="center" gap="2" pt="4">
          <Text color="#000000" fontWeight="600" fontSize="sm">
            Already have an account ?
          </Text>
          <Text
            color="blue.600"
            fontWeight="600"
            fontSize="sm"
            textUnderlineOffset="true"
            cursor="pointer"
            onClick={() => navigate(SIGN_IN)}>
            Login Now
          </Text>
        </Flex>
      </Box>
    </Box>
  );
}

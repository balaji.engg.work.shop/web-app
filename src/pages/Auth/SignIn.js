import { AbsoluteCenter, Box, Button, Divider, Flex, Stack, Text } from '@chakra-ui/react';
import { Checkbox, Form, Input, message } from 'antd';
import { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { AiOutlineUser } from 'react-icons/ai';
import { useDispatch, useSelector } from 'react-redux';
import { googleSignIn, signIn } from '../../action/userActions';
import { FORGET_PASSWORD, ROOT, SIGN_UP } from '../../constants/routeConstant';
import { LoginWithGoogle } from '../../components/GoogleAuth';

export function SignIn() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');

  const userSignIn = useSelector((state) => state.userSignIn);
  const { loading, error, success } = userSignIn;

  useEffect(() => {
    if (success) {
      navigate(ROOT);
    }
    if (error) {
      message.error(error);
    }
  }, [error, success, navigate]);

  const onFinish = () => {
    dispatch(signIn(userName, password));
  };

  const onSuccess = async (res) => {
    dispatch(
      googleSignIn({
        email: res?.email
      })
    );
  };

  return (
    <Box
      minH="100vh"
      display="flex"
      alignItems="center"
      justifyContent="center"
      bgGradient="linear(to-br, #E0C3FC, #8EC5FC)">
      <Box
        width={{ base: '90%', lg: '400px' }}
        borderRadius="15px"
        boxShadow="lg"
        bgColor="#FFFFFF"
        px={{ base: '5', lg: '8' }}
        py={{ base: '6', lg: '10' }}>
        <Stack spacing={4}>
          <Text
            align="center"
            fontSize={{ base: 'lg', lg: '2xl' }}
            fontWeight="bold"
            color="#333333">
            Welcome Back
          </Text>
          <Form
            layout="vertical"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            fields={[
              { name: ['userName'], value: userName },
              { name: ['password'], value: password }
            ]}>
            <Form.Item
              label={
                <Text color="#555555" fontSize="md" m="0" p="0" fontWeight="600">
                  Username or Email
                </Text>
              }
              name="userName"
              rules={[{ required: true, message: 'Please input your username!' }]}>
              <Input
                // addonBefore={<AiOutlineUser color="#555555" />}
                value={userName}
                placeholder="Enter username or email"
                onChange={(e) => setUserName(e.target.value)}
                size="large"
                style={{ borderColor: '#DDDDDD', padding: '6px 12px' }}
              />
            </Form.Item>
            <Form.Item
              label={
                <Text color="#555555" fontSize="md" m="0" fontWeight="600">
                  Password
                </Text>
              }
              name="password"
              rules={[{ required: true, message: 'Please input your password!' }]}>
              <Input.Password
                placeholder="Enter your password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                size="large"
                style={{ borderColor: '#DDDDDD', padding: '6px 12px' }}
              />
            </Form.Item>
            <Flex justifyContent="space-between">
              <Form.Item name="remember" noStyle>
                <Checkbox onClick={() => {}}>Remember me</Checkbox>
              </Form.Item>

              <a className="login-form-forgot" href="" onClick={() => navigate(FORGET_PASSWORD)}>
                Forgot password
              </a>
            </Flex>
            <Form.Item style={{ marginBottom: '0', paddingTop: '20px' }}>
              <Stack pt={2} spacing={3}>
                <Button
                  type="submit"
                  bgColor="#6A0572"
                  color="#FFFFFF"
                  _hover={{ bgColor: '#540465' }}
                  fontSize="lg"
                  isLoading={loading}
                  loadingText="Please wait...">
                  Sign In
                </Button>
                <Box position="relative" py="4">
                  <Divider color="#DDD00" border="1px" />
                  <AbsoluteCenter bg="white" px="4">
                    Or, Login with
                  </AbsoluteCenter>
                </Box>
                <LoginWithGoogle onSuccess={onSuccess} />

                <Flex align="center" justify="center" gap="2">
                  <Text color="#000000" fontWeight="600" fontSize="sm">
                    Don't have an account?
                  </Text>
                  <Text
                    color="blue.600"
                    fontWeight="600"
                    fontSize="sm"
                    textUnderlineOffset="true"
                    cursor="pointer"
                    onClick={() => navigate(SIGN_UP)}>
                    Register Now
                  </Text>
                </Flex>
              </Stack>
            </Form.Item>
          </Form>
        </Stack>
      </Box>
    </Box>
  );
}

import { Box, Text, Flex } from '@chakra-ui/react';
import { useEffect, useState, useCallback } from 'react';
import { message } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { DELETE_USER_RESET } from '../../constants/userConstants';
import { useNavigate } from 'react-router-dom';
import { ROOT, SIGN_IN } from '../../constants/routeConstant';
import { deleteBankAccount, getAllBankAccount } from '../../action/bankAccountAction';
import { AddButton } from '../../components/InputComponents/MyButton';
import { AddEditBankAccountModal } from '../../components/modal/AddEditBankAccountModal';
import { BankAccountTable, SearchInputField } from '../../components';

export function BankAccount() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [searchText, setSearchText] = useState('');

  const [open, setOpen] = useState(false);
  const [option, setOption] = useState('Add');

  const [currentBankAccount, setCurrentBankAccount] = useState({});

  const bankAccountList = useSelector((state) => state.bankAccountList);
  const { error, loading, data: bankList } = bankAccountList;

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const bankAccountDelete = useSelector((state) => state.bankAccountDelete);
  const { error: errorDeleteUser, loading: loadingDeleteUser, success } = bankAccountDelete;

  const confirm = (e, record) => {
    e.preventDefault();
    dispatch(deleteBankAccount(record?.bankAccountId));
  };

  const handleOpenEditCustomerModel = useCallback(
    (bankAccount) => {
      setCurrentBankAccount(bankAccount);
      setOption('Edit');
      setOpen(true);
    },
    [open, currentBankAccount]
  );

  const handleOpenAddCustomerModel = useCallback(() => {
    setOption('Add');
    setCurrentBankAccount({});
    setOpen(true);
  }, [open]);

  const handleCloseModel = useCallback(() => {
    setOpen(false);
  }, [open]);

  useEffect(() => {
    if (!userInfo) navigate(SIGN_IN);
    if (userInfo?.userRole !== 'Admin') {
      message.error('You have no access!');
      navigate(ROOT);
    }
    if (!(bankList?.length > 0)) {
      dispatch(getAllBankAccount());
    }
  }, [dispatch, userInfo, navigate]);

  useEffect(() => {
    if (error) {
      message.error(error);
    }
    if (errorDeleteUser) {
      message.error(errorDeleteUser);
      dispatch({ type: DELETE_USER_RESET });
    }
    if (success) {
      message.success('User record deleted successfully');
      dispatch({ type: DELETE_USER_RESET });

      dispatch(getAllBankAccount());
    }
  }, [error, errorDeleteUser, success, dispatch]);

  return (
    <Box bgColor="#F5F5F5" m="0">
      <AddEditBankAccountModal
        open={open}
        onCancel={handleCloseModel}
        option={option}
        bankAccount={currentBankAccount}
        setCurrentBankAccount={setCurrentBankAccount}
      />
      <Flex p="3" pl="5" bgColor="#FFFFFF" justifyContent={'space-between'}>
        <Text fontWeight="600" fontSize="2xl" m="0">
          Bank Account
        </Text>
        <div className="w-96">
          <SearchInputField
            value={searchText}
            onChange={setSearchText}
            placeholder="Search bank by holder name"
          />
        </div>
      </Flex>
      <Box p="2" pl="5">
        <Flex justifyContent="space-between">
          <AddButton onClick={handleOpenAddCustomerModel} label="Bank Account" />
        </Flex>
        <div className="py-2">
          <BankAccountTable
            data={bankList?.filter((bank) =>
              bank.accountHolderName.toLowerCase()?.includes(searchText.toLowerCase())
            )}
            loading={loading}
            confirm={confirm}
            handelEditBankAccount={handleOpenEditCustomerModel}
          />
        </div>
      </Box>
    </Box>
  );
}

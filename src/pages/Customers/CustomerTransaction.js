import { Box, Flex, Text } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import {
  getCustomerSellReturnTransactionList,
  getCustomerTransactionList
} from '../../action/transactionActions';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { convertNumberToIndian } from '../../utils/validateData';
import { CustomTab, TransactionTable } from '../../components';
import { deleteSellProductDetails, getSellProductDetails } from '../../action/sellProductAction';
import { SELL_PRODUCT_DETAILS, SELL_RETURN_PRODUCT_DETAILS } from '../../constants/routeConstant';
import { getSellReturnDetails } from '../../action/sellReturnAction';

export const CustomerTransaction = (props) => {
  const dispatch = useDispatch();
  const location = useLocation();
  const navigate = useNavigate();
  const { customer } = location.state;

  const [current, setCurrent] = useState('Purchase Transaction');

  const customerTransactionList = useSelector((state) => state.customerTransactionList);
  const { error, loading, data: transactions } = customerTransactionList;

  const customerSellReturnTransaction = useSelector((state) => state.customerSellReturnTransaction);
  const {
    error: errorCustomerReturnTransaction,
    loading: loadingCustomerReturnTransaction,
    data: CustomerReturnTransaction
  } = customerSellReturnTransaction;

  const handelDeleteSellTransaction = (transactionId) => {
    if (window.confirm('Do u really want to delete this transaction?'))
      dispatch(deleteSellProductDetails(transactionId));
  };

  useEffect(() => {
    dispatch(getCustomerTransactionList(customer?._id));
    dispatch(getCustomerSellReturnTransactionList(customer?._id));
  }, []);

  const getTotalPurchaseAmount = () => {
    let ans = transactions?.reduce((accum, current) => {
      return accum + current.paidAmount;
    }, 0);
    return ans;
  };

  return (
    <Box bgColor="#F5F5F5" m="0" gap="4">
      <Flex p="3" pl="5" bgColor="#FFFFFF" flexDir="column">
        <Text fontWeight="600" fontSize="2xl" m="0">
          Customer Transactions
        </Text>
      </Flex>
      <Flex pl="5" mt="4" bgColor="" flexDir="row" gap="8">
        <Box gap="4">
          <Text fontWeight="400" fontSize="lg" m="0">
            Customer name
          </Text>
          <Text fontWeight="400" fontSize="lg" m="0">
            Total purchase Amount
          </Text>
        </Box>
        <Box gap="4">
          <Text fontWeight="600" fontSize="lg" m="0">
            {customer?.customerName}
          </Text>
          <Text fontWeight="400" fontSize="lg" m="0" color="green.600">
            {convertNumberToIndian(getTotalPurchaseAmount())}
          </Text>
        </Box>
      </Flex>
      <Box p="2" pl="5">
        <CustomTab
          setCurrent={(value) => setCurrent(value)}
          data={['Purchase Transaction', 'Sell Return']}
          current={current}
        />

        {current === 'Purchase Transaction' && (
          <TransactionTable
            loading={loading}
            data={transactions}
            deleteTransaction={handelDeleteSellTransaction}
            getDetail={(billNo) => {
              dispatch(getSellProductDetails(billNo));
              navigate(SELL_PRODUCT_DETAILS);
            }}
          />
        )}

        {current === 'Sell Return' && (
          <TransactionTable
            loading={loadingCustomerReturnTransaction}
            data={CustomerReturnTransaction}
            deleteTransaction={() => {}}
            getDetail={(billNo) => {
              dispatch(getSellReturnDetails(billNo));
              navigate(SELL_RETURN_PRODUCT_DETAILS);
            }}
          />
        )}
      </Box>
    </Box>
  );
};

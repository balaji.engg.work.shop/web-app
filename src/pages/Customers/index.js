import { Box, Button, Flex, Text, Stack } from '@chakra-ui/react';
import { useEffect, useState, useCallback } from 'react';
import { message } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { deleteCustomer, getAllCustomers } from '../../action/customerAction';
import { AddButton } from '../../components/InputComponents/MyButton';
import { DELETE_CUSTOMER_RESET } from '../../constants/customerConstants';
import { AddEditCustomerModal, CustomerTable, SearchInputField } from '../../components';

export function Customers() {
  const [searchText, setSearchText] = useState('');
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [currentCustomer, setCurrentCustomer] = useState({});
  const [option, setOption] = useState('Add');

  const handelSetCustomer = useCallback(() => {
    setCurrentCustomer({});
  }, [currentCustomer]);

  const customerList = useSelector((state) => state.customerList);
  const { error, loading, customers } = customerList;

  const customerDelete = useSelector((state) => state.customerDelete);
  const { error: customerDeleteError, loading: customerDeleteLoading, success } = customerDelete;

  const handleOpenEditCustomerModel = useCallback(
    (customer) => {
      setCurrentCustomer(customer);
      setOption('Edit');
      setOpen(true);
    },
    [open]
  );

  const handleOpenAddCustomerModel = useCallback(() => {
    setOption('Add');
    setOpen(true);
  }, [open]);

  const handleCloseModel = useCallback(() => {
    setOpen(false);
  }, [open]);

  useEffect(() => {
    if (customerDeleteError) {
      message.error(customerDeleteError);
      dispatch({ type: DELETE_CUSTOMER_RESET });
    } else if (success) {
      message.success('Customer deleted successfully');
      dispatch({ type: DELETE_CUSTOMER_RESET });
      dispatch(getAllCustomers());
    }
  }, [customerDeleteError, success]);

  useEffect(() => {
    if (!(customers?.length > 0)) {
      dispatch(getAllCustomers());
    }
  }, []);

  const confirmDeleteCustomer = (e, customer) => {
    e.preventDefault();
    dispatch(deleteCustomer(customer.customerId));
  };

  return (
    <Box bgColor="#F5F5F5" m="0" height="100%">
      <AddEditCustomerModal
        open={open}
        onCancel={handleCloseModel}
        option={option}
        customer={currentCustomer}
        setCurrentCustomer={handelSetCustomer}
      />
      <Flex p="3" pl="5" bgColor="#FFFFFF" justifyContent={'space-between'}>
        <Text fontWeight="600" fontSize="2xl" m="0">
          Customer
        </Text>
        <div className="w-96">
          <SearchInputField
            value={searchText}
            onChange={setSearchText}
            placeholder="Search customer by name"
          />
        </div>
      </Flex>
      <Box p="2" pl="5">
        <Flex justifyContent="space-between">
          <AddButton onClick={handleOpenAddCustomerModel} label="Customer" />

          <Button color="#FFFFFF" bgColor="green">
            Total customer : {customers?.length}
          </Button>
        </Flex>
        <div className="py-2">
          <CustomerTable
            confirm={confirmDeleteCustomer}
            handelEditCustomer={handleOpenEditCustomerModel}
            data={customers?.filter((customer) =>
              customer.customerName.toLowerCase()?.includes(searchText.toLowerCase())
            )}
            loading={loading}
          />
        </div>
      </Box>
    </Box>
  );
}

import { Select } from 'antd';
import { Box, Button, Flex, Text } from '@chakra-ui/react';
import { Input, message } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IoMdAdd } from 'react-icons/io';
import { RiDeleteBin6Fill } from 'react-icons/ri';
import {
  addDailyManufactureProductInBulk,
  getDailyManufactureProductList
} from '../../action/productActions';
import { ADD_DAILY_MANUFACTURE_RESET } from '../../constants/productConstants';
import { useNavigate } from 'react-router-dom';
import { SaveButton } from '../../components/InputComponents/MyButton';

const data = [
  {
    title: 'S.N.',
    width: '50px',
    required: false
  },
  {
    title: 'Descriptions',
    width: '400px',
    required: true
  },
  {
    title: 'Quantity',
    width: '150px',
    required: true
  },
  {
    title: 'Action',
    width: '100px',
    required: false
  }
];

export const AddDailyProduct = (props) => {
  const { open, onCancel } = props;
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [date, setDate] = useState(null);
  const [products, setMyProducts] = useState([
    {
      productId: '',
      quantity: ''
    }
  ]);

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const handleRemoveRow = (index) => {
    const updatedRows = [...products];
    updatedRows.splice(index, 1);
    setMyProducts(updatedRows);
  };
  const validate = () => {
    const index = products.length - 1;
    for (let key in products[index]) {
      if (products[index][key] == '' || products[index][key] === undefined) {
        message.error('Please fill all data first');
        return false;
      }
    }
    return true;
  };
  const handleAddRow = () => {
    if (validate())
      setMyProducts([
        ...products,
        {
          productId: '',
          quantity: ''
        }
      ]);
  };

  function handleInputChange(event, index, field) {
    const updatedRows = [...products];
    updatedRows[index][field] = event.target.value;
    setMyProducts(updatedRows);
  }
  function handleSelectChange(value, index, field) {
    let res = products.some((product) => product.productId == value);
    if (res) {
      message.error('This product has all ready selected');
      return;
    } else {
      const updatedRows = [...products];
      updatedRows[index][field] = value;
      setMyProducts(updatedRows);
    }
  }

  useEffect(() => {
    const today = new Date();
    const numberOfDaysToAdd = 3;
    const date = today.setDate(today.getDate() + numberOfDaysToAdd);
    const defaultValue = new Date(date).toISOString().split('T')[0];
    setDate(defaultValue);
  }, []);

  const handelSave = () => {
    if (date && validate()) {
      dispatch(
        addDailyManufactureProductInBulk({
          date: date,
          products
        })
      );
    } else message.error('Please select date');
  };

  const productList = useSelector((state) => state.productList);
  const { error, loading, products: myProducts } = productList;

  const addDailyManufactureProducts = useSelector((state) => state.addDailyManufactureProducts);
  const {
    error: errorAddDaily,
    loading: loadingAddDaily,
    data: addDaily
  } = addDailyManufactureProducts;

  useEffect(() => {
    if (errorAddDaily) {
      message.error(errorAddDaily);
      dispatch({ type: ADD_DAILY_MANUFACTURE_RESET });
    }
    if (addDaily) {
      message.success(addDaily);
      dispatch({ type: ADD_DAILY_MANUFACTURE_RESET });
      dispatch(getDailyManufactureProductList());
      setMyProducts([]);
      setTimeout(() => {
        props.onCancel();
      }, 0);
    }
  }, [errorAddDaily, addDaily]);

  // addDailyManufactureProducts
  return (
    <Box gap="4px">
      <Flex flexDir="row" gap="4" align="center" py="4">
        <Text fontWeight="500" fontSize="16px">
          Date
        </Text>
        <Input
          type="date"
          onChange={(e) => setDate(e.target.value)}
          value={date}
          style={{ width: '200px' }}
        />
      </Flex>

      <table>
        <tr>
          {data?.map((d, index) => (
            <th key={index}>
              <Text
                width={d.width}
                height="30px"
                fontSize="sm"
                color="#FFFFFF"
                fontWeight="500"
                border="1px"
                borderColor="#000000"
                align="center"
                justifyContent="center"
                px="2px"
                py="2px"
                bgColor="gray.600">
                {d.title}
              </Text>
            </th>
          ))}
        </tr>

        {products.map((product, index) => (
          <tr key={index}>
            <td>
              <Input value={index + 1} style={{ width: data[0].width, borderRadius: 0 }} />
            </td>
            <td>
              <Select
                size={'large'}
                showSearch
                value={product.productId}
                options={myProducts?.map((product) => {
                  return {
                    value: product.productId,
                    label: `${product.productCode} - ${product.description}`
                  };
                })}
                filterOption={(input, option) =>
                  (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                }
                style={{ width: data[1].width, borderRadius: '0px' }}
                onChange={(value) => handleSelectChange(value, index, 'productId')}
                placeholder="Select price type "
              />
            </td>
            <td>
              <Input
                type="number"
                style={{ width: data[2].width, borderRadius: 0 }}
                value={product.quantity}
                onChange={(value) => handleInputChange(value, index, 'quantity')}
              />
            </td>
            <td>
              <Flex gap="2" align="center" p="1">
                <Button
                  style={{ height: '32px' }}
                  size="md"
                  bgColor="green"
                  title="add more"
                  onClick={handleAddRow}>
                  <IoMdAdd color="#FFFFFF" />
                </Button>
                {products?.length > 1 && index != 0 ? (
                  <Button
                    style={{ height: '32px' }}
                    bgColor="red"
                    title="remove"
                    onClick={() => handleRemoveRow(index)}>
                    <RiDeleteBin6Fill color="#FFFFFF" size="16px" />
                  </Button>
                ) : (
                  <Button
                    style={{ height: '32px' }}
                    bgColor="red"
                    title="remove"
                    isDisabled={true}
                    onClick={() => handleRemoveRow(index)}>
                    <RiDeleteBin6Fill color="#FFFFFF" size="16px" />
                  </Button>
                )}
              </Flex>
            </td>
          </tr>
        ))}
      </table>
      <Flex py="4">
        <SaveButton
          onClick={handelSave}
          label="Save"
          loading={loadingAddDaily}
          loadingText="Saving..."
        />
      </Flex>
    </Box>
  );
};

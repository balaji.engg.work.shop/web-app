import { Box, Text, Flex } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { AddDailyProduct } from './AddDailyProduct';
import { AddButton } from '../../components/InputComponents/MyButton';
import { useDispatch, useSelector } from 'react-redux';
import { getDailyManufactureProductList } from '../../action/productActions';
import { DailyProductTable } from './DailyProductTable';
import { MyMenu } from '../../components';

export function DailyManufacturingEntry() {
  const dispatch = useDispatch();
  const [openAddDailyProduct, setOpenAddDailyProduct] = useState(false);

  const getDailyManufactureProducts = useSelector((state) => state.getDailyManufactureProducts);
  const {
    error: errorAddDaily,
    loading: loadingAddDaily,
    data: dailyProducts
  } = getDailyManufactureProducts;

  useEffect(() => {
    if (!(dailyProducts?.length > 0)) dispatch(getDailyManufactureProductList());
  }, []);

  return (
    <Box bgColor="#F5F5F5" m="0">
      <Flex p="3" pl="5" bgColor="#FFFFFF" justifyContent={'space-between'}>
        <Text fontWeight="600" fontSize="2xl" m="0">
          Daily Manufacturing
        </Text>
        <MyMenu />
      </Flex>
      <Box p="2" pl="5">
        {openAddDailyProduct && (
          <AddDailyProduct
            open={openAddDailyProduct}
            onCancel={() => setOpenAddDailyProduct(false)}
          />
        )}
        {!openAddDailyProduct && (
          <>
            <AddButton
              label="Add Daily Manufacturing details"
              onClick={() => setOpenAddDailyProduct(true)}
            />
            <DailyProductTable products={dailyProducts} loading={loadingAddDaily} />
          </>
        )}
      </Box>
    </Box>
  );
}

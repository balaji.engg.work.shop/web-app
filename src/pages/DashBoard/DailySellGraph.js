// src/BarChart.js
import React from 'react';
import { Bar } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
} from 'chart.js';
import { Text } from '@chakra-ui/react';

// Register the required components for Chart.js
ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend);

export const DailySellGraph = ({ data }) => {
  const chartData = {
    labels: data?.map((singleData) => singleData?.date),
    datasets: [
      {
        label: 'Daily Sales Amount',
        data: data?.map((singleData) => singleData?.totalSell),
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 1
      }
    ]
  };

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top'
      },
      tooltip: {
        callbacks: {
          label: function (tooltipItem) {
            return `Amount: ${tooltipItem.raw}`;
          }
        }
      }
    },
    scales: {
      x: {
        beginAtZero: true
      },
      y: {
        beginAtZero: true
      }
    }
  };

  return (
    <div>
      <Text fontWeight="600" fontSize="20px" m="0">
        Daily Sales Bar Chart
      </Text>
      <Bar data={chartData} options={options} />
    </div>
  );
};

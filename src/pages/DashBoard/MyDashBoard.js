import { Box, Flex, SimpleGrid, Text } from '@chakra-ui/react';
import { AiOutlineProduct } from 'react-icons/ai';
import { CiBookmarkRemove } from 'react-icons/ci';
import { HiUserGroup } from 'react-icons/hi2';
import { MonthlySellBarGraph } from './MonthlySellBarGraph';
import { DailySellGraph } from './DailySellGraph';
import { MyStock } from './MyStock';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { getAllProduct } from '../../action/productActions';
import { CUSTOMERS, PRODUCTS, SIGN_IN, SELL_TRANSACTION } from '../../constants/routeConstant';
import { Skeleton } from 'antd';
import { getCountData } from '../../action/sellProductAction';

export const MyDashBoard = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const productList = useSelector((state) => state.productList);
  const { error, loading, products } = productList;

  const getCount = useSelector((state) => state.getCount);
  const { error: errorCountData, loading: loadingCountData, data: countData } = getCount;

  const data = [
    {
      total: countData?.totalBillCount,
      thisMonth: countData?.thisMonthBillCount,
      label: 'Total sales',
      icon: <CiBookmarkRemove size="24px" />,
      bgColor: '#9ADE7B',
      path: SELL_TRANSACTION
    },
    {
      total: countData?.totalProductCount,
      label: 'Total Products',
      icon: <AiOutlineProduct size="24px" />,
      bgColor: '#78C1F3',
      path: PRODUCTS
    },
    {
      total: countData?.totalCustomerCount,
      label: 'Total Customer',
      icon: <HiUserGroup size="24px" />,
      bgColor: '#BC7AF9',
      path: CUSTOMERS
    }
  ];

  useEffect(() => {
    if (!userInfo) {
      navigate(SIGN_IN);
    }
    dispatch(getCountData());
    dispatch(getAllProduct());
  }, []);

  return (
    <Box>
      <SimpleGrid columns={[2, 2, 4]} gap={{ base: '2', lg: '8' }}>
        {data.map((singleData) => (
          <Box
            onClick={() => navigate(singleData.path)}
            borderRadius="md"
            px="4"
            py="2"
            bgColor={singleData.bgColor}
            color="#ffffff"
            fontWeight="600">
            <Text fontSize="md">{singleData.label}</Text>
            <Flex justify="space-between" size="lg" pt="4">
              {singleData.icon}
              <Text fontSize="24px">{singleData.total}</Text>
            </Flex>
            {singleData.thisMonth && (
              <Flex justify="space-between" pt="2">
                <Text fontSize="md">This month</Text>
                <Text>{singleData.thisMonth}</Text>
              </Flex>
            )}
          </Box>
        ))}
      </SimpleGrid>
      <SimpleGrid columns={[1, 2, 2]} gap="4" mt="4">
        <MonthlySellBarGraph data={countData?.yearSellData} />
        <DailySellGraph data={countData?.monthSellData} />
        {loading ? <Skeleton active /> : <MyStock data={products} />}
      </SimpleGrid>
    </Box>
  );
};

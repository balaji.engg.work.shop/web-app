import React from 'react';
import { Bar } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
} from 'chart.js';
import { Text } from '@chakra-ui/react';
import { getStock } from '../../utils/processData';

// Register the required components for Chart.js
ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend);

export const MyStock = ({ data }) => {
  const { labels, quantity } = getStock(data);

  const chartData = {
    labels: labels,
    datasets: [
      {
        label: 'Stocks',
        data: quantity,
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 1
      }
    ]
  };

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top'
      },
      tooltip: {
        callbacks: {
          label: function (tooltipItem) {
            return `Quantity: ${tooltipItem.raw}`;
          }
        }
      }
    },
    scales: {
      x: {
        beginAtZero: true
      },
      y: {
        beginAtZero: true
      }
    }
  };

  return (
    <div>
      <Text fontWeight="600" fontSize="20px" m="0">
        My Stocks
      </Text>
      <Bar data={chartData} options={options} />
    </div>
  );
};

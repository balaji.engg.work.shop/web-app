import { Box, Text, Flex } from '@chakra-ui/react';
import React from 'react';
import { MyDashBoard } from './MyDashBoard';

export const DashBoard = (props) => {
  return (
    <Box bgColor="#F5F5F5" m="0" height="100%" overflowY="auto">
      <Flex p="3" pl="5" bgColor="#FFFFFF">
        <Text fontWeight="600" fontSize="2xl" m="0">
          DashBoard
        </Text>
      </Flex>
      <Box p="2" pl="5">
        <MyDashBoard />
      </Box>
    </Box>
  );
};

import { Box, Flex, Text, Button, SimpleGrid } from '@chakra-ui/react';
import { Select, Input, message } from 'antd';
import { useEffect, useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllCustomers } from '../../action/customerAction';
import { FcOk } from 'react-icons/fc';
import { GET_CUSTOMER_RESET } from '../../constants/customerConstants';
import { getAllProduct } from '../../action/productActions';
import { useNavigate } from 'react-router-dom';
import { SIGN_IN } from '../../constants/routeConstant';
import { IoMdAdd } from 'react-icons/io';
import { RiCloseCircleLine } from 'react-icons/ri';
import { FaRupeeSign } from 'react-icons/fa';
import { FiPrinter } from 'react-icons/fi';
import { GrPowerReset } from 'react-icons/gr';
import { getShopDetails } from '../../action/shopActions';
import { convertNumberToIndianRs } from '../../utils/validateData';
import { productUnits } from '../../utils/dataUtiles';
import { AddEditCustomerModal, EstimateFormModel, MyMenu } from '../../components';

export const EstimateForm = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [isGST, setIsGST] = useState(false);
  const [isSameState, setIsSameState] = useState(false);
  const [open, setOpen] = useState(false);
  const [openEstimateModel, setOpenEstimateModel] = useState(false);

  const [customer, setCustomer] = useState({});
  const [CGSTPercent, setCGSTPercent] = useState(9);
  const [SGSTPercent, setSGSTPercent] = useState(9);
  const [IGSTPercent, setIGSTPercent] = useState(18);
  const [billDate, setBillDate] = useState(new Date().toISOString().split('T')[0]);

  const [myProducts, setMyProducts] = useState([
    {
      sn: 1,
      productId: '',
      description: '',
      priceType: '',
      availableQuantity: '',
      quantity: '',
      price: '',
      total: ''
    }
  ]);

  const getTotal = () => {
    let sum = 0;
    for (let product of myProducts) {
      sum += Number(product.total);
    }
    return sum;
  };

  const getGSTPresent = () => {
    let gstPresent = 0;
    if (isGST) {
      if (isSameState) {
        gstPresent = Number(SGSTPercent) + Number(CGSTPercent);
      } else {
        gstPresent = Number(IGSTPercent);
      }
    }
    return gstPresent;
  };

  const customerList = useSelector((state) => state.customerList);
  const { error: errorCustomer, loading: loadingCustomer, customers } = customerList;

  const shopDetail = useSelector((state) => state.shopDetail);
  const { error: errorShopDetail, loading: loadingShopDetail, data: shop } = shopDetail;

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const productList = useSelector((state) => state.productList);
  const { error, loading, products } = productList;

  const handleOpenAddSupplierModal = useCallback(() => {
    setOpen(true);
  }, [open]);

  const handleCloseModel = useCallback(() => {
    setOpen(false);
  }, [open]);

  const handleCloseEstimateModal = useCallback(() => {
    setOpenEstimateModel(false);
  }, [openEstimateModel]);

  const validateRow = () => {
    const index = myProducts.length - 1;
    for (let key in myProducts[index]) {
      if (myProducts[index][key] == '' || myProducts[index][key] === undefined) {
        message.error('Please fill all row data first');
        return false;
      }
    }
    return true;
  };

  const validate = () => {
    if (!customer?.customerId) {
      message.error('first select or add new customer');
      return false;
    } else if (!billDate) {
      message.error('Add bill date');
      return false;
    } else return true;
  };

  const handleAddRow = () => {
    if (validateRow()) {
      setMyProducts([
        ...myProducts,
        { description: '', priceType: '', quantity: '', price: '', total: '' }
      ]);
    } else {
    }
  };

  const handleRemoveRow = (index) => {
    const updatedRows = [...myProducts];
    updatedRows.splice(index, 1);
    setMyProducts(updatedRows);
  };

  useEffect(() => {
    if (!customer) {
      dispatch({ type: GET_CUSTOMER_RESET });
    }
  }, [customer]);

  useEffect(() => {
    if (!userInfo) {
      navigate(SIGN_IN);
    }
    dispatch(getAllProduct());
    dispatch(getShopDetails());
    dispatch(getAllCustomers());
  }, []);

  const getCustomer = (customerId) => {
    const customer = customers.find((singleCustomer) => singleCustomer.customerId == customerId);
    setCustomer(customer);
  };

  function handleInputChange(event, index, field) {
    const updatedRows = [...myProducts];
    updatedRows[index][field] = event.target.value;
    setMyProducts(updatedRows);
  }
  function handleSelectValue(value, index, field) {
    const updatedRows = [...myProducts];
    updatedRows[index][field] = value;
    setMyProducts(updatedRows);
  }
  function handleSelectDescription(productId, index, field) {
    const updatedRows = [...myProducts];
    const product = products.find((data) => data.productId == productId);
    updatedRows[index][field] = product.description;
    updatedRows[index]['price'] = product.sellingPrice;
    updatedRows[index]['priceType'] = product.priceType;
    updatedRows[index]['productId'] = productId;
    updatedRows[index]['availableQuantity'] = product.quantity;

    setMyProducts(updatedRows);
  }

  function setTotal(index) {
    const updatedRows = [...myProducts];
    updatedRows[index]['total'] = updatedRows[index]['quantity'] * updatedRows[index]['price'];
    setMyProducts(updatedRows);
  }

  const goToInvoice = () => {
    if (validateRow() && validate()) setOpenEstimateModel(true);
  };

  const checkValidNumber = (value) => {
    if (Number(value) === NaN) {
      return false;
    } else if (Number(value) < 0) {
      return false;
    } else return true;
  };

  const handleReset = () => {
    window.location.reload();
  };

  return (
    <div className="prime" style={{ background: '#FFFFFF' }}>
      <AddEditCustomerModal
        open={open}
        onCancel={handleCloseModel}
        option={'Add'}
        setCurrentCUstomer={() => {}}
      />
      <EstimateFormModel
        open={openEstimateModel}
        onCancel={handleCloseEstimateModal}
        customer={customer}
        products={myProducts}
        shop={shop}
        sellDetails={{
          billDate,
          isGST,
          isSameState,
          total: Number(getTotal()), // without gst
          SGSTPercent,
          CGSTPercent,
          IGSTPercent,
          gstAmount: Number(getTotal() * getGSTPresent() * 0.01),
          netTotal: getTotal() + getTotal() * getGSTPresent() * 0.01
        }}
      />
      <Box bgColor="#F5F5F5" m="0" height="100vh">
        <Flex
          p="3"
          align="center"
          justifyContent="space-between"
          borderTop="1px"
          borderLeft="1px"
          // border="1px solid green"
          borderColor="#EBEBEB"
          pr="20"
          bgColor="#FFFFFF">
          <Text px="2" fontWeight="600" fontSize="2xl" m="0">
            Generate Estimate
          </Text>
          <MyMenu />
        </Flex>
        <Box p="3" px="5" className="h-[80vh] overflow-y-auto">
          <SimpleGrid
            columns={[1, 2, 2]}
            gap={{ base: '1', lg: '4' }}
            mr={{ base: '0px', lg: '50px' }}>
            <Flex gap="4">
              <Flex flexDir="column" gap="1" width="full">
                <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                  Bill Type
                </Text>
                <Select
                  style={{ width: 'full' }}
                  placeholder="Select sell type"
                  optionFilterProp="children"
                  onChange={setIsGST}
                  value={isGST}
                  size="large"
                  filterOption={(input, option) => (option?.label ?? '').includes(input)}
                  options={[
                    {
                      label: 'Without GST',
                      value: false
                    },
                    {
                      label: 'GST',
                      value: true
                    }
                  ]}
                />
              </Flex>
              {isGST && (
                <Flex flexDir="column" gap="1" width="full">
                  <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                    Select State
                  </Text>
                  <Select
                    style={{ width: 'full' }}
                    placeholder="Select option"
                    optionFilterProp="children"
                    onChange={setIsSameState}
                    value={isSameState}
                    size="large"
                    filterOption={(input, option) => (option?.label ?? '').includes(input)}
                    options={[
                      {
                        label: 'Same state',
                        value: true
                      },
                      {
                        label: 'Other state',
                        value: false
                      }
                    ]}
                  />
                </Flex>
              )}
            </Flex>
            <Flex flexDir="column" gap="1" width="100%">
              <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                Select customer
              </Text>
              <Select
                size={'large'}
                showSearch
                value={customer.customerId}
                options={customers?.map((customer) => {
                  return {
                    value: customer.customerId,
                    label: `${customer.mobileNo} (${customer.customerName})`
                  };
                })}
                filterOption={(input, option) =>
                  (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                }
                style={{ width: 'full', borderRadius: 0 }}
                onChange={(value) => {
                  getCustomer(value);
                }}
                placeholder="Select customer by name or mobile no"
              />
            </Flex>
            <Flex flexDir="column" gap="1" justifyItems="center" width="full">
              <Flex gap="2" align="center">
                <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                  Customer Name
                </Text>
                {customer?.customerName && <FcOk />}
              </Flex>
              <Input size="large" placeholder="Mobile Number" value={customer?.customerName} />
            </Flex>
            <Flex flexDir="column" gap="1" justifyItems="center" width="full">
              <Flex gap="2" align="center">
                <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                  Customer GST
                </Text>
                {customer?.customerName && <FcOk />}
              </Flex>
              <Input size="large" placeholder="GST" value={customer?.gst} />
            </Flex>
            <Flex flexDir="column" gap="1" justifyItems="center" width="full">
              <Flex gap="2" align="center">
                <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                  Generate Date
                </Text>
              </Flex>
              <Input
                size="large"
                type="date"
                placeholder="GST"
                value={billDate}
                onChange={(e) => {
                  setBillDate(e.target.value);
                }}
              />
            </Flex>
          </SimpleGrid>
          <Flex flexDir={'column'} mt="4">
            <Flex>
              <Text
                width="50px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                S.N.
              </Text>{' '}
              <Text
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px"
                width="350px">
                Product Name
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Unit
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Current Stock{' '}
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Quantity
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Price
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Total
              </Text>{' '}
            </Flex>
            {myProducts.map((product, index) => (
              <Flex>
                <Input value={index + 1} style={{ width: '50px', borderRadius: 0 }} />
                <Select
                  size={'large'}
                  showSearch
                  value={product.description}
                  options={products?.map((product) => {
                    return {
                      value: product.productId,
                      label: `${product.productCode} - ${product.description}`
                    };
                  })}
                  filterOption={(input, option) =>
                    (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                  }
                  style={{ width: '350px', borderRadius: 0 }}
                  onChange={(value) => handleSelectDescription(value, index, 'description')}
                  placeholder="Search product name with code"
                />
                <Select
                  showSearch
                  size={'large'}
                  value={product.priceType}
                  options={productUnits}
                  filterOption={(input, option) =>
                    (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                  }
                  style={{ width: '150px', borderRadius: 0, fontSize: '16px' }}
                  onChange={(value) => handleSelectValue(value, index, 'priceType')}
                  placeholder="Select product units "
                />
                <Input
                  type="number"
                  value={product.availableQuantity}
                  style={{ width: '150px', borderRadius: 0 }}
                  disabled
                />
                <Input
                  type="number"
                  value={product.quantity}
                  style={{ width: '150px', borderRadius: 0 }}
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value)) {
                      handleInputChange(e, index, 'quantity');
                      setTotal(index);
                    }
                  }}
                />{' '}
                <Input
                  type="number"
                  value={product.price}
                  style={{ width: '150px', borderRadius: 0 }}
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value)) {
                      handleInputChange(e, index, 'price');
                      setTotal(index);
                    }
                  }}
                />{' '}
                <Input
                  // type="number"
                  value={convertNumberToIndianRs(product.total)}
                  style={{ width: '150px', borderRadius: 0 }}
                />
                <Flex gap="4" align="center" pl="4" p="1">
                  <Button
                    style={{ height: '32px' }}
                    size="md"
                    bgColor="green"
                    title="add more"
                    onClick={handleAddRow}>
                    <IoMdAdd color="#FFFFFF" />
                  </Button>
                  {myProducts?.length > 1 && index != 0 ? (
                    <Button
                      style={{ height: '32px' }}
                      bgColor="red"
                      title="remove"
                      onClick={() => handleRemoveRow(index)}>
                      <RiCloseCircleLine color="#FFFFFF" size="16px" />
                    </Button>
                  ) : null}
                </Flex>
              </Flex>
            ))}
          </Flex>
          <SimpleGrid columns={[1, 2, 3]} gap="4" pt="4" mr="50px">
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Total
              </Text>
              <Input
                addonBefore={<FaRupeeSign />}
                size="medium"
                placeholder="total"
                value={convertNumberToIndianRs(getTotal() || 0)}
              />
            </Flex>
            {isGST ? (
              isSameState ? (
                <>
                  <Flex flexDir="column" gap="1" justifyItems="center">
                    <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                      SGST %
                    </Text>
                    <Input
                      size="medium"
                      placeholder="gst %"
                      type="number"
                      value={CGSTPercent}
                      onChange={(e) => {
                        setSGSTPercent(e.target.value);
                        setCGSTPercent(e.target.value);
                      }}
                    />
                  </Flex>
                  <Flex flexDir="column" gap="1" justifyItems="center">
                    <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                      CGST %
                    </Text>
                    <Input
                      size="medium"
                      placeholder="gst %"
                      type="number"
                      value={CGSTPercent}
                      onChange={(e) => {
                        setSGSTPercent(e.target.value);
                        setCGSTPercent(e.target.value);
                      }}
                    />
                  </Flex>
                </>
              ) : (
                <Flex flexDir="column" gap="1" justifyItems="center">
                  <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                    IGST %
                  </Text>
                  <Input
                    size="medium"
                    placeholder="gst %"
                    type="number"
                    value={IGSTPercent}
                    onChange={(e) => setIGSTPercent(e.target.value)}
                  />
                </Flex>
              )
            ) : null}
            {isGST && (
              <Flex flexDir="column" gap="1" justifyItems="center">
                <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                  GST Amount
                </Text>
                <Input
                  addonBefore={<FaRupeeSign />}
                  size="medium"
                  placeholder="gst %"
                  type="number"
                  value={getTotal() * getGSTPresent() * 0.01}
                />
              </Flex>
            )}
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Net Total
              </Text>
              <Input
                size="medium"
                placeholder="net total"
                value={convertNumberToIndianRs(getTotal() + getTotal() * getGSTPresent() * 0.01)}
                // onChange={(e) => setNetTotal(e.target.value)}
                addonBefore={<FaRupeeSign />}
              />
            </Flex>
          </SimpleGrid>
          <Flex justifyContent="center" gap="8" pt="8">
            <Button
              type={'submit'}
              bgColor="rgba(40,167,69,1)"
              fontSize="lg"
              px={{ base: '5', lg: '10' }}
              color="#FFFFFF"
              leftIcon={<FiPrinter />}
              _hover={{
                color: 'rgba(40,167,69,1)',
                bgColor: '#FFFFFF',
                borderColor: 'rgba(40,167,69,1)',
                border: '2px'
              }}
              onClick={goToInvoice}>
              Print
            </Button>

            <Button
              type={'submit'}
              bgColor="rgba(40,167,69,1)"
              fontSize="lg"
              px={{ base: '5', lg: '10' }}
              color="#FFFFFF"
              leftIcon={<GrPowerReset />}
              _hover={{
                color: 'rgba(40,167,69,1)',
                bgColor: '#FFFFFF',
                borderColor: 'rgba(40,167,69,1)',
                border: '2px'
              }}
              onClick={handleReset}>
              Reset
            </Button>
          </Flex>
        </Box>
      </Box>
    </div>
  );
};

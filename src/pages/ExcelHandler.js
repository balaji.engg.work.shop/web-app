// export default ExcelHandler;

// expectedHeaders-up

import React, { useState } from 'react';
import ExcelJS from 'exceljs';
import { saveAs } from 'file-saver';

const ExcelHandler = () => {
  const [errors, setErrors] = useState([]);
  const [data, setData] = useState([]);
  const [workbookWithErrors, setWorkbookWithErrors] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const expectedHeaders = ['Name', 'Email', 'Age'];
  const maxSize = 5 * 1024 * 1024; // 5MB

  // Function to handle file import and validation
  const handleImport = async (event) => {
    const file = event.target.files[0];
    if (!file) return;

    // File format validation
    if (!file.name.endsWith('.xlsx')) {
      setErrors(['Please upload a valid Excel file with .xlsx extension.']);
      setIsModalOpen(true);
      return;
    }

    // File size validation
    if (file.size < 1024 || file.size > maxSize) {
      setErrors([
        'File size should be between 1KB and 5MB. Current file size is too ' +
          (file.size < 1024 ? 'small.' : 'large.')
      ]);
      setIsModalOpen(true);
      return;
    }

    const workbook = new ExcelJS.Workbook();
    try {
      await workbook.xlsx.load(await file.arrayBuffer());
    } catch (error) {
      setErrors(['Failed to read the Excel file. Please ensure it is a valid .xlsx file.']);
      setIsModalOpen(true);
      return;
    }

    const worksheet = workbook.worksheets[0];
    if (!worksheet) {
      setErrors(['The file is empty. Please upload a file with data.']);
      setIsModalOpen(true);
      return;
    }

    const validationErrors = [];
    const dataRows = [];

    // Validate headers
    const headerRow = worksheet.getRow(1);
    const headers = headerRow.values.slice(1);
    if (!validateHeaders(headers)) {
      validationErrors.push('Headers do not match the expected format.');
      highlightHeaderErrors(headerRow, headers);
    }

    // Validate data rows
    const emailSet = new Set();
    worksheet.eachRow({ includeEmpty: false }, (row, rowNumber) => {
      if (rowNumber === 1) return; // Skip header row

      const rowErrors = validateRow(row, rowNumber, emailSet);
      if (rowErrors.length > 0) {
        validationErrors.push(...rowErrors);
      } else {
        dataRows.push({
          name: getCellValue(row.getCell(1)),
          email: getCellValue(row.getCell(2)),
          age: getCellValue(row.getCell(3))
        });
      }
    });

    if (validationErrors.length > 0) {
      setErrors(validationErrors);
      setData([]);
      setWorkbookWithErrors(workbook);
      setIsModalOpen(true);
    } else {
      setErrors([]);
      setData(dataRows);
      setWorkbookWithErrors(null);
    }
  };

  // Function to get cell value as string
  // Function to get cell value as string
  const getCellValue = (cell) => {
    if (!cell || cell.value === undefined || cell.value === null) {
      return ''; // Return an empty string if the cell is null or undefined
    }

    let value = cell.value;
    if (typeof value === 'object') {
      if (value.text) {
        return value.text; // Handle cell object with text
      }
      return JSON.stringify(value); // Fallback for other objects
    }
    return value.toString(); // Convert any other value to a string
  };

  // Function to validate headers
  const validateHeaders = (headers) => {
    return JSON.stringify(headers) === JSON.stringify(expectedHeaders);
  };

  // Function to highlight header errors
  const highlightHeaderErrors = (headerRow, headers) => {
    headers.forEach((header, index) => {
      if (header !== expectedHeaders[index]) {
        const cell = headerRow.getCell(index + 1);
        applyErrorStyle(cell);
        cell.note = `Expected "${expectedHeaders[index]}"`;
      }
    });
  };

  // Function to validate individual row
  const validateRow = (row, rowNumber, emailSet) => {
    const rowErrors = [];
    const nameCell = row.getCell(1);
    const emailCell = row.getCell(2);
    const ageCell = row.getCell(3);

    // Name validation
    if (!nameCell.value || nameCell.value.toString().trim() === '') {
      rowErrors.push(`Row ${rowNumber}: Name is required.`);
      applyErrorStyle(nameCell);
    }

    // Email validation
    let emailValue = getCellValue(emailCell);
    emailValue = String(emailValue).trim(); // Convert to string and trim

    if (!emailValue) {
      rowErrors.push(`Row ${rowNumber}: Email is required.`);
      applyErrorStyle(emailCell);
    } else if (!validateEmail(emailValue)) {
      rowErrors.push(`Row ${rowNumber}: Invalid email format.`);
      applyErrorStyle(emailCell);
    } else if (emailSet.has(emailValue.toLowerCase())) {
      rowErrors.push(`Row ${rowNumber}: Email must be unique.`);
      applyErrorStyle(emailCell);
    } else {
      emailSet.add(emailValue.toLowerCase());
    }

    // Age validation
    if (
      ageCell.value === undefined ||
      ageCell.value === null ||
      String(ageCell.value).trim() === ''
    ) {
      rowErrors.push(`Row ${rowNumber}: Age is required.`);
      applyErrorStyle(ageCell);
    } else if (isNaN(ageCell.value)) {
      rowErrors.push(`Row ${rowNumber}: Age must be a number.`);
      applyErrorStyle(ageCell);
    } else if (ageCell.value < 0 || ageCell.value > 120) {
      rowErrors.push(`Row ${rowNumber}: Age must be between 0 and 120.`);
      applyErrorStyle(ageCell);
    }

    return rowErrors;
  };

  // Function to apply error style to cell
  const applyErrorStyle = (cell) => {
    cell.fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'FFFFC7CE' } // Light red background
    };
    cell.font = {
      color: { argb: 'FF9C0006' } // Dark red font
    };
    cell.border = {
      top: { style: 'thin', color: { argb: 'FF9C0006' } },
      left: { style: 'thin', color: { argb: 'FF9C0006' } },
      bottom: { style: 'thin', color: { argb: 'FF9C0006' } },
      right: { style: 'thin', color: { argb: 'FF9C0006' } }
    };
  };

  // Function to validate email format
  const validateEmail = (email) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  // Function to export Excel template
  const handleExportTemplate = async () => {
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Template');

    // Add headers
    const headerRow = worksheet.addRow(expectedHeaders);

    // Style headers
    headerRow.eachCell((cell) => {
      cell.font = { bold: true };
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFB0E0E6' } // Light blue background
      };
      cell.border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' }
      };
      cell.alignment = { vertical: 'middle', horizontal: 'center' };
    });

    // Set column widths
    worksheet.columns = [
      { key: 'Name', width: 20 },
      { key: 'Email', width: 30 },
      { key: 'Age', width: 10 }
    ];

    const buffer = await workbook.xlsx.writeBuffer();
    saveAs(new Blob([buffer]), 'Excel_Template.xlsx');
  };

  // Function to download Excel with errors
  const handleDownloadErrorsFile = async () => {
    if (workbookWithErrors) {
      const buffer = await workbookWithErrors.xlsx.writeBuffer();
      saveAs(new Blob([buffer]), 'Excel_Errors.xlsx');
    }
  };

  // Function to export valid data to Excel
  const handleExportData = async () => {
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Valid Data');

    // Add headers
    const headerRow = worksheet.addRow(expectedHeaders);

    // Style headers
    headerRow.eachCell((cell) => {
      cell.font = { bold: true };
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFB0E0E6' } // Light blue background
      };
      cell.border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' }
      };
      cell.alignment = { vertical: 'middle', horizontal: 'center' };
    });

    // Set column widths
    worksheet.columns = [
      { header: 'Name', key: 'name', width: 20 },
      { header: 'Email', key: 'email', width: 30 },
      { header: 'Age', key: 'age', width: 10 }
    ];

    // Add data rows
    data.forEach((row) => {
      worksheet.addRow(row);
    });

    const buffer = await workbook.xlsx.writeBuffer();
    saveAs(new Blob([buffer]), 'Excel_Valid_Data.xlsx');
  };

  // Function to close the modal
  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div style={{ padding: '20px' }}>
      <h2>Excel Handler with Validation</h2>

      {/* Download Template Button */}
      <button onClick={handleExportTemplate} style={{ marginRight: '10px' }}>
        Download Excel Template
      </button>

      {/* Export Valid Data Button */}
      <button onClick={handleExportData} style={{ marginRight: '10px' }}>
        Import Valid Data
      </button>

      {/* File Upload Input */}
      <input type="file" accept=".xlsx" onChange={handleImport} />

      {/* Modal for Displaying Errors */}
      {isModalOpen && (
        <div
          style={{
            position: 'fixed',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            zIndex: 1000,
            backgroundColor: 'white',
            padding: '20px',
            borderRadius: '10px',
            boxShadow: '0 2px 10px rgba(0,0,0,0.1)',
            width: '400px'
          }}>
          <h3 style={{ color: 'red' }}>Validation Errors:</h3>
          <ul>
            {errors.map((error, index) => (
              <li key={index} style={{ color: 'red' }}>
                {error}
              </li>
            ))}
          </ul>
          <p style={{ marginTop: '10px', fontWeight: 'bold' }}>
            Please download the error-highlighted file, correct the issues, and upload again.
          </p>
          <button onClick={handleDownloadErrorsFile} style={{ marginRight: '10px' }}>
            Download Excel with Errors Highlighted
          </button>
          <button onClick={closeModal}>Close</button>
        </div>
      )}

      {/* Modal Overlay */}
      {isModalOpen && (
        <div
          style={{
            position: 'fixed',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            zIndex: 999
          }}
          onClick={closeModal}></div>
      )}

      {/* Display Valid Data */}
      {data.length > 0 && (
        <div style={{ marginTop: '20px' }}>
          <h3>Valid Data:</h3>
          <table border="1" cellPadding="5" cellSpacing="0">
            <thead>
              <tr>
                {expectedHeaders.map((header, index) => (
                  <th key={index}>{header}</th>
                ))}
              </tr>
            </thead>
            <tbody>
              {data.map((row, rowIndex) => (
                <tr key={rowIndex}>
                  <td>{row.name}</td>
                  <td>{row.email}</td>
                  <td>{row.age}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
    </div>
  );
};

export default ExcelHandler;

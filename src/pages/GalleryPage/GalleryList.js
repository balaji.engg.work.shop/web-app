import React, { useEffect, useState } from 'react';
import { GalleryCard } from '../../components';
import { getAllGallery } from '../../action/galleryAction';
import { useDispatch, useSelector } from 'react-redux';
import { message } from 'antd';

const GalleryList = ({ onEdit }) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const dispatch = useDispatch();

  const {
    data: galleryList1,
    loading: loadingGalleryList,
    error: errorGalleryList
  } = useSelector((state) => state.getAllGalleryReducer);

  // ✅ Fetch Gallery Data (Only if not already present)
  const fetchGallery = async (page = 1) => {
    // ✅ Prevent dispatch if data for this page already exists
    if (galleryList1 && galleryList1.pagination?.currentPage === page) {
      message.info(`Skipping fetch, data for page ${page} already loaded.`);
      return;
    }

    dispatch(
      getAllGallery({
        params: {
          page: page,
          limit: 20
        }
      })
    );
    setCurrentPage(page);
  };

  useEffect(() => {
    if (galleryList1) {
      setTotalPages(galleryList1?.pagination?.totalPages || 0);
    }
  }, [galleryList1]);

  // ✅ Load data only if not already present
  useEffect(() => {
    if (!galleryList1) {
      fetchGallery(1);
    }
  }, []);

  // ✅ Handle Pagination
  const handlePageChange = (page) => {
    if (page >= 1 && page <= totalPages) {
      fetchGallery(page);
    }
  };

  return (
    <div>
      {/* ✅ Loading State */}
      {loadingGalleryList && <p className="text-center text-gray-500">Loading gallery...</p>}

      {/* ✅ Gallery List */}
      <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 gap-4">
        {(!galleryList1?.data || galleryList1?.data.length === 0) && !loadingGalleryList && (
          <p className="text-center col-span-3 text-gray-500">No gallery found.</p>
        )}

        {galleryList1?.data?.map((item) => (
          <GalleryCard item={item} key={item._id} onEdit={onEdit} />
        ))}
      </div>

      {/* ✅ Pagination */}
      <div className="mt-5 flex justify-center gap-2">
        {/* ✅ Previous Button */}
        <button
          onClick={() => handlePageChange(currentPage - 1)}
          disabled={currentPage === 1}
          className={`px-4 py-2 rounded font-semibold transition-all duration-300 ${
            currentPage === 1
              ? 'bg-gray-300 text-gray-500 cursor-not-allowed'
              : 'bg-gradient-to-r from-blue-500 to-blue-600 text-white hover:scale-105 shadow-md'
          }`}>
          Prev
        </button>

        {/* ✅ Page Numbers */}
        {Array.from({ length: totalPages }).map((_, index) => (
          <button
            key={index}
            onClick={() => handlePageChange(index + 1)}
            className={`px-3 py-2 rounded font-semibold transition-all duration-300 border ${
              currentPage === index + 1
                ? 'bg-gradient-to-r from-[#ff7eb3] to-[#ff758f] text-white shadow-lg scale-105'
                : 'bg-gray-100 text-gray-700 hover:bg-gray-200 hover:scale-105'
            }`}>
            {index + 1}
          </button>
        ))}

        {/* ✅ Next Button */}
        <button
          onClick={() => handlePageChange(currentPage + 1)}
          disabled={currentPage === totalPages}
          className={`px-4 py-2 rounded font-semibold transition-all duration-300 ${
            currentPage === totalPages
              ? 'bg-gray-300 text-gray-500 cursor-not-allowed'
              : 'bg-gradient-to-r from-blue-500 to-blue-600 text-white hover:scale-105 shadow-md'
          }`}>
          Next
        </button>
      </div>
    </div>
  );
};

export default GalleryList;

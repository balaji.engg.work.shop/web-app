import React, { useCallback, useState } from 'react';
import { AddEditGalleryModel, SearchInputField } from '../../components';
import { AddButton } from '../../components/InputComponents/MyButton';
import { getAllGallery, getGalleryListByShopId } from '../../action/galleryAction';
import GalleryList from './GalleryList';

export const GalleryPage = () => {
  const [option, setOptions] = useState('Add');
  const [open, setOpen] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [gallery, setGallery] = useState(null);
  // const [currentTab, setCurrentTab] = useState('1');

  // const {
  //   data: galleryListByShopId,
  //   loading,
  //   error
  // } = useSelector((state) => state.getGalleryListByShopIdReducer);
  // const {
  //   data: galleryList,
  //   loading: loadingGalleryList,
  //   error: errorGalleryList
  // } = useSelector((state) => state.getGalleryCategoryReducer);

  const handleOpenAddGalleryModel = useCallback(() => {
    setOptions('Add');
    setOpen(true);
  }, [open]);

  const handleCloseModel = useCallback(() => {
    setOpen(false);
  }, [open]);

  const onEdit = useCallback(
    (item) => {
      setGallery(item);
      setOptions('Edit');
      setOpen(true);
    },
    [gallery, open, option]
  );

  return (
    <div className="w-full h-screen bg-gray-100">
      <div className="flex items-center justify-between border-t border-l border-gray-200 p-3 bg-white pr-20">
        <h2 className="text-2xl font-semibold m-0 px-2">Gallery</h2>
        <div className="w-96">
          <SearchInputField
            value={searchText}
            onChange={setSearchText}
            placeholder="Search product by name"
          />
        </div>
      </div>
      <AddEditGalleryModel
        open={open}
        onCancel={handleCloseModel}
        option={option}
        gallery={gallery}
      />
      <div className="flex justify-between mr-4 p-2 pl-4">
        <AddButton onClick={handleOpenAddGalleryModel} label="Gallery" />
      </div>
      <div className="p-2 pl-4">
        <GalleryList onEdit={onEdit} />
      </div>
    </div>
  );
};

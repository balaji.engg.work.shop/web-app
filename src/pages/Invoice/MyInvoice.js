// src/Invoice.js
import React, { useState } from 'react';
import { Button, Layout } from 'antd';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { Box, Flex, SimpleGrid } from '@chakra-ui/react';
import { inWords } from '../../utils/validateData';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { ADD_NEW_SELL_PRODUCTS_DATA_RESET } from '../../constants/sellProductConstants';
const { Content } = Layout;

const data = [
  {
    title: 'S.N.',
    width: '20px',
    required: false
  },

  {
    title: 'Description of Goods/Services',
    width: '350px',
    required: true
  },
  {
    title: 'HSN/SAC',
    width: '200px',
    required: true
  },
  {
    title: 'Quantity',
    width: '100px',
    required: true
  },
  {
    title: 'Rate',
    width: '100px',
    required: true
  },
  {
    title: 'Per',
    width: '100px',
    required: true
  },
  {
    title: 'Amount',
    width: '100px',
    required: true
  }
];

const MyInVoice = ({ bill, customer, shop }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const printDocument = () => {
    const input = document.getElementById('invoice');
    html2canvas(input).then((canvas) => {
      const imgData = canvas.toDataURL('image/png');
      const pdf = new jsPDF('p', 'mm', 'a4');
      const imgWidth = 210; // A4 width in mm
      const pageHeight = 295; // A4 height in mm
      const imgHeight = (canvas.height * imgWidth) / canvas.width;
      let heightLeft = imgHeight;

      let position = 0;
      pdf.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        pdf.addPage();
        pdf.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }

      pdf.save('invoice.pdf');
    });
  };

  return (
    <Layout>
      <Content style={{ padding: '0 50px' }}>
        <div style={{ padding: 24 }} className="h-[80vh] overflow-y-auto  ">
          <Flex gap="4" pb="4">
            <Button
              type="primary"
              onClick={() => {
                dispatch({ type: ADD_NEW_SELL_PRODUCTS_DATA_RESET });
                navigate(-1);
              }}
              style={{ marginTop: '20px' }}>
              Back
            </Button>
            <Button type="primary" onClick={printDocument} style={{ marginTop: '20px' }}>
              Download Invoice
            </Button>
          </Flex>

          <div
            id="invoice"
            style={{ padding: '20px', width: '210mm', height: '297mm', backgroundColor: '#fff' }}>
            <h1 style={{ textAlign: 'center', fontSize: '24px', fontWeight: '600' }}>
              {bill?.isGST ? 'Tax  Invoice' : 'Invoice'}
            </h1>
            <Box border="1px" color={'#000000'}>
              <SimpleGrid columns={[2, 2, 2]}>
                <Box fontSize="12px" gap="0" p="4px">
                  <h1 style={{ padding: 0, margin: 0, fontSize: '12px', fontWeight: '600' }}>
                    {shop?.shopName}
                  </h1>
                  <h1 style={{ padding: 0, margin: 0 }}>
                    {shop?.address},{shop?.city},{shop?.state}
                  </h1>
                  <h1 style={{ padding: 0, margin: 0 }}>Pin code : {shop?.pinCode}</h1>
                  <h1 style={{ padding: 0, margin: 0 }}>Mobile No: {shop?.shopMobileNo}</h1>
                  {bill?.isGST && <h1 style={{ padding: 0, margin: 0 }}>GSTIN: {shop?.gst}</h1>}
                  <h1 style={{ padding: 0, margin: 0 }}>Email: {shop?.shopEmail}</h1>
                </Box>
                <Box fontSize="12px" gap="0" borderLeft="1px">
                  <SimpleGrid columns={[2, 2, 2]}>
                    <Box borderRight="1px" p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Invoice No:</h1>
                      <h1 style={{ padding: 0, margin: 0 }}>{bill?.billNo}</h1>
                    </Box>
                    <Box p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Invoice Date:</h1>
                      <h1 style={{ padding: 0, margin: 0 }}>
                        {new Date(bill?.billDate)?.toDateString()}
                      </h1>
                    </Box>
                  </SimpleGrid>
                  <SimpleGrid columns={[2, 2, 2]} borderTop="1px">
                    <Box borderRight="1px" p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Delivery Note</h1>
                    </Box>
                    <Box p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Mode/Terms of Payment</h1>
                      <h1 style={{ padding: 0, margin: 0, font: 'bold', fontWeight: '600' }}>
                        {bill?.modeOfTransaction === 'Both'
                          ? 'Online+Cash'
                          : bill?.modeOfTransaction}
                      </h1>
                    </Box>
                  </SimpleGrid>{' '}
                  <SimpleGrid columns={[2, 2, 2]} borderTop="1px">
                    <Box borderRight="1px" height="100%" p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Supplier's Ref</h1>
                    </Box>
                    <Box p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Other Reference(s)</h1>
                    </Box>
                  </SimpleGrid>
                </Box>
              </SimpleGrid>
              <SimpleGrid columns={[2, 2, 2]} borderTop="1px">
                <Box fontSize="10px" gap="0" p="4px">
                  <h1 style={{ padding: 0, margin: 0, fontWeight: '600', fontSize: '12px' }}>
                    Consignee
                  </h1>
                  <h1 style={{ padding: 0, margin: 0 }}>{customer?.customerName}</h1>
                  <h1 style={{ padding: 0, margin: 0 }}>{customer?.address}</h1>
                </Box>
                <Box fontSize="12px" gap="0" borderLeft="1px" p="4px">
                  <SimpleGrid columns={[2, 2, 2]} p="4px">
                    <Box borderRight="1px">
                      <h1 style={{ padding: 0, margin: 0 }}>Buyers Order No.</h1>
                      <h1 style={{ padding: 0, margin: 0 }}></h1>
                    </Box>
                    <Box p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Dated</h1>
                      <h1 style={{ padding: 0, margin: 0 }}></h1>
                    </Box>
                  </SimpleGrid>
                  <SimpleGrid columns={[2, 2, 2]} borderTop="1px">
                    <Box borderRight="1px" p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Despatch Doc No.e</h1>
                    </Box>
                    <Box p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Delivery Note Date</h1>
                      <h1 style={{ padding: 0, margin: 0, font: 'bold', fontWeight: '600' }}></h1>
                    </Box>
                  </SimpleGrid>{' '}
                  <SimpleGrid columns={[2, 2, 2]} borderTop="1px">
                    <Box borderRight="1px" height="auto" p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Despatched through</h1>
                      <h1 style={{ padding: 0, margin: 0 }}>{''}</h1>
                    </Box>
                    <Box p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Destination</h1>
                      <h1 style={{ padding: 0, margin: 0 }}></h1>
                    </Box>
                  </SimpleGrid>
                </Box>
              </SimpleGrid>

              <SimpleGrid columns={[2, 2, 2]} borderTop="1px">
                <Box fontSize="10px" gap="0" p="4px">
                  <h1 style={{ padding: 0, margin: 0, fontSize: '12px', fontWeight: '600' }}>
                    Buyer
                  </h1>
                  <h1 style={{ padding: 0, margin: 0 }}>{customer?.customerName}</h1>
                  <h1 style={{ padding: 0, margin: 0 }}>{customer?.address}</h1>
                  {/* <h1 style={{ padding: 0, margin: 0 }}>Pin code : 802301</h1> */}
                  {bill?.isGST && (
                    <h1 style={{ padding: 0, margin: 0 }}>GSTIN.UIN: {customer?.gst}</h1>
                  )}
                  <h1 style={{ padding: 0, margin: 0 }}>State Name : {customer?.state}</h1>
                  <h1 style={{ padding: 0, margin: 0 }}>Place of supply : {customer?.state}</h1>
                  <h1 style={{ padding: 0, margin: 0 }}>Contact Name : {customer?.customerName}</h1>
                </Box>
                <Box fontSize="12px" gap="0" borderLeft="1px">
                  <SimpleGrid columns={[2, 2, 2]}>
                    <Box borderRight="1px" p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Bill of Lading/LR-RR No.</h1>
                      <h1 style={{ padding: 0, margin: 0 }}></h1>
                    </Box>
                    <Box p="4px">
                      <h1 style={{ padding: 0, margin: 0 }}>Motor Vehicle No.</h1>
                      <h1 style={{ padding: 0, margin: 0 }}></h1>
                    </Box>
                  </SimpleGrid>

                  <Box borderTop="1px" p="4px">
                    <h1 style={{ padding: 0, margin: 0 }}>Terms of Delivery</h1>
                  </Box>
                </Box>
              </SimpleGrid>
              <table style={{ border: '1px solid #000000' }}>
                <tr>
                  {data?.map((d, index) => (
                    <th
                      style={{
                        border: '1px solid #000000',
                        textAlign: 'center',
                        padding: '2px',
                        width: d?.width
                      }}
                      key={index}>
                      {d.title}
                    </th>
                  ))}
                </tr>
                {bill?.products?.map((product, index) => (
                  <tr key={index} style={{ textAlign: 'center', padding: '2px' }}>
                    <td>{index + 1} </td>
                    <td>{product.description}</td>
                    <td>{product.HSN_SAC_Code}</td>
                    <td>{product.quantity}</td>
                    <td>{product.price}</td>
                    <td>{product.priceType}</td>
                    <td>{product.total}</td>
                  </tr>
                ))}
                <tr style={{ height: '30px', textAlign: 'center', padding: '2px' }}>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td style={{ textAlign: 'right' }}>Total</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td style={{ textAlign: 'left' }}>{bill?.total}</td>
                </tr>
                {bill?.isGST ? (
                  bill?.isSameState ? (
                    <>
                      <tr>
                        <td></td>
                        <td style={{ textAlign: 'right' }}>CGST</td>
                        <td></td>
                        <td></td>
                        <td>9%</td>
                        <td></td>
                        <td style={{ textAlign: 'left' }}>{bill?.gstAmount / 2}</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td style={{ textAlign: 'right' }}>SGST</td>
                        <td></td>
                        <td></td>
                        <td>9%</td>
                        <td></td>
                        <td style={{ textAlign: 'left' }}>{bill?.gstAmount / 2}</td>
                      </tr>
                    </>
                  ) : (
                    <tr>
                      <td></td>
                      <td style={{ textAlign: 'right' }}>IGST </td>
                      <td></td>
                      <td>18%</td>
                      <td></td>
                      <td></td>
                      <td style={{ textAlign: 'left' }}>{bill?.gstAmount}</td>
                    </tr>
                  )
                ) : null}
                <tr>
                  <td></td>
                  <td style={{ textAlign: 'right' }}>Net Total</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>

                  <td style={{ textAlign: 'left' }}>{bill?.netTotal}</td>
                </tr>
                <tr>
                  <td></td>
                  <td style={{ textAlign: 'right' }}>Paid Amount</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>

                  <td style={{ textAlign: 'left' }}>{bill?.paidAmount}</td>
                </tr>{' '}
                <tr>
                  <td></td>
                  <td style={{ textAlign: 'right' }}>Discount Amount</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>

                  <td style={{ textAlign: 'left' }}>{bill?.discountAmount}</td>
                </tr>
              </table>
              <div style={{ border: '1px solid #000000', padding: '2px' }}>
                <h1 style={{ padding: 0, margin: 0 }}>Amount In word</h1>
                <h1 style={{ padding: 0, margin: 0, fontWeight: '600' }}>
                  {inWords(bill?.netTotal)}
                </h1>
              </div>

              <SimpleGrid columns={[2, 2, 2]}>
                <Box p="1px">
                  <h1 style={{ padding: 0, margin: 0 }}>Declaration</h1>
                  <h1 style={{ padding: 0, margin: 0 }}>
                    We declare that this invoice shows the actual price of the goods described and
                    that all particulars are true and correct
                  </h1>
                </Box>
                <Flex
                  border="1px"
                  flexDir="column"
                  justifyContent="space-between"
                  align="center"
                  p="1">
                  <h1 style={{ padding: 0, margin: 0 }}>for {shop?.shopName}</h1>
                  <h1 style={{ padding: 0, margin: 0 }}>Authorized Signatory</h1>
                </Flex>
              </SimpleGrid>
            </Box>
          </div>
        </div>
      </Content>
    </Layout>
  );
};

export default MyInVoice;

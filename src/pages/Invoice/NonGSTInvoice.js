import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { ADD_NEW_SELL_PRODUCTS_DATA_RESET } from '../../constants/sellProductConstants';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { inWords } from '../../utils/validateData';

const NonGSTInvoice = ({ bill, customer, shop }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const printDocument = () => {
    const input = document.getElementById('invoice');
    const dpi = window.devicePixelRatio || 2; // Adjust resolution based on device pixel ratio

    html2canvas(input, { scale: dpi, useCORS: true }).then((canvas) => {
      const imgData = canvas.toDataURL('image/png');
      const pdf = new jsPDF('p', 'mm', 'a4');
      const imgWidth = 210;
      const imgHeight = (canvas.height * imgWidth) / canvas.width;
      pdf.addImage(imgData, 'PNG', 0, 0, imgWidth, imgHeight);
      pdf.save('invoice.pdf');
    });
  };

  return (
    <div className="mt-4 bg-gray-100 h-full">
      <div className="flex justify-between mb-4">
        <button
          onClick={() => {
            dispatch({ type: ADD_NEW_SELL_PRODUCTS_DATA_RESET });
            navigate(-1);
          }}
          className="bg-blue-500 text-white px-4 py-2 rounded">
          Back
        </button>
        <button onClick={printDocument} className="bg-green-500 text-white px-4 py-2 rounded">
          Download Invoice
        </button>
      </div>

      <div id="invoice" className="font-bold text-2xl">
        <div className="bg-white p-8 border border-4 rounded-md">
          <h1 className="text-center text-4xl font-bold pb-8 border-b">
            {bill?.isGST ? 'Tax Invoice' : 'Invoice'}
          </h1>

          {/* Shop Details */}
          <div className="grid grid-cols-2 gap-4  border-b pb-4">
            <div className="text-left space-y-1 ">
              <h2 className="font-semibold">{shop?.shopName}</h2>
              <p>
                {shop?.address}, {shop?.city}, {shop?.state}
              </p>
              <p>Pin: {shop?.pinCode}</p>
              <p>Phone: {shop?.shopMobileNo}</p>
              {bill?.isGST && <p>GSTIN: {shop?.gst}</p>}
            </div>
            <div className="text-right space-y-1 ">
              <p>
                <strong>Invoice No:</strong> {bill?.billNo}
              </p>
              <p>
                <strong>Date:</strong> {new Date(bill?.billDate).toDateString()}
              </p>
              <p>
                <strong>Payment Mode:</strong> {bill?.modeOfTransaction}
              </p>
            </div>
          </div>

          {/* Customer Details */}
          <div className="border-b pb-4 text-left space-y-1">
            <h2 className="">Bill To:</h2>
            <p className="">{customer?.customerName}</p>
            <p>{customer?.address}</p>
            {customer?.state && <p>State: {customer?.state}</p>}
            {bill?.isGST && <p>GSTIN: {customer?.gst}</p>}
          </div>

          {/* Product Table */}
          <div className="grid grid-cols-12 border border-collapse w-full ">
            {/* Header */}
            <div className="col-span-1 bg-gray-100 border p-2 text-center pb-4">S.N.</div>
            <div className="col-span-7 bg-gray-100 border p-2 text-center pb-4">Description</div>
            <div className="col-span-1 bg-gray-100 border p-2 text-center pb-4">Quantity</div>
            <div className="col-span-1 bg-gray-100 border p-2 text-center pb-4">Rate</div>
            <div className="col-span-1 bg-gray-100 border p-2 text-center pb-4">Unit / Per</div>
            <div className="col-span-1 bg-gray-100 border p-2 text-center pb-4">Total</div>
          </div>
          {/* Body */}
          {bill?.products?.map((product, index) => (
            <div className="grid grid-cols-12 border border-collapse w-full">
              <div key={index} className="col-span-1 border p-2 text-center pb-4">
                {index + 1}
              </div>
              <div className="col-span-7 border p-2 text-start">{product.description}</div>
              <div className="col-span-1 border p-2 text-center pb-4">{product.quantity}</div>
              <div className="col-span-1 border p-2 text-center pb-4">₹{product.price}</div>
              <div className="col-span-1 border p-2 text-center pb-4">{product.priceType}</div>
              <div className="col-span-1 border p-2 text-center pb-4">₹{product.total}</div>
            </div>
          ))}
          <div className="grid grid-cols-12 border border-collapse w-full">
            {/* Footer */}
            <div className="col-span-10 border-t bg-gray-50 p-2 text-right pb-4">Total</div>
            <div className="col-span-2 border-t bg-gray-50 p-2 text-right pb-4">₹{bill?.total}</div>
          </div>

          <div className="grid grid-cols-3 gap-4  border-b pb-4">
            <div className="text-left mt-4 space-y-1  col-span-2 text-lg">
              <p>
                <strong>Gross in Words:</strong> {inWords(bill?.paidAmount)}
              </p>
            </div>
            <div className="text-right mt-4 space-y-1 col-span-1">
              {bill?.isGST ? (
                bill?.isSameState ? (
                  <>
                    <p>
                      <strong>CGST 9%:</strong> ₹{bill?.gstAmount / 2}
                    </p>
                    <p>
                      <strong>SGST 9%:</strong> ₹{bill?.gstAmount / 2}
                    </p>
                  </>
                ) : (
                  <p>
                    <strong>IGST 18%:</strong> ₹{bill?.gstAmount}
                  </p>
                )
              ) : null}

              <p>
                <strong>Net Total:</strong> ₹{bill?.netTotal}
              </p>
              {bill?.discountAmount && (
                <p>
                  <strong>Discount:</strong> ₹{bill?.discountAmount}
                </p>
              )}
              <p>
                <strong>Paid Amount:</strong> ₹{bill?.paidAmount}
              </p>
            </div>
          </div>
          <div className="grid grid-cols-2 w-full border-t border-gray-300">
            {/* ✅ Declaration Section */}
            <div className="border-r border-gray-300 p-4 space-y-1">
              <p className="">Declaration</p>
              <p className=" text-gray-600">
                We declare that this invoice shows the actual price of the goods described and that
                all particulars are true and correct.
              </p>
            </div>

            {/* ✅ Authorized Signatory Section */}
            <div className="flex flex-col justify-center items-center p-4 space-y-1">
              <p>
                <strong>for </strong>
                {shop?.shopName}
              </p>
              <p className="text-gray-600">Authorized Signatory</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NonGSTInvoice;

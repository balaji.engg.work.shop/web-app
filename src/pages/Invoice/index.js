import React from 'react';
import 'antd/dist/reset.css';
import MyInVoice from './MyInvoice';
import { useLocation, useParams } from 'react-router-dom';
import NonGSTInvoice from './NonGSTInvoice';

const Invoice = () => {
  const location = useLocation();
  const { bill, customer, shop } = location.state;

  const { billNo } = useParams(); // Extracts the 'id' from the URL
  return <NonGSTInvoice bill={bill} customer={customer} shop={shop} />;
};

export default Invoice;

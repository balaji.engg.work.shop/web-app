import { Box, Button, Stack, Flex, Text } from '@chakra-ui/react';
import { useCallback, useEffect, useRef, useState } from 'react';
import { message, Tooltip } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import {
  addProductInBulk,
  deleteAllProducts,
  deleteProduct,
  getAllProduct
} from '../../action/productActions';
import { useNavigate } from 'react-router-dom';
import {
  ADD_PRODUCTS_IN_BULK_RESET,
  DELETE_ALL_PRODUCT_RESET,
  DELETE_PRODUCT_RESET,
  GET_ALL_PRODUCT_RESET
} from '../../constants/productConstants';
import { SIGN_IN } from '../../constants/routeConstant';
import { ExportRecord } from '../../components/ExportRecord';
import {
  AddEditProductModal,
  ImportExcelData,
  ProductTable,
  SearchInputField
} from '../../components';
import { AddButton, DeleteButton } from '../../components/InputComponents/MyButton';
import { ProductCard } from '../../components/molecule/ProductCard';

export function Products() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const hiddenFileInput = useRef(null);
  const [listView, setListView] = useState(true);
  const [option, setOptions] = useState('Add');
  const [open, setOpen] = useState(false);
  const [searchText, setSearchText] = useState('');

  const [selectedProduct, setSelectedProduct] = useState(null);

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const productList = useSelector((state) => state.productList);
  const { error, loading, products } = productList;

  const productDelete = useSelector((state) => state.productDelete);
  const {
    error: errorProductDelete,
    loading: loadingProductDelete,
    success: successProductDelete
  } = productDelete;

  const allProductDelete = useSelector((state) => state.allProductDelete);
  const {
    error: errorAllProductDelete,
    loading: loadingAllProductDelete,
    success: successAllProductDelete
  } = allProductDelete;

  const addProducts = useSelector((state) => state.addProducts);
  const {
    error: errorAddProducts,
    loading: loadingAddProducts,
    success: successAddProducts,
    data
  } = addProducts;

  useEffect(() => {
    if (!userInfo) navigate(SIGN_IN);
    if (!(products?.length > 0)) {
      dispatch(getAllProduct());
    }
  }, []);

  useEffect(() => {
    if (errorAllProductDelete) {
      message.error(errorAllProductDelete);
    }

    if (successAllProductDelete) {
      message.success('All Product deleted!');
      dispatch({ type: DELETE_ALL_PRODUCT_RESET });
      dispatch(getAllProduct());
    }
  }, [successAllProductDelete, errorAllProductDelete]);

  useEffect(() => {
    if (error) {
      message.error(error);
      dispatch({ type: GET_ALL_PRODUCT_RESET });
    }
    if (errorProductDelete) {
      message.error(errorProductDelete);
      dispatch({ type: DELETE_PRODUCT_RESET });
    }
    if (successProductDelete) {
      message.success('Product deleted!');
      dispatch({ type: DELETE_PRODUCT_RESET });
      dispatch(getAllProduct());
    }
  }, [error, errorProductDelete, successProductDelete, dispatch]);

  useEffect(() => {
    if (errorAddProducts) {
      message.error(errorAddProducts);
      dispatch({ type: ADD_PRODUCTS_IN_BULK_RESET });
    }
    if (data) {
      message.success('Successfully added products in bulk');
      dispatch({ type: ADD_PRODUCTS_IN_BULK_RESET });
      dispatch(getAllProduct());
    }
  }, [errorAddProducts, data]);

  const handleOpenEditProductModel = useCallback(
    (product) => {
      setOptions('Edit');
      setOpen(true);
      setSelectedProduct(product);
    },
    [open]
  );

  const handleOpenAddProductModel = useCallback(() => {
    setOptions('Add');
    setOpen(true);
  }, [open]);

  const handleCloseModel = useCallback(() => {
    setOpen(false);
  }, [open]);

  const handleDeleteProduct = (e, product) => {
    dispatch(deleteProduct(product.productId));
  };

  const handelSave = (products) => {
    if (products?.length > 0) {
      dispatch(addProductInBulk(products));
      hiddenFileInput.current.value = '';
    } else message.error('File is empty');
  };

  const handleDeleteAll = () => {
    if (window.confirm('Do you really want to delete all product?')) {
      dispatch(deleteAllProducts());
    }
  };

  const dd = [
    { header: 'HSN SAC Code', key: 'HSN_SAC_Code', width: 15 },
    { header: 'Category', key: 'category', width: 20 },
    { header: 'Product Name', key: 'description', width: 50 },
    { header: 'Brand Name', key: 'brand', width: 20 },
    { header: 'Unit', key: 'priceType', width: 15 },
    { header: 'Available Quantity', key: 'quantity', width: 20 },
    { header: 'Marketing price', key: 'MRP', width: 20 },
    { header: 'Selling price', key: 'sellingPrice', width: 20 },
    { header: 'Weight Per Piece(Gram)', key: 'weightPerPiece', width: 40 },
    { header: 'Images', key: 'images', width: 80 }
  ];

  const dd1 = [
    { header: 'HSN SAC Code', key: 'HSN_SAC_Code', width: 15 },
    { header: 'Category', key: 'category', width: 20 },
    { header: 'Product Name', key: 'description', width: 50 },
    { header: 'Brand Name', key: 'brand', width: 20 },
    { header: 'Unit', key: 'priceType', width: 15 },
    { header: 'Available Quantity', key: 'quantity', width: 20 },
    { header: 'Purchasing price', key: 'purchasingPrice', width: 20 },
    { header: 'Marketing price', key: 'MRP', width: 20 },
    { header: 'Selling price', key: 'sellingPrice', width: 20 },
    { header: 'Weight Per Piece(Gram)', key: 'weightPerPiece', width: 40 }
  ];

  return (
    <Box bgColor="#F5F5F5" m="0">
      <AddEditProductModal
        open={open}
        onCancel={handleCloseModel}
        option={option}
        product={selectedProduct}
      />
      <Flex p="3" pl="5" bgColor="#FFFFFF" justifyContent={'space-between'}>
        <Text fontWeight="600" fontSize="2xl" m="0">
          Product <span className="text-lg">({products?.length})</span>
        </Text>
        <div className="w-96">
          <SearchInputField
            value={searchText}
            onChange={setSearchText}
            placeholder="Search product by name"
          />
        </div>
      </Flex>
      <Box p="2" pl="5">
        <div className="flex justify-between mr-4">
          <AddButton onClick={handleOpenAddProductModel} label="Product" />
          <ExportRecord
            data={[]}
            columns={dd1}
            label="Download Template"
            fileName="productBulUploadTemplate"
          />
          <ImportExcelData
            columnsConfig={dd1}
            handelSave={handelSave}
            hiddenFileInput={hiddenFileInput}
          />
          <ExportRecord data={products} columns={dd} label="Download list" fileName="StockList" />
          {userInfo?.isAdmin && <DeleteButton onClick={handleDeleteAll} label="Delete All" />}
          <div className="flex gap-4 items-center">
            <Tooltip title="Click to see the list view">
              <div
                className={`truncate px-1 rounded flex items-center gap-2`}
                onClick={() => setListView(true)}>
                <i
                  className={`fi fi-rr-table-list
                        text-[16px]
                        ${listView && 'text-[#129BFF]'}`}></i>
              </div>
            </Tooltip>
            <Tooltip title="Click to see the grid view">
              <div
                className={`truncate px-1 rounded flex items-center gap-2`}
                onClick={() => setListView(false)}>
                <i
                  className={`fi fi-sr-apps
                        text-[16px]
                        ${!listView && 'text-[#129BFF]'}`}></i>
              </div>
            </Tooltip>
          </div>
        </div>

        <div className="py-2">
          {listView ? (
            <ProductTable
              data={products?.filter((product) =>
                product.description.toLowerCase()?.includes(searchText.toLowerCase())
              )}
              loading={loading}
              handelEditProduct={handleOpenEditProductModel}
              confirm={handleDeleteProduct}
            />
          ) : (
            <div className="flex flex-wrap gap-5 h-[82vh] overflow-y-auto py-4">
              {products
                ?.filter((product) =>
                  product.description.toLowerCase()?.includes(searchText.toLowerCase())
                )
                .map((product) => (
                  <ProductCard product={product} />
                ))}
            </div>
          )}
        </div>
      </Box>
    </Box>
  );
}

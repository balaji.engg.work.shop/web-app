import { Box, Flex, Text, Button, SimpleGrid } from '@chakra-ui/react';
import { Select, Input, message } from 'antd';
import { useEffect, useState, useCallback } from 'react';
import { BiSave } from 'react-icons/bi';
import { useDispatch, useSelector } from 'react-redux';
import { getAllProduct } from '../../action/productActions';
import { useNavigate } from 'react-router-dom';
import { SIGN_IN } from '../../constants/routeConstant';
import { IoMdAdd } from 'react-icons/io';
import { RiCloseCircleLine } from 'react-icons/ri';
import { FaRupeeSign } from 'react-icons/fa';
import { getAllBankAccount } from '../../action/bankAccountAction';
import { GrPowerReset } from 'react-icons/gr';
import { getShopDetails } from '../../action/shopActions';
import { convertNumberToIndianRs } from '../../utils/validateData';
import { productUnits } from '../../utils/dataUtiles';
import { getAllSuppliers } from '../../action/supplierAction';
import { ADD_NEW_PURCHASE_PRODUCTS_DATA_RESET } from '../../constants/purchaseProductConstants';
import { addNewPurchaseProducts } from '../../action/purchaseProductAction';
import { MyMenu } from '../../components';
import { AddEditSupplierModel } from '../../components/modal/AddEditSupplierModel';

export function PurchaseProductEntry() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [open, setOpen] = useState(false);

  const [supplier, setSupplier] = useState({});
  const [gstPercent, setGSTPercent] = useState(0);
  const [billNo, setBillNo] = useState('');

  const [discount, setDiscount] = useState(0);
  const [onlineAmount, setOnlineAmount] = useState(0);
  const [cashAmount, setCashAmount] = useState(0);

  const [paidAmount, setPaidAmount] = useState(0);
  const [modeOfTransaction, setModeOfTransaction] = useState('');
  const [accountNumber, setAccountNo] = useState('');
  const [bankTransactionId, setTransactionId] = useState('');
  const [billDate, setBillDate] = useState(new Date().toISOString().split('T')[0]);

  const [myProducts, setMyProducts] = useState([
    {
      productId: '',
      description: '',
      priceType: '',
      quantity: '',
      price: '',
      total: ''
    }
  ]);

  const getTotal = () => {
    let sum = 0;
    for (let product of myProducts) {
      sum += Number(product.total);
    }
    return sum;
  };

  const bankAccountList = useSelector((state) => state.bankAccountList);
  const {
    error: errorBankAccountList,
    loading: loadingBankAccountList,
    data: bankList
  } = bankAccountList;

  const purchaseProducts = useSelector((state) => state.purchaseProducts);
  const {
    error: errorPurchaseProducts,
    loading: loadingPurchaseProducts,
    data: purchaseProductsData
  } = purchaseProducts;

  const supplierList = useSelector((state) => state.supplierList);
  const { error: errorSupplier, loading: loadingSupplier, suppliers } = supplierList;

  const shopDetail = useSelector((state) => state.shopDetail);
  const { error: errorShopDetail, loading: loadingShopDetail, data: shop } = shopDetail;

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const productList = useSelector((state) => state.productList);
  const { error, loading, products } = productList;

  const validateRow = () => {
    // const index = myProducts.length - 1;
    // for (let key in myProducts[index]) {
    //   if (myProducts[index][key] == '' || myProducts[index][key] === undefined) {
    //     message.error('Please fill all row data first');
    //     return false;
    //   }
    // }
    return true;
  };

  const validate = () => {
    if (!supplier?.supplierId) {
      message.error('first add new supplier');
      return false;
    } else if (!billNo) {
      message.error('Add bill number');
      return false;
    } else if (!billDate) {
      message.error('Add bill date');
      return false;
    } else return true;
  };

  const checkValidNumber = (value) => {
    if (Number(value) === NaN) {
      return false;
    } else if (Number(value) < 0) {
      return false;
    } else return true;
  };

  const handleSaveBill = () => {
    if (validateRow() && validate()) {
      const requestData = {
        billDate,
        billNo,
        modeOfTransaction,
        supplierId: supplier?.supplierId,
        supplierName: supplier?.supplierName,
        total: Number(getTotal()), // without gst
        gstPercent,
        gstAmount: Number(getTotal() * gstPercent * 0.01),
        discountAmount: Number(
          getTotal() + getTotal() * gstPercent * 0.01 - Number(cashAmount) + Number(onlineAmount)
        )?.toFixed(2),
        onlineAmount,
        cashAmount,
        paidAmount: Number(cashAmount) + Number(onlineAmount),

        netTotal: getTotal() + getTotal() * gstPercent * 0.01,
        products: myProducts.map((product) => {
          if (product.productId === 'Other') {
            return {
              ...product,
              productId: ''
            };
          }
          return product;
        }),
        bankTransactionId,
        accountNumber
      };
      // call dispatcher
      dispatch(addNewPurchaseProducts(requestData));
    }
  };

  const handleInputDescription = (value, index, field) => {
    const updatedRows = [...myProducts];
    updatedRows[index][field] = value;
    setMyProducts(updatedRows);
  };

  const handleAddRow = () => {
    if (validateRow()) {
      setMyProducts([
        ...myProducts,
        { description: '', priceType: '', quantity: '', price: '', total: '' }
      ]);
    } else {
    }
  };

  useEffect(() => {
    if (error) {
      message.error(errorPurchaseProducts);
      dispatch({ type: ADD_NEW_PURCHASE_PRODUCTS_DATA_RESET });
    }
    if (purchaseProductsData) {
      message.success('Successfully saved!, print the data');
      dispatch({ type: ADD_NEW_PURCHASE_PRODUCTS_DATA_RESET });
      navigate(-1);
    }
  }, [errorPurchaseProducts, purchaseProductsData]);

  const handleRemoveRow = (index) => {
    const updatedRows = [...myProducts];
    updatedRows.splice(index, 1);
    setMyProducts(updatedRows);
  };

  useEffect(() => {
    if (!userInfo) {
      navigate(SIGN_IN);
    }
    dispatch(getAllProduct());
    dispatch(getAllBankAccount());
    dispatch(getShopDetails());
    dispatch(getAllSuppliers());
  }, []);

  const handleOpenAddSupplierModal = useCallback(() => {
    setOpen(true);
  }, [open]);

  const handleCloseModel = useCallback(() => {
    setOpen(false);
  }, [open]);

  const getSupplier = (supplierId) => {
    const supplier = suppliers?.find((singleCustomer) => singleCustomer.supplierId == supplierId);
    setSupplier(supplier);
  };

  function handleInputChange(event, index, field) {
    const updatedRows = [...myProducts];
    updatedRows[index][field] = event.target.value;
    setMyProducts(updatedRows);
  }
  function handleSelectValue(value, index, field) {
    const updatedRows = [...myProducts];
    updatedRows[index][field] = value;
    setMyProducts(updatedRows);
  }

  function handleSelectDescription(productId, index, field) {
    const updatedRows = [...myProducts];
    if (productId === 'Other') {
      updatedRows[index][field] = '';
      updatedRows[index]['productId'] = productId;
      updatedRows[index]['availableQuantity'] = 0;

      // Use the custom description
    } else {
      const product = products.find((data) => data.productId == productId);
      updatedRows[index][field] = product.description;
      updatedRows[index]['price'] = product.sellingPrice;
      updatedRows[index]['priceType'] = product.priceType;
      updatedRows[index]['productId'] = productId;
      updatedRows[index]['availableQuantity'] = product.quantity;
    }

    setMyProducts(updatedRows);
  }

  function setTotal(index) {
    const updatedRows = [...myProducts];
    updatedRows[index]['total'] = updatedRows[index]['quantity'] * updatedRows[index]['price'];
    setMyProducts(updatedRows);
  }
  const safeProducts = Array.isArray(products) ? products : []; // Ensure products is an array

  return (
    <div className="prime" style={{ background: '#FFFFFF' }}>
      <AddEditSupplierModel
        open={open}
        onCancel={handleCloseModel}
        option={'Add'}
        setCurrentSupplier={() => {}}
      />
      <Box bgColor="#F5F5F5" m="0" height="100vh">
        <Flex
          p="3"
          align="center"
          justifyContent="space-between"
          borderTop="1px"
          borderLeft="1px"
          // border="1px solid green"
          borderColor="#EBEBEB"
          pr="20"
          bgColor="#FFFFFF">
          <Text px="2" fontWeight="600" fontSize="2xl" m="0">
            Purchase Product Entry
          </Text>
          <MyMenu />
        </Flex>
        <Box p="3" px="5" className="h-[80vh] overflow-y-auto">
          <SimpleGrid columns={[1, 2, 2]} gap={{ base: '1', lg: '4' }} mr={{ base: '0', lg: '50' }}>
            <Flex flexDir="column" gap="1" justifyItems="center" width="full">
              <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                Select supplier
              </Text>
              <Select
                size={'large'}
                showSearch
                value={supplier?.supplierId}
                options={suppliers?.map((supplier) => {
                  return {
                    value: supplier.supplierId,
                    label: `${supplier.supplierName}, ${supplier.address}`
                  };
                })}
                filterOption={(input, option) =>
                  (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                }
                style={{ width: 'full', borderRadius: 0 }}
                onChange={(value) => {
                  getSupplier(value);
                }}
                placeholder="Select supplier by name or mobile no"
              />
            </Flex>

            <Flex flexDir="column" gap="1" justifyItems="center" width="full">
              <Flex gap="2" align="center">
                <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                  Bill No.
                </Text>
              </Flex>
              <Input
                size="large"
                placeholder="purchase bill no"
                value={billNo}
                onChange={(e) => {
                  setBillNo(e.target.value);
                }}
              />
            </Flex>
            <Flex flexDir="column" gap="1" justifyItems="center" width="full">
              <Flex gap="2" align="center">
                <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                  Bill Date
                </Text>
              </Flex>
              <Input
                size="large"
                type="date"
                placeholder="GST"
                value={billDate}
                onChange={(e) => {
                  setBillDate(e.target.value);
                }}
              />
            </Flex>
          </SimpleGrid>
          <Flex flexDir={'column'} mt="4">
            <Flex>
              <Text
                width="50px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                S.N.
              </Text>{' '}
              <Text
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px"
                width="350px">
                Product Name
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Per
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Quantity
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Purchasing Price
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Total
              </Text>{' '}
            </Flex>
            {myProducts.map((product, index) => (
              <Flex>
                <Input value={index + 1} style={{ width: '50px', borderRadius: 0 }} />
                {product.productId === 'Other' ? (
                  <Input
                    value={product.description || ''} // Custom input field for 'Other' description
                    onChange={(e) => handleInputDescription(e.target.value, index, 'description')} // Handle custom input change
                    placeholder="Enter custom product description"
                    style={{ width: '350px', borderRadius: 0 }}
                  />
                ) : (
                  <Select
                    size={'large'}
                    showSearch
                    value={product.description}
                    options={[
                      ...safeProducts.map((product) => {
                        return {
                          value: product.productId,
                          label: `${product.productCode} - ${product.description}`
                        };
                      }),
                      { value: 'Other', label: '---Custom Input---' } // 'Other' option
                    ]}
                    filterOption={(input, option) =>
                      (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                    }
                    style={{ width: '350px', borderRadius: 0 }}
                    onChange={(value) => handleSelectDescription(value, index, 'description')}
                    placeholder="Search product name with code"
                  />
                )}
                <Select
                  showSearch
                  size={'large'}
                  value={product.priceType}
                  options={productUnits}
                  filterOption={(input, option) =>
                    (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                  }
                  style={{ width: '150px', borderRadius: 0, fontSize: '16px' }}
                  onChange={(value) => handleSelectValue(value, index, 'priceType')}
                  placeholder="Select product units "
                />
                <Input
                  type="number"
                  value={product.quantity}
                  style={{ width: '150px', borderRadius: 0 }}
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value)) {
                      handleInputChange(e, index, 'quantity');
                      setTotal(index);
                    }
                  }}
                />{' '}
                <Input
                  type="number"
                  value={product.price}
                  style={{ width: '150px', borderRadius: 0 }}
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value)) {
                      handleInputChange(e, index, 'price');
                      setTotal(index);
                    }
                  }}
                />{' '}
                <Input
                  type="number"
                  value={product.total}
                  style={{ width: '150px', borderRadius: 0 }}
                />
                <Flex gap="4" align="center" pl="4" p="1">
                  <Button
                    style={{ height: '32px' }}
                    size="md"
                    bgColor="green"
                    title="add more"
                    onClick={handleAddRow}>
                    <IoMdAdd color="#FFFFFF" />
                  </Button>
                  {myProducts?.length > 1 && index != 0 ? (
                    <Button
                      style={{ height: '32px' }}
                      bgColor="red"
                      title="remove"
                      onClick={() => handleRemoveRow(index)}>
                      <RiCloseCircleLine color="#FFFFFF" size="16px" />
                    </Button>
                  ) : null}
                </Flex>
              </Flex>
            ))}
          </Flex>
          <SimpleGrid columns={[1, 2, 3]} gap="4" pt="4" mr="50px">
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Total
              </Text>
              <Input
                addonBefore={<FaRupeeSign />}
                size="medium"
                placeholder="total"
                value={convertNumberToIndianRs(getTotal() || 0)}
              />
            </Flex>
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Total gst %
              </Text>
              <Input
                size="medium"
                placeholder="gst %"
                type="number"
                value={gstPercent}
                onChange={(e) => {
                  if (checkValidNumber(e.target.value)) {
                    setGSTPercent(e.target.value);
                  }
                }}
              />
            </Flex>
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                GST Amount
              </Text>
              <Input
                addonBefore={<FaRupeeSign />}
                size="medium"
                placeholder="gst %"
                type="number"
                value={getTotal() * gstPercent * 0.01}
              />
            </Flex>
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Net Total
              </Text>
              <Input
                size="medium"
                placeholder="net total"
                value={convertNumberToIndianRs(getTotal() + getTotal() * gstPercent * 0.01)}
                // onChange={(e) => setNetTotal(e.target.value)}
                addonBefore={<FaRupeeSign />}
              />
            </Flex>{' '}
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Mode of Transaction
              </Text>
              <Select
                onChange={(value) => {
                  setModeOfTransaction(value);
                  setOnlineAmount(0);
                  setCashAmount(0);
                }}
                placeholder="Select mode of transaction"
                value={modeOfTransaction}
                options={[
                  {
                    label: 'Cash',
                    value: 'Cash'
                  },
                  {
                    label: 'Online',
                    value: 'Online'
                  },
                  {
                    label: 'Both',
                    value: 'Both'
                  }
                ]}
              />
            </Flex>
            {(modeOfTransaction === 'Online' || modeOfTransaction === 'Both') && (
              <Flex flexDir="column" gap="1" justifyItems="center">
                <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                  Online Paid Amount
                </Text>
                <Input
                  size="medium"
                  placeholder="Paid amount"
                  value={onlineAmount}
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value)) setOnlineAmount(Number(e.target.value));
                  }}
                  addonBefore={<FaRupeeSign />}
                />
              </Flex>
            )}
            {(modeOfTransaction === 'Cash' || modeOfTransaction === 'Both') && (
              <Flex flexDir="column" gap="1" justifyItems="center">
                <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                  Cash Paid Amount
                </Text>
                <Input
                  size="medium"
                  placeholder="Paid amount"
                  value={cashAmount}
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value)) setCashAmount(Number(e.target.value));
                  }}
                  addonBefore={<FaRupeeSign />}
                />
              </Flex>
            )}
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Paid Amount
              </Text>
              <Input
                size="medium"
                placeholder="Paid amount"
                value={convertNumberToIndianRs(Number(cashAmount) + Number(onlineAmount))}
                addonBefore={<FaRupeeSign />}
              />
            </Flex>
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Discount Amount (Rs.)
              </Text>
              <Input
                size="medium"
                placeholder="discount"
                value={convertNumberToIndianRs(
                  getTotal() +
                    getTotal() * gstPercent * 0.01 -
                    (Number(cashAmount) + Number(onlineAmount))
                )}
                onChange={(e) => setDiscount(e.target.value)}
                addonBefore={<FaRupeeSign />}
              />
            </Flex>
            {(modeOfTransaction === 'Online' || modeOfTransaction === 'Both') && (
              <>
                <Flex flexDir="column" gap="1" justifyItems="center">
                  <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                    Select Bank Account
                  </Text>
                  <Select
                    onChange={(value) => setAccountNo(value)}
                    value={accountNumber}
                    options={bankList?.map((account) => {
                      return {
                        value: account.accountNumber,
                        label: `${account.bankName} - ${account.accountHolderName}`
                      };
                    })}
                  />
                </Flex>
                <Flex flexDir="column" gap="1" justifyItems="center">
                  <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                    Transaction Id
                  </Text>
                  <Input
                    size="medium"
                    placeholder="last 8 digits transaction id "
                    value={bankTransactionId}
                    onChange={(e) => setTransactionId(e.target.value)}
                  />
                </Flex>
              </>
            )}
          </SimpleGrid>
          <Flex justifyContent="center" gap="8" pt="8">
            <Button
              type={'submit'}
              bgColor="rgba(40,167,69,1)"
              fontSize="lg"
              px={{ base: '5', lg: '10' }}
              color="#FFFFFF"
              leftIcon={<BiSave />}
              _hover={{
                color: 'rgba(40,167,69,1)',
                bgColor: '#FFFFFF',
                borderColor: 'rgba(40,167,69,1)',
                border: '2px'
              }}
              onClick={handleSaveBill}
              isLoading={loadingPurchaseProducts}
              loadingText="Saving....">
              Save
            </Button>
            <Button
              type={'submit'}
              bgColor="rgba(40,167,69,1)"
              fontSize="lg"
              px={{ base: '5', lg: '10' }}
              color="#FFFFFF"
              leftIcon={<GrPowerReset />}
              _hover={{
                color: 'rgba(40,167,69,1)',
                bgColor: '#FFFFFF',
                borderColor: 'rgba(40,167,69,1)',
                border: '2px'
              }}
              loadingText="Saving....">
              Reset
            </Button>
          </Flex>
        </Box>
      </Box>
    </div>
  );
}

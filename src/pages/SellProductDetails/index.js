import { useSelector } from 'react-redux';
import MyInvoice from '../Invoice/MyInvoice';
import NonGSTInvoice from '../Invoice/NonGSTInvoice';

export const SellProductDetails = () => {
  const sellProductDetails = useSelector((state) => state.sellProductDetails);
  const {
    error: errorSellProductDetails,
    loading: loadingSellProductDetails,
    data
  } = sellProductDetails;

  return (
    <div className="bg-white min-h-screen">
      {/* Header Section */}
      <div className="bg-gray-100 min-h-screen">
        <div className="flex justify-between items-center bg-white p-4 shadow-md border-b border-gray-200">
          <h1 className="text-2xl font-semibold">Sell Product Details</h1>
        </div>

        {/* ✅ Invoice Section - Added Scrollable */}
        <div className="flex justify-center items-center">
          <div className="overflow-y-auto w-full max-h-[calc(100vh-150px)] p-4 border rounded-md shadow-md bg-white">
            <NonGSTInvoice bill={data?.bill} customer={data?.customer} shop={data?.shop} />
          </div>
        </div>
      </div>
    </div>
  );
};

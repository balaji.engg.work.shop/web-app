import { Select, Input, message } from 'antd';
import { useEffect, useState, useCallback } from 'react';
import { BiSave } from 'react-icons/bi';
import { useDispatch, useSelector } from 'react-redux';
import { getAllCustomers } from '../../action/customerAction';
import { FcOk } from 'react-icons/fc';
import { GET_CUSTOMER_RESET } from '../../constants/customerConstants';
import { getAllProduct } from '../../action/productActions';
import { useNavigate } from 'react-router-dom';
import { SIGN_IN } from '../../constants/routeConstant';
import { IoMdAdd } from 'react-icons/io';
import { RiCloseCircleLine } from 'react-icons/ri';
import { FaRupeeSign } from 'react-icons/fa';
import { getAllBankAccount } from '../../action/bankAccountAction';
import { FiPrinter } from 'react-icons/fi';
import { GrPowerReset } from 'react-icons/gr';
import { getShopDetails } from '../../action/shopActions';
import { addNewSellProducts } from '../../action/sellProductAction';
import { ADD_NEW_SELL_PRODUCTS_DATA_RESET } from '../../constants/sellProductConstants';
import { convertNumberToIndianRs } from '../../utils/validateData';
import { productUnits } from '../../utils/dataUtiles';
import { AddEditCustomerModal, MyMenu } from '../../components';
import { Button } from '@chakra-ui/react';

export const NewSellInputForm = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [isGST, setIsGST] = useState(false);
  const [isSameState, setIsSameState] = useState(false);
  const [open, setOpen] = useState(false);
  const [customer, setCustomer] = useState({});
  const [CGSTPercent, setCGSTPercent] = useState(9);
  const [SGSTPercent, setSGSTPercent] = useState(9);
  const [IGSTPercent, setIGSTPercent] = useState(18);

  const [onlineAmount, setOnlineAmount] = useState(0);
  const [cashAmount, setCashAmount] = useState(0);
  const [modeOfTransaction, setModeOfTransaction] = useState('');
  const [accountNumber, setAccountNo] = useState('');
  const [bankTransactionId, setTransactionId] = useState('');

  const [customerName, setCustomerName] = useState('');
  const [gst, setGST] = useState('');
  const [billDate, setBillDate] = useState(new Date().toISOString().split('T')[0]);

  const [myProducts, setMyProducts] = useState([
    {
      productId: '',
      description: '',
      priceType: '',
      availableQuantity: '',
      quantity: '',
      price: '',
      total: ''
    }
  ]);

  const getTotal = () => {
    let sum = 0;
    for (let product of myProducts) {
      sum += Number(product.total);
    }
    return sum;
  };

  const getGSTPresent = () => {
    let gstPresent = 0;
    if (isGST) {
      if (isSameState) {
        gstPresent = Number(SGSTPercent) + Number(CGSTPercent);
      } else {
        gstPresent = Number(IGSTPercent);
      }
    }
    return gstPresent;
  };

  const bankAccountList = useSelector((state) => state.bankAccountList);
  const {
    error: errorBankAccountList,
    loading: loadingBankAccountList,
    data: bankList
  } = bankAccountList;

  const sellProducts = useSelector((state) => state.sellProducts);
  const {
    error: errorSellProducts,
    loading: loadingSellProducts,
    data: sellProductsData
  } = sellProducts;

  const customerList = useSelector((state) => state.customerList);
  const { error: errorCustomer, loading: loadingCustomer, customers } = customerList;

  const shopDetail = useSelector((state) => state.shopDetail);
  const { error: errorShopDetail, loading: loadingShopDetail, data: shop } = shopDetail;

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const productList = useSelector((state) => state.productList);
  const { error, loading, products } = productList;

  const handleOpenAddSupplierModal = useCallback(() => {
    setOpen(true);
  }, [open]);

  const handleCloseModel = useCallback(() => {
    setOpen(false);
  }, [open]);

  // availableQuantity;
  const validateRow = () => {
    // const index = myProducts.length - 1;
    // for (let key in myProducts[index]) {
    //   if (myProducts[index][key] || key != 'availableQuantity') {
    //     message.error(
    //       `Please fill all row data first , ${key} ,${index} ${myProducts[index][key]} `
    //     );
    //     return false;
    //   }
    // }
    return true;
  };

  const validate = () => {
    if (!customer?.customerId) {
      message.error('first add new customer');
      return false;
    } else if (!billDate) {
      message.error('Add bill date');
      return false;
    } else if (isGST && !shop?.gst) {
      message.error('Shop gst not present, please add shop GST in new tab');
      return false;
    } else if (Number(cashAmount) + Number(onlineAmount) == 0) {
      message.error('Please add paid amount in cash amount or online amount');
      return false;
    } else return true;
  };

  const handleSaveBill = () => {
    if (
      validateRow() &&
      validate() &&
      window.confirm(
        'Have you verify your bill? If you continue else verify once and then continue.'
      )
    ) {
      const requestData = {
        billDate,
        customerId: customer?.customerId,
        customerName: customer?.customerName,
        isGST,
        isSameState,
        customerGSTNo: customer?.gst,
        total: Number(getTotal()), // without gst
        SGSTPercent,
        CGSTPercent,
        IGSTPercent,
        gstAmount: Number(getTotal() * getGSTPresent() * 0.01),
        discountAmount: Number(
          getTotal() +
            getTotal() * getGSTPresent() * 0.01 -
            (Number(cashAmount) + Number(onlineAmount))
        )?.toFixed(2),
        onlineAmount,
        cashAmount,
        paidAmount: Number(cashAmount) + Number(onlineAmount),
        modeOfTransaction,
        netTotal: getTotal() + getTotal() * getGSTPresent() * 0.01,
        products: myProducts.map((product) => {
          if (product.productId === 'Other') {
            return {
              ...product,
              productId: ''
            };
          }
          return product;
        }),
        bankTransactionId,
        accountNumber
      };
      // call dispatcher
      dispatch(addNewSellProducts(requestData));
    }
  };

  const handleAddRow = () => {
    if (validateRow()) {
      setMyProducts([
        ...myProducts,
        {
          description: '',
          priceType: '',
          quantity: '',
          price: '',
          total: '',
          availableQuantity: 0
        }
      ]);
    } else {
    }
  };

  useEffect(() => {
    if (error) {
      message.error(errorSellProducts);
      dispatch({ type: ADD_NEW_SELL_PRODUCTS_DATA_RESET });
    }
    if (sellProductsData) {
      message.success('Successfully saved!, print the data');
      dispatch(getAllProduct());
    }
  }, [errorSellProducts, sellProductsData]);

  const handleRemoveRow = (index) => {
    const updatedRows = [...myProducts];
    updatedRows.splice(index, 1);
    setMyProducts(updatedRows);
  };

  useEffect(() => {
    if (!customer) {
      dispatch({ type: GET_CUSTOMER_RESET });
    }
  }, [customer]);

  useEffect(() => {
    if (!userInfo) {
      navigate(SIGN_IN);
    }
    dispatch(getAllProduct());
    dispatch(getAllBankAccount());
    dispatch(getShopDetails());
    dispatch(getAllCustomers());
  }, []);

  const getCustomer = (customerId) => {
    const customer = customers.find((singleCustomer) => singleCustomer.customerId == customerId);
    setCustomer(customer);
  };

  function handleInputChange(event, index, field) {
    const updatedRows = [...myProducts];
    updatedRows[index][field] = event.target.value;
    setMyProducts(updatedRows);
  }
  function handleSelectValue(value, index, field) {
    const updatedRows = [...myProducts];
    updatedRows[index][field] = value;
    setMyProducts(updatedRows);
  }
  const handleInputDescription = (value, index, field) => {
    const updatedRows = [...myProducts];
    updatedRows[index][field] = value;
    setMyProducts(updatedRows);
  };
  function handleSelectDescription(productId, index, field) {
    const updatedRows = [...myProducts];
    if (productId === 'Other') {
      updatedRows[index][field] = '';
      updatedRows[index]['productId'] = productId;
      updatedRows[index]['availableQuantity'] = 0;

      // Use the custom description
    } else {
      const product = products.find((data) => data.productId == productId);
      updatedRows[index][field] = product.description;
      updatedRows[index]['price'] = product.sellingPrice;
      updatedRows[index]['priceType'] = product.priceType;
      updatedRows[index]['productId'] = productId;
      updatedRows[index]['availableQuantity'] = product.quantity;
    }

    setMyProducts(updatedRows);
  }

  function setTotal(index) {
    const updatedRows = [...myProducts];
    updatedRows[index]['total'] = updatedRows[index]['quantity'] * updatedRows[index]['price'];
    setMyProducts(updatedRows);
  }

  const goToInvoice = () => {
    navigate('/invoice', {
      state: {
        bill: sellProductsData?.bill,
        customer: sellProductsData.customer,
        shop: sellProductsData.shop
      }
    });
  };

  const checkValidNumber = (value) => {
    if (Number(value) === NaN) {
      return false;
    } else if (Number(value) < 0) {
      return false;
    } else return true;
  };

  const handleReset = () => {
    window.location.reload();
  };

  const safeProducts = Array.isArray(products) ? products : []; // Ensure products is an array

  let netTotal = getTotal() + getTotal() * getGSTPresent() * 0.01;

  const handleValidateOnlineAmount = (value) => {
    if (Number(value) + Number(cashAmount) > Number(netTotal)) {
      message.error('You have enter wrong online amount or cash Amount');
      return;
    }
    setOnlineAmount(value);
    // setCashAmount(Number(netTotal) - Number(value));
  };

  const handleValidateCashAmount = (value) => {
    if (Number(value) + Number(onlineAmount) > Number(netTotal)) {
      message.error('You have enter wrong online amount or cash Amount');
      return;
    }
    setCashAmount(value);
    // setOnlineAmount(Number(netTotal) - Number(value));
  };
  return (
    <div className="bg-white">
      <AddEditCustomerModal
        open={open}
        onCancel={handleCloseModel}
        option={'Add'}
        setCurrentCustomer={() => {}}
      />
      <div className="bg-gray-100 min-h-screen">
        {/* Header Section */}
        <div className="p-3 flex items-center justify-between border-t border-l border-gray-300 pr-20 bg-white">
          <h1 className="text-2xl font-semibold">Sell Product</h1>
          <MyMenu />
        </div>

        {/* Main Content */}
        <div className="p-3 px-5 h-[80vh] overflow-y-auto">
          <div className="grid grid-cols-2 lg:grid-cols-3 gap-4">
            {/* Bill Type */}
            <div className="flex gap-4">
              <div className="flex flex-col w-full gap-0">
                <p className="text-md font-medium text-black my-2">Bill Type</p>
                <Select
                  style={{ width: '100%', padding: '0', margin: '0' }}
                  placeholder="Select sell type"
                  optionFilterProp="children"
                  onChange={setIsGST}
                  value={isGST}
                  size="large"
                  filterOption={(input, option) => (option?.label ?? '').includes(input)}
                  options={[
                    {
                      label: 'Without GST',
                      value: false
                    },
                    {
                      label: 'GST',
                      value: true
                    }
                  ]}
                />
              </div>

              {/* State Select */}
              {isGST && (
                <div className="flex flex-col w-full ">
                  <p className="text-md font-medium text-black my-2">Select State</p>
                  <Select
                    style={{ width: 'full' }}
                    placeholder="Select option"
                    optionFilterProp="children"
                    onChange={setIsSameState}
                    value={isSameState}
                    size="large"
                    filterOption={(input, option) => (option?.label ?? '').includes(input)}
                    options={[
                      {
                        label: 'Same state',
                        value: true
                      },
                      {
                        label: 'Other state',
                        value: false
                      }
                    ]}
                  />
                </div>
              )}
            </div>

            {/* Select Customer */}
            <div className="flex flex-col w-full ">
              <p className="text-md font-medium text-black my-2">Select Customer</p>
              <Select
                size={'large'}
                showSearch
                value={customer.customerId}
                options={customers?.map((customer) => {
                  return {
                    value: customer.customerId,
                    label: `${customer.mobileNo} (${customer.customerName})`
                  };
                })}
                filterOption={(input, option) =>
                  (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                }
                style={{ width: 'full', borderRadius: 0 }}
                onChange={(value) => {
                  getCustomer(value);
                }}
                placeholder="Select customer by name or mobile no"
              />
            </div>

            {/* Customer Name */}
            <div className="flex flex-col w-full ">
              <div className="flex gap-2 items-center">
                <p className="text-md font-medium text-black my-2">Customer Name</p>
                {customer?.customerName && <FcOk />}
              </div>
              <input
                className="border p-2 rounded"
                placeholder="Customer Name"
                value={customer?.customerName || customerName}
              />
            </div>

            {/* Customer GST */}
            <div className="flex flex-col w-full ">
              <div className="flex gap-2 items-center">
                <p className="text-md font-medium text-black my-2">Customer GST</p>
                {customer?.customerName && <FcOk />}
              </div>
              <input
                className="border p-2 rounded"
                placeholder="GST"
                value={customer?.gst || gst}
              />
            </div>

            {/* Bill Date */}
            <div className="flex flex-col w-full ">
              <p className="text-md font-medium text-black my-2">Bill Date</p>
              <input
                type="date"
                className="border p-2 rounded"
                value={billDate}
                onChange={(e) => setBillDate(e.target.value)}
              />
            </div>
          </div>

          {/* Product Table */}
          <div className="flex flex-col space-y-2 mt-4">
            <div className="grid grid-cols-12 gap-2 border-b font-semibold bg-[#129BEF80]">
              <p className="text-center p-2 col-span-1">S.N.</p>
              <p className="text-center p-2 col-span-3">Product Name</p>
              <p className="text-center p-2 col-span-1">Stock</p>
              <p className="text-center p-2 col-span-1">Price</p>
              <p className="text-center p-2 col-span-2">Unit</p>
              <p className="text-center p-2 col-span-1">Quantity</p>
              <p className="text-center p-2 col-span-1">Total</p>
              <p className="text-center p-2 col-span-1">Action</p>
            </div>

            {myProducts.map((product, index) => (
              <div className="grid grid-cols-12 border-b" key={index}>
                <Input value={index + 1} className="col-span-1" />
                {product.productId === 'Other' ? (
                  <Input
                    className="col-span-3"
                    value={product.description || ''} // Custom input field for 'Other' description
                    onChange={(e) => handleInputDescription(e.target.value, index, 'description')} // Handle custom input change
                    placeholder="Enter custom product description"
                  />
                ) : (
                  <Select
                    size={'large'}
                    showSearch
                    value={product.description}
                    options={[
                      ...safeProducts.map((product) => {
                        return {
                          value: product.productId,
                          label: `${product.productCode} - ${product.description}`
                        };
                      }),
                      { value: 'Other', label: '---Custom Input---' } // 'Other' option
                    ]}
                    filterOption={(input, option) =>
                      (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                    }
                    className="col-span-3"
                    onChange={(value) => handleSelectDescription(value, index, 'description')}
                    placeholder="Search product name with code"
                  />
                )}
                <Input
                  type="number"
                  value={product.availableQuantity - product.quantity}
                  className={
                    product.availableQuantity - product.quantity <= 0
                      ? 'text-red-600 col-span-1'
                      : 'text-green-500 col-span-1'
                  }
                />
                <Input
                  type="number"
                  value={product.price}
                  className="col-span-1"
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value)) {
                      handleInputChange(e, index, 'price');
                      setTotal(index);
                    }
                  }}
                />
                <Select
                  showSearch
                  size={'large'}
                  value={product.priceType}
                  options={productUnits}
                  filterOption={(input, option) =>
                    (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                  }
                  className="col-span-2"
                  onChange={(value) => handleSelectValue(value, index, 'priceType')}
                  placeholder="Select product units "
                />
                <Input
                  type="number"
                  value={product.quantity}
                  className="col-span-1"
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value)) {
                      handleInputChange(e, index, 'quantity');
                      setTotal(index);
                    }
                  }}
                />{' '}
                <Input value={convertNumberToIndianRs(product.total)} className="col-span-1" />
                <div className="flex items-center justify-center gap-4 col-span-1">
                  <button
                    className="bg-green-500 text-white flex items-center justify-center h-8 px-3 rounded hover:bg-green-600"
                    title="add more"
                    onClick={handleAddRow}>
                    <IoMdAdd size="16px" />
                  </button>
                  <button
                    className="bg-red-500 text-white flex items-center justify-center h-8 px-3 rounded hover:bg-red-600"
                    title="remove"
                    onClick={() => {
                      if (myProducts?.length > 1) {
                        handleRemoveRow(index);
                      }
                    }}>
                    <RiCloseCircleLine size="16px" />
                  </button>
                </div>
              </div>
            ))}
          </div>

          {/* Payment Details */}
          <div className="grid grid-cols-3 gap-4 mt-6">
            <div className="flex flex-col">
              <p className="text-md font-medium text-black my-2">Total</p>
              <input
                className="border p-2 rounded"
                value={convertNumberToIndianRs(getTotal() || 0)}
              />
            </div>

            <div className="flex flex-col ">
              <p className="text-md font-medium text-black my-2">Net Total</p>
              <input className="border p-2 rounded" value={convertNumberToIndianRs(netTotal)} />
            </div>

            <div className="flex flex-col ">
              <p className="text-md font-medium text-black my-2">Mode of Transaction</p>
              <Select
                onChange={(value) => {
                  setModeOfTransaction(value);
                  setOnlineAmount(0);
                  setCashAmount(0);
                }}
                size="large"
                placeholder="Select mode of transaction"
                value={modeOfTransaction}
                options={[
                  {
                    label: 'Cash',
                    value: 'Cash'
                  },
                  {
                    label: 'Online',
                    value: 'Online'
                  },
                  {
                    label: 'Both',
                    value: 'Both'
                  }
                ]}
              />
            </div>
            {(modeOfTransaction === 'Online' || modeOfTransaction === 'Both') && (
              <div className="flex flex-col ">
                <p className="text-md font-medium text-black my-2">Online Paid Amount</p>
                <input
                  className="border p-2 rounded"
                  placeholder="Paid amount"
                  value={onlineAmount}
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value))
                      handleValidateOnlineAmount(e.target.value);
                  }}
                />
              </div>
            )}
            {(modeOfTransaction === 'Cash' || modeOfTransaction === 'Both') && (
              <div className="flex flex-col ">
                <p className="text-md font-medium text-black my-2">Cash Paid Amount</p>
                <input
                  className="border p-2 rounded"
                  placeholder="Paid amount"
                  value={cashAmount}
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value)) handleValidateCashAmount(e.target.value);
                  }}
                />
              </div>
            )}

            <div className="flex flex-col ">
              <p className="text-md font-medium text-black my-2">Paid Amount</p>
              <input
                className="border p-2 rounded"
                placeholder="Paid amount"
                value={convertNumberToIndianRs(Number(cashAmount) + Number(onlineAmount))}
              />
            </div>
            <div className="flex flex-col ">
              <p className="text-md font-medium text-black my-2">Discount Amount</p>
              <input
                className="border p-2 rounded"
                placeholder="Discount amount"
                value={convertNumberToIndianRs(
                  getTotal() +
                    getTotal() * getGSTPresent() * 0.01 -
                    (Number(cashAmount) + Number(onlineAmount))
                )}
              />
            </div>
            {(modeOfTransaction === 'Online' || modeOfTransaction === 'Both') && (
              <>
                <div className="flex flex-col ">
                  <p className="text-md font-medium text-black my-2">Select Bank Account</p>
                  <Select
                    size="large"
                    placeholder="Select back account"
                    onChange={(value) => setAccountNo(value)}
                    value={accountNumber}
                    options={bankList?.map((account) => {
                      return {
                        value: account.accountNumber,
                        label: `${account.bankName} - ${account.accountHolderName}`
                      };
                    })}
                  />
                </div>
                <div className="flex flex-col ">
                  <p className="text-md font-medium text-black my-2">Transaction Id</p>
                  <input
                    className="border p-2 rounded"
                    placeholder="Discount amount"
                    value={bankTransactionId}
                    onChange={(e) => setTransactionId(e.target.value)}
                  />
                </div>
              </>
            )}
          </div>

          {/* Action Buttons */}
          <div className="flex justify-center gap-4 mt-8">
            <Button
              type={'submit'}
              bgColor="rgba(40,167,69,1)"
              fontSize="lg"
              px={{ base: '5', lg: '10' }}
              color="#FFFFFF"
              leftIcon={<BiSave />}
              _hover={{
                color: 'rgba(40,167,69,1)',
                bgColor: '#FFFFFF',
                borderColor: 'rgba(40,167,69,1)',
                border: '2px'
              }}
              onClick={handleSaveBill}
              isLoading={loadingSellProducts}
              loadingText="Saving....">
              Save
            </Button>
            {sellProductsData && (
              <Button
                type={'submit'}
                bgColor="rgba(40,167,69,1)"
                fontSize="lg"
                px={{ base: '5', lg: '10' }}
                color="#FFFFFF"
                leftIcon={<FiPrinter />}
                _hover={{
                  color: 'rgba(40,167,69,1)',
                  bgColor: '#FFFFFF',
                  borderColor: 'rgba(40,167,69,1)',
                  border: '2px'
                }}
                onClick={goToInvoice}>
                Print
              </Button>
            )}
            <Button
              type={'submit'}
              bgColor="rgba(40,167,69,1)"
              fontSize="lg"
              px={{ base: '5', lg: '10' }}
              color="#FFFFFF"
              leftIcon={<GrPowerReset />}
              _hover={{
                color: 'rgba(40,167,69,1)',
                bgColor: '#FFFFFF',
                borderColor: 'rgba(40,167,69,1)',
                border: '2px'
              }}
              onClick={handleReset}>
              Reset
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewSellInputForm;

import { Flex, Text, Button, SimpleGrid } from '@chakra-ui/react';
import { Select, Input, message } from 'antd';
import { useEffect, useState, useCallback } from 'react';
import { BiSave } from 'react-icons/bi';
import { useDispatch, useSelector } from 'react-redux';
import { FcOk } from 'react-icons/fc';
import { getAllProduct } from '../../action/productActions';
import { useNavigate } from 'react-router-dom';
import { SELL_PRODUCT_DETAILS, SIGN_IN } from '../../constants/routeConstant';
import { RiCloseCircleLine } from 'react-icons/ri';
import { FaRupeeSign } from 'react-icons/fa';
import { getAllBankAccount } from '../../action/bankAccountAction';
import { FiPrinter } from 'react-icons/fi';
import { GrPowerReset } from 'react-icons/gr';
import { getShopDetails } from '../../action/shopActions';
import { getSellProductDetails } from '../../action/sellProductAction';
import { GET_SELL_PRODUCTS_DETAIL_RESET } from '../../constants/sellProductConstants';
import { convertNumberToIndianRs } from '../../utils/validateData';
import { SearchInputField } from '../../components';
import { createNewSellReturnAction } from '../../action/sellReturnAction';
import { CREATE_NEW_SELL_RETURN_RESET } from '../../constants/sellReturnConstants';

export const SellReturn = () => {
  const [searchText, setSearchText] = useState('');
  const dispatch = useDispatch();

  const handleSearch = useCallback(() => {
    dispatch(getSellProductDetails(searchText?.trim()));
  }, [searchText]);

  const sellProductDetails = useSelector((state) => state.sellProductDetails);
  const {
    error: errorSellProductDetails,
    loading: loadingSellProductDetails,
    data
  } = sellProductDetails;

  const navigate = useNavigate();
  const [isGST, setIsGST] = useState(false);
  const [isSameState, setIsSameState] = useState(false);
  const [CGSTPercent, setCGSTPercent] = useState(9);
  const [SGSTPercent, setSGSTPercent] = useState(9);
  const [IGSTPercent, setIGSTPercent] = useState(18);

  const [onlineAmount, setOnlineAmount] = useState();
  const [cashAmount, setCashAmount] = useState();
  const [modeOfTransaction, setModeOfTransaction] = useState('');
  const [accountNumber, setAccountNo] = useState('');
  const [bankTransactionId, setTransactionId] = useState('');

  const [customerName, setCustomerName] = useState('');
  const [gst, setGST] = useState('');
  const [billDate, setBillDate] = useState(new Date().toISOString().split('T')[0]);

  const [myProducts, setMyProducts] = useState([
    {
      sn: 1,
      productId: '',
      description: '',
      priceType: '',
      quantity: '',
      price: '',
      total: ''
    }
  ]);

  const getTotal = () => {
    let sum = 0;
    for (let product of myProducts) {
      sum += Number(product.total);
    }
    return sum;
  };

  const getGSTPresent = () => {
    let gstPresent = 0;
    if (isGST) {
      if (isSameState) {
        gstPresent = Number(SGSTPercent) + Number(CGSTPercent);
      } else {
        gstPresent = Number(IGSTPercent);
      }
    }
    return gstPresent;
  };

  const bankAccountList = useSelector((state) => state.bankAccountList);
  const {
    error: errorBankAccountList,
    loading: loadingBankAccountList,
    data: bankList
  } = bankAccountList;

  const createNewSellReturn = useSelector((state) => state.createNewSellReturn);
  const {
    error: errorSellProducts,
    loading: loadingSellProducts,
    data: sellReturnData
  } = createNewSellReturn;

  const shopDetail = useSelector((state) => state.shopDetail);
  const { error: errorShopDetail, loading: loadingShopDetail, data: shop } = shopDetail;

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const validateRow = () => {
    const index = myProducts.length - 1;
    for (let key in myProducts[index]) {
      if (myProducts[index][key] == '' || myProducts[index][key] === undefined) {
        message.error('Please fill all row data first');
        return false;
      }
    }
    return true;
  };

  const validate = () => {
    if (!billDate) {
      message.error('Add bill date');
      return false;
    } else if (Number(cashAmount) + Number(onlineAmount) == 0) {
      message.error('Please add paid amount in cash amount or online amount');
      return false;
    } else return true;
  };

  const handleSaveBill = () => {
    const { bill, customer } = data;
    if (
      validateRow() &&
      validate() &&
      window.confirm(
        'Have you verify your bill? If you continue else verify once and then continue.'
      )
    ) {
      const requestData = {
        billDate,
        oldBillNo: bill.billNo,
        customerId: customer?.customerId,
        customerName: customer?.customerName,
        isGST,
        isSameState,
        customerGSTNo: customer?.gst,
        total: Number(getTotal()), // without gst
        SGSTPercent,
        CGSTPercent,
        IGSTPercent,
        gstAmount: Number(getTotal() * getGSTPresent() * 0.01),
        discountAmount: Number(
          getTotal() +
            getTotal() * getGSTPresent() * 0.01 -
            (Number(cashAmount) + Number(onlineAmount))
        )?.toFixed(2),
        onlineAmount,
        cashAmount,
        paidAmount: Number(cashAmount) + Number(onlineAmount),
        modeOfTransaction,
        netTotal: getTotal() + getTotal() * getGSTPresent() * 0.01,
        products: myProducts,
        bankTransactionId,
        accountNumber
      };
      // call dispatcher
      dispatch(createNewSellReturnAction(requestData));
    }
  };

  useEffect(() => {
    if (errorSellProducts) {
      message.error(errorSellProducts);
      dispatch({ type: CREATE_NEW_SELL_RETURN_RESET });
    }
    if (sellReturnData) {
      message.success('Successfully saved!, print the data');
      dispatch(getAllProduct());
    }
  }, [errorSellProducts, sellReturnData]);

  const handleRemoveRow = (index) => {
    const updatedRows = [...myProducts];
    updatedRows.splice(index, 1);
    setMyProducts(updatedRows);
  };

  useEffect(() => {
    if (!userInfo) {
      navigate(SIGN_IN);
    }
    dispatch(getAllBankAccount());
    dispatch(getShopDetails());
  }, []);

  function handleInputChange(event, index, field) {
    const updatedRows = [...myProducts];
    updatedRows[index][field] = event.target.value;
    setMyProducts(updatedRows);
  }
  function handleSelectValue(value, index, field) {
    const updatedRows = [...myProducts];
    updatedRows[index][field] = value;
    setMyProducts(updatedRows);
  }

  function setTotal(index) {
    const updatedRows = [...myProducts];
    updatedRows[index]['total'] = updatedRows[index]['quantity'] * updatedRows[index]['price'];
    setMyProducts(updatedRows);
  }

  const goToInvoice = () => {
    navigate('/invoice', {
      state: {
        bill: sellReturnData?.bill,
        customer: sellReturnData.customer,
        shop: sellReturnData.shop
      }
    });
  };

  const checkValidNumber = (value) => {
    if (Number(value) === NaN) {
      return false;
    } else if (Number(value) < 0) {
      return false;
    } else return true;
  };

  const handleReset = () => {
    dispatch({ type: GET_SELL_PRODUCTS_DETAIL_RESET });
    setSearchText('');
  };

  useEffect(() => {
    if (errorSellProductDetails) {
      message.error(errorSellProductDetails);
      dispatch({ type: GET_SELL_PRODUCTS_DETAIL_RESET });
    }
    if (data && data?.bill) {
      const { bill, customer } = data;
      setIsGST(bill?.isGST);
      setIsSameState(bill?.isSameState);
      setCustomerName(customer?.customerName);
      setMyProducts(
        bill?.products?.map((data, index) => {
          return {
            sn: index + 1,
            productId: data?.productId,
            description: data?.description,
            priceType: data?.priceType,
            quantity: data?.quantity,
            price: data?.price,
            total: data?.total
          };
        })
      );
    }
  }, [data, errorSellProductDetails]);

  return (
    <div className="m-0 bg-[#F5F5F5] h-[90vh] w-full">
      <div className="m-0  p-4  bg-[#FFFFFF] flex justify-between">
        <h1 className="font-bold text-[24px] m-0">Sell Return</h1>
        <div className="w-96 flex gap-4">
          <SearchInputField
            value={searchText}
            onChange={setSearchText}
            placeholder="Search Old Bill"
          />
          <button
            onClick={handleSearch}
            disabled={loadingSellProductDetails}
            className="border border-1 hover:border-sky-500 px-4 rounded-md text-sky-500">
            {loadingSellProductDetails ? 'Loading...' : 'Search'}
          </button>
        </div>
      </div>
      <div className="p-2 pl-4">
        <div className="h-[80vh] overflow-y-auto p-4 ">
          <SimpleGrid
            columns={[1, 2, 2]}
            gap={{ base: '1', lg: '4' }}
            mr={{ base: '0px', lg: '50px' }}>
            <Flex gap="4">
              <Flex flexDir="column" gap="1" width="full">
                <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                  Bill Type
                </Text>
                <Select
                  style={{ width: 'full' }}
                  placeholder="Select sell type"
                  optionFilterProp="children"
                  onChange={setIsGST}
                  value={isGST}
                  size="large"
                  filterOption={(input, option) => (option?.label ?? '').includes(input)}
                  options={[
                    {
                      label: 'Without GST',
                      value: false
                    },
                    {
                      label: 'GST',
                      value: true
                    }
                  ]}
                />
              </Flex>
              {isGST && (
                <Flex flexDir="column" gap="1" width="full">
                  <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                    Select State
                  </Text>
                  <Select
                    style={{ width: 'full' }}
                    placeholder="Select option"
                    optionFilterProp="children"
                    onChange={setIsSameState}
                    value={isSameState}
                    size="large"
                    filterOption={(input, option) => (option?.label ?? '').includes(input)}
                    options={[
                      {
                        label: 'Same state',
                        value: true
                      },
                      {
                        label: 'Other state',
                        value: false
                      }
                    ]}
                  />
                </Flex>
              )}
            </Flex>

            <Flex flexDir="column" gap="1" justifyItems="center" width="full">
              <Flex gap="2" align="center">
                <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                  Customer Name
                </Text>
              </Flex>
              <Input
                size="large"
                placeholder="Customer Name"
                disabled
                value={data?.customer?.customerName}
              />
            </Flex>
            <Flex flexDir="column" gap="1" justifyItems="center" width="full">
              <Flex gap="2" align="center">
                <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                  Customer GST
                </Text>
                {data?.customer?.gst && <FcOk />}
              </Flex>
              <Input size="large" disabled placeholder="GST" value={data?.customer?.gst} />
            </Flex>
            <Flex flexDir="column" gap="1" justifyItems="center" width="full">
              <Flex gap="2" align="center">
                <Text fontSize="md" color="#000000" fontWeight="500" p="0" m="0">
                  Bill Date
                </Text>
              </Flex>
              <Input
                size="large"
                type="date"
                placeholder="GST"
                value={billDate}
                onChange={(e) => {
                  setBillDate(e.target.value);
                }}
              />
            </Flex>
          </SimpleGrid>
          <Flex flexDir={'column'} mt="4">
            <Flex>
              <Text
                width="50px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                S.N.
              </Text>{' '}
              <Text
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px"
                width="350px">
                Product Name
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Unit
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Quantity
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Price
              </Text>{' '}
              <Text
                width="150px"
                fontSize="md"
                color="#000000"
                fontWeight="500"
                border="1px"
                px="8px"
                py="2px">
                Total
              </Text>{' '}
            </Flex>
            {myProducts.map((product, index) => (
              <Flex>
                <Input value={index + 1} style={{ width: '50px', borderRadius: 0 }} />
                <Input value={product.description} style={{ width: '350px', borderRadius: 0 }} />
                <Input
                  value={product.priceType}
                  style={{ width: '150px', borderRadius: 0, fontSize: '16px' }}
                />
                <Input
                  type="number"
                  value={product.quantity}
                  style={{ width: '150px', borderRadius: 0 }}
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value)) {
                      handleInputChange(e, index, 'quantity');
                      setTotal(index);
                    }
                  }}
                />
                <Input
                  type="number"
                  value={product.price}
                  style={{ width: '150px', borderRadius: 0 }}
                />
                <Input
                  value={convertNumberToIndianRs(product.total)}
                  style={{ width: '150px', borderRadius: 0 }}
                />
                <Flex gap="4" align="center" pl="4" p="1">
                  {myProducts?.length > 1 && index != 0 ? (
                    <Button
                      style={{ height: '32px' }}
                      bgColor="red"
                      title="remove"
                      onClick={() => handleRemoveRow(index)}>
                      <RiCloseCircleLine color="#FFFFFF" size="16px" />
                    </Button>
                  ) : null}
                </Flex>
              </Flex>
            ))}
          </Flex>
          <SimpleGrid columns={[1, 2, 3]} gap="4" pt="4" mr="50px">
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Total
              </Text>
              <Input
                addonBefore={<FaRupeeSign />}
                size="medium"
                placeholder="total"
                value={convertNumberToIndianRs(getTotal() || 0)}
              />
            </Flex>
            {isGST ? (
              isSameState ? (
                <>
                  <Flex flexDir="column" gap="1" justifyItems="center">
                    <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                      SGST %
                    </Text>
                    <Input
                      size="medium"
                      placeholder="gst %"
                      type="number"
                      value={CGSTPercent}
                      onChange={(e) => {
                        setSGSTPercent(e.target.value);
                        setCGSTPercent(e.target.value);
                      }}
                    />
                  </Flex>
                  <Flex flexDir="column" gap="1" justifyItems="center">
                    <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                      CGST %
                    </Text>
                    <Input
                      size="medium"
                      placeholder="gst %"
                      type="number"
                      value={CGSTPercent}
                      onChange={(e) => {
                        setSGSTPercent(e.target.value);
                        setCGSTPercent(e.target.value);
                      }}
                    />
                  </Flex>
                </>
              ) : (
                <Flex flexDir="column" gap="1" justifyItems="center">
                  <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                    IGST %
                  </Text>
                  <Input
                    size="medium"
                    placeholder="gst %"
                    type="number"
                    value={IGSTPercent}
                    onChange={(e) => setIGSTPercent(e.target.value)}
                  />
                </Flex>
              )
            ) : null}
            {isGST && (
              <Flex flexDir="column" gap="1" justifyItems="center">
                <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                  GST Amount
                </Text>
                <Input
                  addonBefore={<FaRupeeSign />}
                  size="medium"
                  placeholder="gst %"
                  type="number"
                  value={getTotal() * getGSTPresent() * 0.01}
                />
              </Flex>
            )}
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Net Total
              </Text>
              <Input
                size="medium"
                placeholder="net total"
                value={convertNumberToIndianRs(getTotal() + getTotal() * getGSTPresent() * 0.01)}
                // onChange={(e) => setNetTotal(e.target.value)}
                addonBefore={<FaRupeeSign />}
              />
            </Flex>
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Mode of Transaction
              </Text>
              <Select
                onChange={(value) => {
                  setModeOfTransaction(value);
                  setOnlineAmount(0);
                  setCashAmount(0);
                }}
                placeholder="Select mode of transaction"
                value={modeOfTransaction}
                options={[
                  {
                    label: 'Cash',
                    value: 'Cash'
                  },
                  {
                    label: 'Online',
                    value: 'Online'
                  },
                  {
                    label: 'Both',
                    value: 'Both'
                  }
                ]}
              />
            </Flex>
            {(modeOfTransaction === 'Online' || modeOfTransaction === 'Both') && (
              <Flex flexDir="column" gap="1" justifyItems="center">
                <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                  Online Paid Amount
                </Text>
                <Input
                  type="number"
                  size="medium"
                  placeholder="Paid amount"
                  value={onlineAmount}
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value)) setOnlineAmount(e.target.value);
                  }}
                  addonBefore={<FaRupeeSign />}
                />
              </Flex>
            )}
            {(modeOfTransaction === 'Cash' || modeOfTransaction === 'Both') && (
              <Flex flexDir="column" gap="1" justifyItems="center">
                <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                  Cash Paid Amount
                </Text>
                <Input
                  type="number"
                  size="medium"
                  placeholder="Paid amount"
                  value={cashAmount}
                  onChange={(e) => {
                    if (checkValidNumber(e.target.value)) setCashAmount(e.target.value);
                  }}
                  addonBefore={<FaRupeeSign />}
                />
              </Flex>
            )}
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Paid Amount
              </Text>
              <Input
                size="medium"
                placeholder="Paid amount"
                value={convertNumberToIndianRs(Number(cashAmount) + Number(onlineAmount))}
                addonBefore={<FaRupeeSign />}
              />
            </Flex>
            <Flex flexDir="column" gap="1" justifyItems="center">
              <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                Discount Amount (Rs.)
              </Text>
              <Input
                size="medium"
                placeholder="discount"
                value={convertNumberToIndianRs(
                  getTotal() +
                    getTotal() * getGSTPresent() * 0.01 -
                    (Number(cashAmount) + Number(onlineAmount))
                )}
                addonBefore={<FaRupeeSign />}
              />
            </Flex>
            {(modeOfTransaction === 'Online' || modeOfTransaction === 'Both') && (
              <>
                <Flex flexDir="column" gap="1" justifyItems="center">
                  <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                    Select Bank Account
                  </Text>
                  <Select
                    onChange={(value) => setAccountNo(value)}
                    value={accountNumber}
                    options={bankList?.map((account) => {
                      return {
                        value: account.accountNumber,
                        label: `${account.bankName} - ${account.accountHolderName}`
                      };
                    })}
                  />
                </Flex>
                <Flex flexDir="column" gap="1" justifyItems="center">
                  <Text fontSize="sm" color="#000000" fontWeight="500" p="0" m="0">
                    Transaction Id
                  </Text>
                  <Input
                    size="medium"
                    placeholder="last 8 digits transaction id "
                    value={bankTransactionId}
                    onChange={(e) => setTransactionId(e.target.value)}
                  />
                </Flex>
              </>
            )}
          </SimpleGrid>
          <Flex justifyContent="center" gap="8" pt="8">
            <Button
              type={'submit'}
              bgColor="rgba(40,167,69,1)"
              fontSize="lg"
              px={{ base: '5', lg: '10' }}
              color="#FFFFFF"
              leftIcon={<BiSave />}
              _hover={{
                color: 'rgba(40,167,69,1)',
                bgColor: '#FFFFFF',
                borderColor: 'rgba(40,167,69,1)',
                border: '2px'
              }}
              onClick={handleSaveBill}
              isLoading={loadingSellProducts}
              loadingText="Saving....">
              Save
            </Button>
            {sellReturnData && (
              <Button
                type={'submit'}
                bgColor="rgba(40,167,69,1)"
                fontSize="lg"
                px={{ base: '5', lg: '10' }}
                color="#FFFFFF"
                leftIcon={<FiPrinter />}
                _hover={{
                  color: 'rgba(40,167,69,1)',
                  bgColor: '#FFFFFF',
                  borderColor: 'rgba(40,167,69,1)',
                  border: '2px'
                }}
                onClick={goToInvoice}>
                Print
              </Button>
            )}
            <Button
              type={'submit'}
              bgColor="rgba(40,167,69,1)"
              fontSize="lg"
              px={{ base: '5', lg: '10' }}
              color="#FFFFFF"
              leftIcon={<GrPowerReset />}
              _hover={{
                color: 'rgba(40,167,69,1)',
                bgColor: '#FFFFFF',
                borderColor: 'rgba(40,167,69,1)',
                border: '2px'
              }}
              onClick={handleReset}>
              Reset
            </Button>
          </Flex>
        </div>
      </div>
    </div>
  );
};

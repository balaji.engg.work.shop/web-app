import React from 'react';

import { useSelector } from 'react-redux';
import MyInVoice from '../Invoice/MyInvoice';
import { Box, Flex, Text } from '@chakra-ui/react';

export const SellReturnDetails = () => {
  const sellReturnDetails = useSelector((state) => state.sellReturnDetails);
  const {
    error: errorSellProductDetails,
    loading: loadingSellProductDetails,
    data
  } = sellReturnDetails;

  return (
    <div className="prime" style={{ background: '#FFFFFF' }}>
      <Box bgColor="#F5F5F5" m="0" height="100vh">
        <Flex
          p="3"
          align="center"
          justifyContent="space-between"
          borderTop="1px"
          borderLeft="1px"
          // border="1px solid green"
          borderColor="#EBEBEB"
          pr="20"
          bgColor="#FFFFFF">
          <Text px="2" fontWeight="600" fontSize="2xl" m="0">
            Sell Return Details
          </Text>
        </Flex>
        <Box display="flex" align="center" justifyContent="center">
          <MyInVoice bill={data?.bill} customer={data?.customer} shop={data?.shop} />
        </Box>
      </Box>
    </div>
  );
};

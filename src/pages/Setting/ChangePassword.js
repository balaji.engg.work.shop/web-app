import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { MdLockOutline } from 'react-icons/md';
import { updateUserPassword } from '../../action/userActions';
import { USER_UPDATE_PASSWORD_RESET } from '../../constants/userConstants';
import { message } from 'antd';

export function ChangePassword() {
  const dispatch = useDispatch();
  const [oldPassword, setOldPassword] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const {
    loading,
    error: errorUpdateProfile,
    success
  } = useSelector((state) => state.userUpdatePassword);

  useEffect(() => {
    if (success) {
      dispatch({ type: USER_UPDATE_PASSWORD_RESET });
      message.success('New Password Added Successfully!');
      setOldPassword('');
      setPassword('');
      setConfirmPassword('');
    }
    if (errorUpdateProfile) {
      message.error(errorUpdateProfile);
      dispatch({ type: USER_UPDATE_PASSWORD_RESET });
    }
  }, [dispatch, errorUpdateProfile, success]);

  const handleSave = () => {
    if (!oldPassword) return message.error('Please Enter Your Old Password!');
    if (password !== confirmPassword) return message.error('Passwords do not match!');
    if (password.length < 8) return message.error('Password must be at least 8 characters long.');
    dispatch(updateUserPassword({ oldPassword, newPassword: password }));
  };

  return (
    <div className="flex items-center justify-center h-[70vh] bg-gray-100">
      <div className="p-6 max-w-lg w-full bg-white rounded shadow-md">
        <h2 className="text-2xl font-semibold text-gray-800 mb-6 text-center">Change Password</h2>
        <div className="space-y-4">
          <div>
            <label className="block text-gray-700">Current Password</label>
            <div className="flex items-center border rounded p-2">
              <MdLockOutline className="mr-2 text-gray-500" />
              <input
                type="password"
                placeholder="Old Password"
                value={oldPassword}
                onChange={(e) => setOldPassword(e.target.value)}
                className="w-full outline-none"
                required
              />
            </div>
          </div>
          <div>
            <label className="block text-gray-700">New Password</label>
            <div className="flex items-center border rounded p-2">
              <MdLockOutline className="mr-2 text-gray-500" />
              <input
                type="password"
                placeholder="New Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                className="w-full outline-none"
                required
              />
            </div>
          </div>
          <div>
            <label className="block text-gray-700">Confirm New Password</label>
            <div className="flex items-center border rounded p-2">
              <MdLockOutline className="mr-2 text-gray-500" />
              <input
                type="password"
                placeholder="Confirm New Password"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
                className="w-full outline-none"
                required
              />
            </div>
          </div>
          <button
            onClick={handleSave}
            className="w-full bg-blue-500 text-white p-2 rounded hover:bg-blue-600 flex justify-center items-center"
            disabled={loading}>
            {loading ? 'Saving...' : 'Change Password'}
          </button>
        </div>
      </div>
    </div>
  );
}

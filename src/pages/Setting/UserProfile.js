import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateUserProfile } from '../../action/userActions';
import { UPDATE_USER_PROFILE_RESET } from '../../constants/userConstants';
import { isValidEmail, isValidMobileNo } from '../../utils/validateData';
import ProfilePicture from '../../components/molecule/ProfilePicture';
import { BiSave } from 'react-icons/bi';
import { IoMdCall } from 'react-icons/io';
import { MdOutlineMail } from 'react-icons/md';
import { getSignedUrl, uploadFileToSignedUrl } from '../../utils/s3Utils';

export const UserProfile = (props) => {
  const dispatch = useDispatch();
  const [mediaFiles, setMediaFiles] = useState({});

  const profileUpdate = useSelector((state) => state.profileUpdate);
  const { error, loading, success } = profileUpdate;
  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const [formData, setFormData] = useState({
    email: userInfo?.email || '',
    fullName: userInfo?.fullName || '',
    mobileNo: userInfo?.mobileNo || '',
    gender: userInfo?.gender || '',
    aadhaarNo: userInfo?.aadhaarNo || ''
  });

  const getAllImagesURL = async () => {
    let data = mediaFiles;
    try {
      if (data.awsURL === '') {
        const content_type = data.file.type.split('/')[1];
        const response = await getSignedUrl({ key: 'profileImage', content_type });
        const { fileLink, signedUrl } = response;
        await uploadFileToSignedUrl(signedUrl, data.file, content_type, () => {
          console.log('File uplaoding on AWS');
        });
        data.awsURL = fileLink;
      }
    } catch (error) {
      console.log('Error in upload file :', error);
    }
    return data;
  };

  const handleFilesUploader = async (file) => {
    if (file) {
      const url = URL.createObjectURL(file);
      setMediaFiles({ url, awsURL: '', file });
    }
  };

  useEffect(() => {
    setMediaFiles({ url: userInfo?.avatar, awsURL: userInfo?.avatar, file: null });
  }, [userInfo]);

  useEffect(() => {
    if (error) alert(error);
    if (success) alert('Profile updated successfully!');
    dispatch({ type: UPDATE_USER_PROFILE_RESET });
  }, [error, success, dispatch]);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    let allImagesURL = await getAllImagesURL();
    dispatch(updateUserProfile({ ...formData, avatar: allImagesURL.awsURL }));
  };

  return (
    <div className="flex items-center justify-center h-[80vh]">
      <div className="p-6 max-w-4xl w-[60vw] mx-auto bg-white rounded shadow-md">
        <h2 className="text-2xl font-semibold text-gray-800 mb-4 text-center">User Profile</h2>
        <div className="flex justify-center">
          <ProfilePicture
            initialUrl={mediaFiles?.url || ''}
            handleFilesUploader={handleFilesUploader}
          />
        </div>
        <form onSubmit={handleSubmit} className="space-y-4 mt-8">
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
            <div>
              <label className="block text-gray-700">Email</label>
              <div className="flex items-center border rounded p-2">
                <MdOutlineMail className="mr-2 text-gray-500" />
                <input
                  type="email"
                  name="email"
                  value={formData.email}
                  onChange={handleChange}
                  className="w-full outline-none"
                  required
                />
              </div>
            </div>
            <div>
              <label className="block text-gray-700">Full Name</label>
              <input
                type="text"
                name="fullName"
                value={formData.fullName}
                onChange={handleChange}
                className="w-full border rounded p-2 outline-none"
                required
              />
            </div>
            <div>
              <label className="block text-gray-700">Mobile No</label>
              <div className="flex items-center border rounded p-2">
                <IoMdCall className="mr-2 text-gray-500" />
                <input
                  type="text"
                  name="mobileNo"
                  value={formData.mobileNo}
                  onChange={handleChange}
                  className="w-full outline-none"
                  required
                />
              </div>
            </div>
            <div>
              <label className="block text-gray-700">Gender</label>
              <select
                name="gender"
                value={formData.gender}
                onChange={handleChange}
                className="w-full border rounded p-2 outline-none"
                required>
                <option value="">Select Gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="Others">Others</option>
              </select>
            </div>
            <div>
              <label className="block text-gray-700">Aadhaar Number</label>
              <input
                type="text"
                name="aadhaarNo"
                value={formData.aadhaarNo}
                onChange={handleChange}
                className="w-full border rounded p-2 outline-none"
                required
              />
            </div>
          </div>
          <button
            type="submit"
            className="flex items-center justify-center w-full bg-blue-500 text-white p-2 rounded hover:bg-blue-600"
            disabled={loading}>
            {loading ? (
              'Updating...'
            ) : (
              <>
                <BiSave className="mr-2" /> Update
              </>
            )}
          </button>
        </form>
      </div>
    </div>
  );
};

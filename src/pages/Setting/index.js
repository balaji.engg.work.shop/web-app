import { Box, Text, Flex, Stack } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { UserProfile } from './UserProfile';
import { ChangePassword } from './ChangePassword';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { SIGN_IN } from '../../constants/routeConstant';
import { MyTabs } from '../../components';

const options = ['Profile', 'Security'];

export function Setting(props) {
  const navigate = useNavigate();
  const [selectedTab, setSelectedTab] = useState(1);

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  useEffect(() => {
    if (!userInfo) {
      navigate(SIGN_IN, { state: { path: '/setting' } });
    }
  }, [userInfo]);

  return (
    <Box w="100%" h="100vh" bgColor="#F5F5F5" m="0">
      <Flex
        p="3"
        align="center"
        justifyContent="space-between"
        borderTop="1px"
        borderLeft="1px"
        // border="1px solid green"
        borderColor="#EBEBEB"
        pr="20"
        bgColor="#FFFFFF">
        <Text px="2" fontWeight="600" fontSize="2xl" m="0">
          Setting
        </Text>
      </Flex>

      <Flex
        borderTop="1px"
        borderLeft="1px"
        // border="1px solid green"
        borderColor="#EBEBEB">
        <MyTabs selectedTab={selectedTab} setSelectedTab={setSelectedTab} options={options} />
      </Flex>

      {selectedTab === 1 ? <UserProfile /> : selectedTab === 2 ? <ChangePassword /> : null}
    </Box>
  );
}

import React, { useEffect, useState } from 'react';
import { UserOutlined } from '@ant-design/icons';
import { Layout, Menu, theme } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import { signOut } from '../../action/userActions';
import {
  BANK_ACCOUNT,
  BUY_PRODUCTS,
  CUSTOMERS,
  DAILY_MANUFACTURE,
  DASHBOARD,
  PRODUCTS,
  SELL_PRODUCTS,
  SETTING,
  SHOP,
  SIGN_IN,
  SUPPLIER,
  SELL_TRANSACTION,
  USERS,
  BUY_TRANSACTION,
  ESTIMATE,
  SELL_RETURN,
  GALLERY
} from '../../constants/routeConstant';
import { Box } from '@chakra-ui/react';

import SubMenu from 'antd/es/menu/SubMenu';

const { Content, Sider } = Layout;

export const ShopDashBoard = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const sideStyle = {
    overflow: 'auto',
    height: '90vh',
    position: 'relative',
    insetInlineStart: 0,
    top: 0,
    bottom: 0,
    scrollbarWidth: 'thin'
  };

  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer, borderRadiusLG }
  } = theme.useToken();

  const signOutHandler = () => {
    dispatch(signOut());
    navigate(SIGN_IN);
    window.location.reload();
  };

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  useEffect(() => {
    if (!userInfo) {
      navigate(SIGN_IN);
    }
  }, [userInfo]);

  const [current, setCurrent] = useState();

  function handleClick(e) {
    setCurrent(e.key);
  }

  return (
    <Layout style={{ minHeight: '100vh', maxHeight: '100vh', maxWidth: '100hW' }}>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
        width={'250px'}
        style={{
          background: colorBgContainer
        }}>
        <Menu
          onClick={handleClick}
          mode="vertical"
          // style={{
          //   background: colorBgContainer,
          //   fontSize: '3vh'
          // }}
          style={sideStyle}
          // selectedKeys={[current]}
          defaultSelectedKeys={['Dashboard']}>
          <div className="flex gap-4 items-center justify-start px-4 py-2 border border-2 bg-yellow-200">
            <img
              src={userInfo?.avatar ?? 'https://cdn-icons-png.flaticon.com/512/3135/3135715.png'}
              alt="profile image"
              className="h-10 w-10 rounded-full"
            />
            {!collapsed && <h1 className="font-bold text-[16px] ">{userInfo?.fullName}</h1>}
            <i className="fi fi-rs-log-out" title="Log out" onClick={signOutHandler}></i>
          </div>
          <Menu.Item key={'DashBoard' || ''} icon={<i className="fi fi-rr-apps"></i>} size="large">
            <Link to={DASHBOARD}>DashBoard</Link>
          </Menu.Item>
          {userInfo?.isAdmin && (
            <Menu.Item key={'Users'} icon={<UserOutlined size="3vh" />} size="large">
              <Link to={USERS}> Users</Link>
            </Menu.Item>
          )}

          <Menu.Item key={'Products'} icon={<i className="fi fi-rr-box-open"></i>} size="large">
            <Link to={PRODUCTS}>Products</Link>
          </Menu.Item>

          <Menu.Item key={'Customers'} icon={<i className="fi fi-rr-hr-person"></i>} size="large">
            <Link to={CUSTOMERS}>Customers</Link>
          </Menu.Item>
          <Menu.Item key={'Supplier'} icon={<i className="fi fi-rr-supplier-alt"></i>} size="large">
            <Link to={SUPPLIER}>Supplier</Link>
          </Menu.Item>
          {userInfo?.isAdmin && (
            <Menu.Item key={'BankAccount'} icon={<i className="fi fi-rs-bank"></i>} size="large">
              <Link to={BANK_ACCOUNT}>Bank Account</Link>
            </Menu.Item>
          )}
          <Menu.Item
            key={'Buy Products'}
            icon={<i className="fi fi-rr-cart-arrow-down"></i>}
            size="large">
            <Link to={BUY_PRODUCTS}>Buy Products</Link>
          </Menu.Item>

          <Menu.Item key={'SellProducts'} icon={<i className="fi fi-rr-sell"></i>} size="large">
            <Link to={SELL_PRODUCTS}>Sell Product</Link>
          </Menu.Item>

          <Menu.Item
            key={'Estimate'}
            icon={<i className="fi fi-rs-calculator-bill"></i>}
            size="large">
            <Link to={ESTIMATE}>Generate Estimate </Link>
          </Menu.Item>
          <Menu.Item
            key={'Transaction'}
            icon={<i className="fi fi-tr-chart-mixed-up-circle-dollar"></i>}
            size="large">
            <Link to={SELL_TRANSACTION}>Transactions</Link>
          </Menu.Item>

          <Menu.Item
            key={'Daily Manufacture'}
            icon={<i className="fi fi-ts-industry-alt"></i>}
            size="large">
            <Link to={DAILY_MANUFACTURE}>Daily Manufacture</Link>
          </Menu.Item>
          <Menu.Item key={'Sell Return'} icon={<i className="fi fi-rr-restock"></i>} size="large">
            <Link to={SELL_RETURN}>Sell Return</Link>
          </Menu.Item>
          <Menu.Item key={'Gallery'} icon={<i className="fi fi-bs-layout-fluid"></i>} size="large">
            <Link to={GALLERY}>Gallery</Link>
          </Menu.Item>
          {userInfo?.isAdmin && (
            <Menu.Item
              key={'ShopDetails'}
              icon={<i className="fi fi-rs-store-alt"></i>}
              size="large">
              <Link to={SHOP}>My Shop</Link>
            </Menu.Item>
          )}

          <Menu.Item key={'Setting'} icon={<i className="fi fi-rr-settings"></i>} size="large">
            <Link to={SETTING}>Setting</Link>
          </Menu.Item>

          <Menu.Item
            key={'/logout'}
            icon={<i className="fi fi-rs-log-out"></i>}
            onClick={signOutHandler}>
            Log Out
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Content style={{}}>
          <Box height="100vh" maxH="100vh" bgColor="#F5F5F5" overflow="hidden">
            {props.children}
          </Box>
        </Content>
      </Layout>
    </Layout>
  );
};

// 'rgb(26, 32, 44)'

import { Box, Flex, SimpleGrid, Text, Button } from '@chakra-ui/react';
import { Checkbox, Form, Input, message } from 'antd';
import { IoLogoWhatsapp } from 'react-icons/io';
import { BiLogoGmail, BiSave } from 'react-icons/bi';
import { useDispatch, useSelector } from 'react-redux';
import { getShopDetails, updateShopDetails } from '../../action/shopActions';
import { useEffect, useState } from 'react';
import { UPDATE_SHOP_DETAILS_RESET } from '../../constants/shopConstants';
import { useNavigate } from 'react-router-dom';
import { UpdateButton } from '../../components/InputComponents/MyButton';
import {
  isValidEmail,
  isValidGST,
  isValidMobileNo,
  isValidPinCode
} from '../../utils/validateData';
import { getSignedUrl, uploadFileToSignedUrl } from '../../utils/s3Utils';
import { MultipleFileUploader } from '../../components/molecule/MultipleFileUploader';
import ImageView from '../../components/molecule/ImageView';

export function ShopDetails() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [location, setLocation] = useState({
    lat: null,
    lng: null
  });

  console.log('location', location);
  const [componentDisabled, setComponentDisabled] = useState(false);
  const [mediaFiles, setMediaFiles] = useState([]);

  const shopDetail = useSelector((state) => state.shopDetail);
  const { error, loading, data: shop } = shopDetail;

  const updateShop = useSelector((state) => state.updateShop);
  const {
    error: errorUpdateShop,
    loading: loadingUpdateShop,
    data: shopUpdated,
    success
  } = updateShop;

  useEffect(() => {
    setMediaFiles(
      shop?.shopImages?.map((url) => {
        return {
          url,
          awsURL: url,
          file: null
        };
      })
    );
    console.log('shop?.geoLocation', shop?.geoLocation);
    setLocation({ lat: shop?.geoLocation?.lat, lng: shop?.geoLocation?.lng });
  }, [shop]);

  useEffect(() => {
    if (errorUpdateShop) {
      message.error(errorUpdateShop);
      dispatch({ type: UPDATE_SHOP_DETAILS_RESET });
    }
    if (success) {
      message.success('Shop updated success fully!');
      dispatch({ type: UPDATE_SHOP_DETAILS_RESET });
      setTimeout(() => {
        navigate('/');
      }, 0);
    }
  }, [success, errorUpdateShop]);

  const getAllImagesURL = async () => {
    let myData = [...mediaFiles];
    for (let data of myData) {
      try {
        if (data.awsURL === '') {
          const content_type = data.file.type.split('/')[1];
          const response = await getSignedUrl({ key: 'shopImages', content_type });
          const { fileLink, signedUrl } = response;
          await uploadFileToSignedUrl(signedUrl, data.file, content_type, () => {
            console.log('File uplaoding on AWS');
          });
          data.awsURL = fileLink;
        }
      } catch (error) {
        console.log('Error in upload file :', error);
      }
    }
    return myData;
  };

  const handleFilesUploader = async (files) => {
    if (files) {
      const mediaFilesArray = Array.isArray(mediaFiles) ? [...mediaFiles] : [];

      for (let file of Array.from(files)) {
        const url = URL.createObjectURL(file);
        const awsURL = '';
        mediaFilesArray.push({ url, awsURL, file });
      }
      setMediaFiles(mediaFilesArray);
    }
  };

  const handleRemoveImage = (index) => {
    setMediaFiles((pre) => pre.filter((_, i) => i != index));
  };

  const handleUpdateShopDetails = async (values) => {
    let allImagesURL = await getAllImagesURL();
    allImagesURL = allImagesURL
      ?.filter((data) => data.awsURL?.length > 0)
      ?.map((data) => data.awsURL);
    dispatch(
      updateShopDetails({
        ...values,
        shopImages: allImagesURL,
        geoLocation: { lat: location.lat, lng: location.lng }
      })
    );
  };

  useEffect(() => {
    dispatch(getShopDetails());
  }, [navigate, dispatch]);

  const validateNumber = (_, value) => {
    return isValidMobileNo(value);
  };

  const validateGSTIN = (_, value) => {
    return isValidGST(value);
  };
  const validateEmail = (_, value) => {
    return isValidEmail(value);
  };
  const validatePinCode = (_, value) => {
    return isValidPinCode(value);
  };

  const getLatLang = () => {
    if (!navigator.geolocation) {
      console.error('Geolocation is not supported by your browser');
      return;
    }

    navigator.geolocation.getCurrentPosition(
      (position) => {
        setLocation({
          lat: position.coords.latitude,
          lng: position.coords.longitude
        });
      },
      (error) => {
        console.error('Error getting location:', error);
      }
    );
  };

  return (
    <Box bgColor="#F5F5F5" m="0">
      <Flex p="3" pl="5" bgColor="#FFFFFF">
        <Text fontWeight="600" fontSize="2xl" m="0">
          Shop Details
        </Text>
      </Flex>
      <Box p="2" pl="5">
        <Text color="#000000" align="center" fontSize="32px" paddingY="8px">
          Update Shop Details
        </Text>
        <Checkbox
          style={{ color: '#000000', paddingBottom: 8 }}
          checked={componentDisabled}
          onChange={(e) => setComponentDisabled(e.target.checked)}>
          Form disabled
        </Checkbox>
        <Form
          layout="vertical"
          disabled={componentDisabled}
          initialValues={{ remember: true }}
          autoComplete="off"
          onFinish={handleUpdateShopDetails}
          style={{ color: '#000000', fontSize: '40px' }}
          fields={[
            {
              name: ['shopName'],
              value: shop?.shopName
            },
            {
              name: ['ownerName'],
              value: shop?.ownerName
            },
            {
              name: ['email'],
              value: shop?.email
            },
            {
              name: ['address'],
              value: shop?.address
            },
            {
              name: ['city'],
              value: shop?.city
            },
            {
              name: ['state'],
              value: shop?.state
            },
            {
              name: ['pinCode'],
              value: shop?.pinCode
            },
            {
              name: ['shopEmail'],
              value: shop?.shopEmail
            },
            {
              name: ['gst'],
              value: shop?.gst
            },
            {
              name: ['shopMobileNo'],
              value: shop?.shopMobileNo
            },
            {
              name: ['lat'],
              value: location?.lat
            },
            {
              name: ['lng'],
              value: location?.lng
            }
          ]}>
          <SimpleGrid columns={[, 2, 3]} gap="4">
            <Form.Item
              label={<label style={{ color: '#000000' }}>Shop Name</label>}
              name="shopName"
              fontSize="40"
              rules={[
                {
                  required: true,
                  message: 'Please input your Shop Name!'
                }
              ]}>
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Shop ownerName name</label>}
              name="ownerName"
              fontSize="40"
              rules={[
                {
                  required: true,
                  message: 'Please input your shop ownerName name!'
                }
              ]}>
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Address</label>}
              name="address"
              fontSize="40"
              rules={[
                {
                  required: true,
                  message: 'Please input your shop ownerName name!'
                }
              ]}>
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>City</label>}
              name="city"
              fontSize="40"
              rules={[
                {
                  required: true,
                  message: 'Please input your shop ownerName name!'
                }
              ]}>
              <Input size="large" />
            </Form.Item>

            <Form.Item
              label={<label style={{ color: '#000000' }}>State</label>}
              name="state"
              fontSize="40"
              rules={[
                {
                  required: true,
                  message: 'Please input your shop ownerName name!'
                }
              ]}>
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>PinCode</label>}
              name="pinCode"
              fontSize="40"
              rules={[
                {
                  required: true,
                  message: 'PinCode'
                },
                {
                  validator: validatePinCode
                }
              ]}>
              <Input size="large" maxLength="6" showCount />
            </Form.Item>

            <Form.Item
              label={<label style={{ color: '#000000' }}>WhatsApp Number</label>}
              name="shopMobileNo"
              rules={[
                {
                  required: true,
                  message: 'Please input your Mobile number!'
                },
                {
                  validator: validateNumber
                }
              ]}>
              <Input
                addonBefore={<IoLogoWhatsapp color="green" size="20px" />}
                maxLength={10}
                size="large"
                showCount
              />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Email</label>}
              name="shopEmail"
              fontSize="40"
              rules={[
                {
                  required: true,
                  message: 'Please input your Email'
                },
                {
                  validator: validateEmail
                }
              ]}>
              <Input addonBefore={<BiLogoGmail color="yellow" size="20px" />} size="large" />
            </Form.Item>

            <Form.Item label={<label style={{ color: '#000000' }}>GST</label>} name="lat">
              <Input placeholder="GST" size="large" autoComplete={'off'} maxLength={15} showCount />
            </Form.Item>
            <Form.Item
              label={<label style={{ color: '#000000' }}>Latitude</label>}
              name="lat"
              fontSize="40">
              <Input size="large" value={location?.lat} />
            </Form.Item>

            <Form.Item label={<label style={{ color: '#000000' }}>longitude</label>} name="lng">
              <Input placeholder="GST" size="large" autoComplete={'off'} value={location?.lng} />
            </Form.Item>
            <button
              onClick={getLatLang}
              type="button"
              className="px-6 py-1 bg-blue-600  text-[16px] text-white font-semibold rounded-lg shadow-md hover:bg-blue-700 transition-all">
              📍 Get Current Location
            </button>
          </SimpleGrid>
          <div className="flex gap-4 flex-wrap">
            {mediaFiles?.map((data, index) => (
              <ImageView url={data.url} key={index} index={index} removeImage={handleRemoveImage} />
            ))}
            <MultipleFileUploader handleFilesUploader={handleFilesUploader} />
          </div>
          <Form.Item>
            {!componentDisabled && (
              <Flex justifyContent="center" gap="8" pt="8">
                <UpdateButton
                  loading={loadingUpdateShop}
                  loadingText={'Updating..'}
                  label="Update"
                />
              </Flex>
            )}
          </Form.Item>
        </Form>
      </Box>
    </Box>
  );
}

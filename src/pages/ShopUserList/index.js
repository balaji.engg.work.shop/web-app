import { Box, Text, Flex } from '@chakra-ui/react';
import { useEffect, useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteUserDetails, getAllUsers } from '../../action/userActions';
import { SearchInputField, ShopUserTable } from '../../components';
import { DELETE_USER_RESET } from '../../constants/userConstants';
import { useNavigate } from 'react-router-dom';
import { ROOT } from '../../constants/routeConstant';
import { AddButton } from '../../components/InputComponents/MyButton';
import { Button, message } from 'antd';
import { AddEditShopUserModal } from '../../components/modal/AddEditShopUserModel';

export function ShopUserList() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [option, setOptions] = useState('Add');
  const [open, setOpen] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [currentUser, setCurrentUser] = useState(null);

  const getUsers = useSelector((state) => state.getUsers);
  const { error, loading, Users } = getUsers;

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const deleteUser = useSelector((state) => state.deleteUser);
  const { error: errorDeleteUser, loading: loadingDeleteUser, success } = deleteUser;

  const confirm = (e, record) => {
    e.preventDefault();
    dispatch(deleteUserDetails(record));
  };

  const handleOpenEditUserModel = useCallback(
    (user) => {
      setOptions('Edit');
      setOpen(true);
      setCurrentUser(user);
    },
    [open]
  );

  const handleOpenAddCustomerModel = useCallback(() => {
    setOptions('Add');
    setOpen(true);
  }, [open]);

  const handleCloseModel = useCallback(() => {
    setOpen(false);
  }, [open]);

  useEffect(() => {
    if (userInfo?.userRole !== 'Admin') {
      message.error('You have no access!');
      navigate(ROOT);
    }
    if (!(Users?.length > 0)) dispatch(getAllUsers());
  }, [dispatch, userInfo, navigate]);

  useEffect(() => {
    if (error) {
      message.error(error);
    }
    if (errorDeleteUser) {
      message.error(errorDeleteUser);
      dispatch({ type: DELETE_USER_RESET });
    }
    if (success) {
      message.success('User record deleted successfully');
      dispatch({ type: DELETE_USER_RESET });

      dispatch(getAllUsers());
    }
  }, [error, errorDeleteUser, success, dispatch]);

  return (
    <Box bgColor="#F5F5F5" m="0">
      <AddEditShopUserModal
        open={open}
        onCancel={handleCloseModel}
        option={option}
        user={currentUser}
        setCurrentUser={() => {
          setCurrentUser({});
        }}
      />
      <Flex p="3" pl="5" bgColor="#FFFFFF" justifyContent={'space-between'}>
        <Text fontWeight="600" fontSize="2xl" m="0">
          Shop user List
        </Text>
        <div className="w-96">
          <SearchInputField
            value={searchText}
            onChange={setSearchText}
            placeholder="Search user by name"
          />
        </div>
      </Flex>
      <Box p="2" pl="5">
        <Flex justifyContent="space-between">
          <AddButton onClick={handleOpenAddCustomerModel} label="User" />
          <Button color="#FFFFFF" bgColor="green">
            Total users : {Users?.length}
          </Button>
        </Flex>
        <div className="py-2">
          <ShopUserTable
            data={Users?.filter((user) =>
              user.fullName.toLowerCase()?.includes(searchText.toLowerCase())
            )}
            loading={loading}
            handelEditUser={handleOpenEditUserModel}
            confirm={confirm}
          />
        </div>
      </Box>
    </Box>
  );
}

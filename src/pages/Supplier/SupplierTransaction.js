import { Box, Flex, Text } from '@chakra-ui/react';
import { useEffect } from 'react';
import { getSupplierTransactionList } from '../../action/transactionActions';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { convertNumberToIndian } from '../../utils/validateData';
import { TransactionTable } from '../../components';
import { getPurchaseProductsDetails } from '../../action/purchaseProductAction';
import { PURCHASE_PRODUCT_DETAILS } from '../../constants/routeConstant';

export const SupplierTransaction = (props) => {
  const dispatch = useDispatch();
  const location = useLocation();
  const navigate = useNavigate();
  const { supplierId, supplier } = location.state;

  const supplierTransactionList = useSelector((state) => state.supplierTransactionList);
  const { error, loading, data: transactions } = supplierTransactionList;

  useEffect(() => {
    dispatch(getSupplierTransactionList(supplierId));
  }, []);

  const getTotalPurchaseAmount = () => {
    let ans = transactions?.reduce((accum, current) => {
      return accum + current.paidAmount;
    }, 0);
    return ans;
  };

  return (
    <Box bgColor="#F5F5F5" m="0">
      <Flex p="3" pl="5" bgColor="#FFFFFF">
        <Text fontWeight="600" fontSize="2xl" m="0">
          Supplier Transactions
        </Text>
      </Flex>
      <Flex pl="5" mt="4" bgColor="" flexDir="row" gap="8">
        <Box gap="4">
          <Text fontWeight="400" fontSize="lg" m="0">
            Supplier name
          </Text>
          <Text fontWeight="400" fontSize="lg" m="0">
            Total purchase Amount
          </Text>
        </Box>
        <Box gap="4">
          <Text fontWeight="600" fontSize="lg" m="0">
            {supplier?.supplierName}
          </Text>
          <Text fontWeight="400" fontSize="lg" m="0" color="green.600">
            {convertNumberToIndian(getTotalPurchaseAmount())}
          </Text>
        </Box>
      </Flex>
      <Box p="2" pl="5">
        <TransactionTable
          loading={loading}
          data={transactions}
          deleteTransaction={() => {}}
          getDetail={(billNo) => {
            dispatch(getPurchaseProductsDetails(billNo));
            navigate(PURCHASE_PRODUCT_DETAILS);
          }}
        />
      </Box>
    </Box>
  );
};

import { Box, Button, Flex, Text, Stack } from '@chakra-ui/react';
import { useEffect, useState, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getAllSuppliers } from '../../action/supplierAction';
import { SIGN_IN } from '../../constants/routeConstant';
import { AddButton } from '../../components/InputComponents/MyButton';
import { AddEditSupplierModel } from '../../components/modal/AddEditSupplierModel';
import { SearchInputField, SupplierTable } from '../../components';

export function Suppliers() {
  const [searchText, setSearchText] = useState('');
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [option, setOption] = useState('Add');
  const [open, setOpen] = useState(false);
  const [currentSupplier, setCurrentSupplier] = useState({});

  const userSignIn = useSelector((state) => state.userSignIn);
  const { userInfo } = userSignIn;

  const supplierList = useSelector((state) => state.supplierList);
  const { error, loading, suppliers } = supplierList;

  const handleOpenEditSupplierModal = useCallback(
    (supplier) => {
      setOption('Edit');
      setOpen(true);
      setCurrentSupplier(supplier);
    },
    [open]
  );

  const handleOpenAddSupplierModal = useCallback(() => {
    setOption('Add');
    setOpen(true);
  }, [open]);

  const handleCloseModel = useCallback(() => {
    setOpen(false);
  }, [open]);

  useEffect(() => {
    if (!userInfo) {
      navigate(SIGN_IN);
    }
    if (!(suppliers?.length > 0)) dispatch(getAllSuppliers());
  }, [navigate, userInfo]);

  return (
    <Box bgColor="#F5F5F5" m="0" height="100%">
      <AddEditSupplierModel
        open={open}
        onCancel={handleCloseModel}
        option={option}
        supplier={currentSupplier}
        setCurrentSupplier={setCurrentSupplier}
      />
      <Flex p="3" pl="5" bgColor="#FFFFFF" justifyContent={'space-between'}>
        <Text fontWeight="600" fontSize="2xl" m="0">
          Supplier
        </Text>
        <div className="w-96">
          <SearchInputField
            value={searchText}
            onChange={setSearchText}
            placeholder="Search supplier by name"
          />
        </div>
      </Flex>
      <Box p="2" pl="5">
        <Flex justifyContent="space-between">
          <AddButton onClick={handleOpenAddSupplierModal} label="Supplier" />
          <Button>Total supplier : {suppliers?.length}</Button>
        </Flex>
        <div className="py-2">
          <SupplierTable
            data={suppliers?.filter((supplier) =>
              supplier.supplierName.toLowerCase()?.includes(searchText.toLowerCase())
            )}
            loading={loading}
            handleEditSupplier={handleOpenEditSupplierModal}
          />
        </div>
      </Box>
    </Box>
  );
}

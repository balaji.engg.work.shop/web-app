import { Box, Flex, Text } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import {
  getPurchaseTransactionList,
  getSellReturnTransactionList,
  getTransactionList
} from '../../action/transactionActions';
import { useDispatch, useSelector } from 'react-redux';
import { CustomTab, TransactionTable } from '../../components';
import { deleteSellProductDetails, getSellProductDetails } from '../../action/sellProductAction';
import { DELETE_SELL_PRODUCTS_DETAIL_RESET } from '../../constants/sellProductConstants';
import { message } from 'antd';
import { getAllCustomers } from '../../action/customerAction';
import { getAllProduct } from '../../action/productActions';
import {
  PURCHASE_PRODUCT_DETAILS,
  SELL_PRODUCT_DETAILS,
  SELL_RETURN_PRODUCT_DETAILS
} from '../../constants/routeConstant';
import { getSellReturnDetails } from '../../action/sellReturnAction';
import { useNavigate } from 'react-router-dom';
import { getPurchaseProductsDetails } from '../../action/purchaseProductAction';

export const Transactions = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [current, setCurrent] = useState('Sell Transaction');

  const transactionList = useSelector((state) => state.transactionList);
  const { error, loading, data: transactions } = transactionList;
  const getPurchaseTransaction = useSelector((state) => state.getPurchaseTransaction);
  const {
    error: errorPurchaseTransaction,
    loading: loadingPurchaseTransaction,
    data: purchaseTransactions
  } = getPurchaseTransaction;

  const sellReturnTransaction = useSelector((state) => state.sellReturnTransaction);
  const {
    error: errorSellReturnTransaction,
    loading: loadingSellReturnTransaction,
    data: sellReturnTransactionData
  } = sellReturnTransaction;

  const customerSellReturnTransaction = useSelector((state) => state.customerSellReturnTransaction);
  const {
    error: errorCustomerSellReturnTransaction,
    loading: loadingCustomerSellReturnTransaction,
    data: sellCustomerSellReturnTransaction
  } = customerSellReturnTransaction;

  const deleteSellProduct = useSelector((state) => state.deleteSellProduct);
  const { error: errorDeleteTransaction, success: successDeleteSellTransaction } =
    deleteSellProduct;
  const handelDeleteSellTransaction = (transactionId) => {
    if (window.confirm('Do u really want to delete this transaction?'))
      dispatch(deleteSellProductDetails(transactionId));
  };

  const deletePurchaseProductDetails = (transactionId) => {
    if (window.confirm('Do u really want to delete this transaction?'))
      console.log('purchase transaction deleted!');
  };

  useEffect(() => {
    if (successDeleteSellTransaction) {
      message.success('Deleted successfully!');
      dispatch(getTransactionList());
      dispatch(getAllCustomers());
      dispatch(getAllProduct());
      dispatch({ type: DELETE_SELL_PRODUCTS_DETAIL_RESET });
    } else if (errorDeleteTransaction) {
      message.error(errorDeleteTransaction);
      dispatch({ type: DELETE_SELL_PRODUCTS_DETAIL_RESET });
    }
  }, [errorDeleteTransaction, successDeleteSellTransaction]);

  useEffect(() => {
    dispatch(getTransactionList());
    dispatch(getPurchaseTransactionList());
    dispatch(getSellReturnTransactionList());
  }, []);

  return (
    <Box bgColor="#F5F5F5" m="0">
      <Flex p="3" pl="5" bgColor="#FFFFFF">
        <Text fontWeight="600" fontSize="2xl" m="0">
          Transactions
        </Text>
      </Flex>

      <Box p="2" pl="5">
        <CustomTab
          setCurrent={(value) => setCurrent(value)}
          data={['Sell Transaction', 'Purchase Transaction', 'Sell Return']}
          current={current}
        />
        {current === 'Sell Transaction' && (
          <TransactionTable
            loading={loading}
            data={transactions}
            deleteTransaction={handelDeleteSellTransaction}
            getDetail={(billNo) => {
              dispatch(getSellProductDetails(billNo));
              navigate(SELL_PRODUCT_DETAILS);
            }}
          />
        )}
        {current === 'Purchase Transaction' && (
          <TransactionTable
            loading={loading}
            data={purchaseTransactions}
            deleteTransaction={deletePurchaseProductDetails}
            getDetail={(billNo) => {
              dispatch(getPurchaseProductsDetails(billNo));
              navigate(PURCHASE_PRODUCT_DETAILS);
            }}
          />
        )}
        {current === 'Sell Return' && (
          <TransactionTable
            loading={loadingSellReturnTransaction}
            data={sellReturnTransactionData}
            deleteTransaction={() => {}}
            getDetail={(billNo) => {
              dispatch(getSellReturnDetails(billNo));
              navigate(SELL_RETURN_PRODUCT_DETAILS);
            }}
          />
        )}
      </Box>
    </Box>
  );
};

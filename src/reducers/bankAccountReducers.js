import {
  ADD_NEW_BANK_ACCOUNT_FAIL,
  ADD_NEW_BANK_ACCOUNT_REQUEST,
  ADD_NEW_BANK_ACCOUNT_RESET,
  ADD_NEW_BANK_ACCOUNT_SUCCESS,
  DELETE_BANK_ACCOUNT_FAIL,
  DELETE_BANK_ACCOUNT_REQUEST,
  DELETE_BANK_ACCOUNT_RESET,
  DELETE_BANK_ACCOUNT_SUCCESS,
  GET_ALL_BANK_ACCOUNT_FAIL,
  GET_ALL_BANK_ACCOUNT_REQUEST,
  GET_ALL_BANK_ACCOUNT_RESET,
  GET_ALL_BANK_ACCOUNT_SUCCESS,
  GET_BANK_ACCOUNT_DETAILS_FAIL,
  GET_BANK_ACCOUNT_DETAILS_REQUEST,
  GET_BANK_ACCOUNT_DETAILS_RESET,
  GET_BANK_ACCOUNT_DETAILS_SUCCESS,
  UPDATE_BANK_ACCOUNT_FAIL,
  UPDATE_BANK_ACCOUNT_REQUEST,
  UPDATE_BANK_ACCOUNT_RESET,
  UPDATE_BANK_ACCOUNT_SUCCESS
} from '../constants/bankAccountConstants';

export function createNewBankAccountReducer(state = {}, action) {
  switch (action.type) {
    case ADD_NEW_BANK_ACCOUNT_REQUEST:
      return { loading: true };
    case ADD_NEW_BANK_ACCOUNT_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case ADD_NEW_BANK_ACCOUNT_FAIL:
      return { loading: false, error: action.payload };
    case ADD_NEW_BANK_ACCOUNT_RESET:
      return {};
    default:
      return state;
  }
}

export function updateBankAccountReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_BANK_ACCOUNT_REQUEST:
      return { loading: true };
    case UPDATE_BANK_ACCOUNT_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case UPDATE_BANK_ACCOUNT_FAIL:
      return { loading: false, error: action.payload };
    case UPDATE_BANK_ACCOUNT_RESET:
      return {};
    default:
      return state;
  }
}

export function deleteBankAccountReducer(state = {}, action) {
  switch (action.type) {
    case DELETE_BANK_ACCOUNT_REQUEST:
      return { loading: true };
    case DELETE_BANK_ACCOUNT_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case DELETE_BANK_ACCOUNT_FAIL:
      return { loading: false, error: action.payload };
    case DELETE_BANK_ACCOUNT_RESET:
      return {};
    default:
      return state;
  }
}

export function getAllBankAccountReducer(state = { data: [] }, action) {
  switch (action.type) {
    case GET_ALL_BANK_ACCOUNT_REQUEST:
      return { loading: true };
    case GET_ALL_BANK_ACCOUNT_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_ALL_BANK_ACCOUNT_FAIL:
      return { loading: false, error: action.payload };
    case GET_ALL_BANK_ACCOUNT_RESET:
      return {};
    default:
      return state;
  }
}

export function getBankAccountDetailsReducer(state = {}, action) {
  switch (action.type) {
    case GET_BANK_ACCOUNT_DETAILS_REQUEST:
      return { loading: true };
    case GET_BANK_ACCOUNT_DETAILS_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_BANK_ACCOUNT_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    case GET_BANK_ACCOUNT_DETAILS_RESET:
      return {};
    default:
      return state;
  }
}

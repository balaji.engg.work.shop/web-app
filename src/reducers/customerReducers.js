import {
  CREATE_NEW_CUSTOMER_FAIL,
  CREATE_NEW_CUSTOMER_REQUEST,
  CREATE_NEW_CUSTOMER_RESET,
  CREATE_NEW_CUSTOMER_SUCCESS,
  DELETE_CUSTOMER_FAIL,
  DELETE_CUSTOMER_REQUEST,
  DELETE_CUSTOMER_RESET,
  DELETE_CUSTOMER_SUCCESS,
  GET_ALL_CUSTOMER_FAIL,
  GET_ALL_CUSTOMER_REQUEST,
  GET_ALL_CUSTOMER_RESET,
  GET_ALL_CUSTOMER_SUCCESS,
  GET_CUSTOMER_FAIL,
  GET_CUSTOMER_REQUEST,
  GET_CUSTOMER_RESET,
  GET_CUSTOMER_SUCCESS,
  UPDATE_CUSTOMER_FAIL,
  UPDATE_CUSTOMER_REQUEST,
  UPDATE_CUSTOMER_RESET,
  UPDATE_CUSTOMER_SUCCESS
} from '../constants/customerConstants';

export function createNewCustomerReducer(state = {}, action) {
  switch (action.type) {
    case CREATE_NEW_CUSTOMER_REQUEST:
      return { loading: true };
    case CREATE_NEW_CUSTOMER_SUCCESS:
      return {
        loading: false,
        success: true,
        customer: action.payload
      };
    case CREATE_NEW_CUSTOMER_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_NEW_CUSTOMER_RESET:
      return {};
    default:
      return state;
  }
}

export function updateCustomerReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_CUSTOMER_REQUEST:
      return { loading: true };
    case UPDATE_CUSTOMER_SUCCESS:
      return {
        loading: false,
        success: true,
        customer: action.payload
      };
    case UPDATE_CUSTOMER_FAIL:
      return { loading: false, error: action.payload };
    case UPDATE_CUSTOMER_RESET:
      return {};
    default:
      return state;
  }
}

export function getAllCustomerReducer(state = { customers: [] }, action) {
  switch (action.type) {
    case GET_ALL_CUSTOMER_REQUEST:
      return { loading: true };
    case GET_ALL_CUSTOMER_SUCCESS:
      return {
        loading: false,
        success: true,
        customers: action.payload
      };
    case GET_ALL_CUSTOMER_FAIL:
      return { loading: false, error: action.payload };
    case GET_ALL_CUSTOMER_RESET:
      return {};
    default:
      return state;
  }
}

export function getCustomerDetailsReducer(state = {}, action) {
  switch (action.type) {
    case GET_CUSTOMER_REQUEST:
      return { loading: true };
    case GET_CUSTOMER_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_CUSTOMER_FAIL:
      return { loading: false, error: action.payload };
    case GET_CUSTOMER_RESET:
      return {};
    default:
      return state;
  }
}

export function customerDeleteReducer(state = {}, action) {
  switch (action.type) {
    case DELETE_CUSTOMER_REQUEST:
      return { loading: true };
    case DELETE_CUSTOMER_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case DELETE_CUSTOMER_FAIL:
      return { loading: false, error: action.payload };
    case DELETE_CUSTOMER_RESET:
      return {};
    default:
      return state;
  }
}

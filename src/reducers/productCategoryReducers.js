import {
  ADD_NEW_PRODUCT_CATEGORY_FAIL,
  ADD_NEW_PRODUCT_CATEGORY_REQUEST,
  ADD_NEW_PRODUCT_CATEGORY_RESET,
  ADD_NEW_PRODUCT_CATEGORY_SUCCESS,
  GET_PRODUCT_CATEGORY_FAIL,
  GET_PRODUCT_CATEGORY_REQUEST,
  GET_PRODUCT_CATEGORY_RESET,
  GET_PRODUCT_CATEGORY_SUCCESS
} from '../constants/productCategoryConstant';

export function addProductCategoryReducer(state = {}, action) {
  switch (action.type) {
    case ADD_NEW_PRODUCT_CATEGORY_REQUEST:
      return { loading: true };
    case ADD_NEW_PRODUCT_CATEGORY_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case ADD_NEW_PRODUCT_CATEGORY_FAIL:
      return { loading: false, error: action.payload };
    case ADD_NEW_PRODUCT_CATEGORY_RESET:
      return {};
    default:
      return state;
  }
}
export function getProductCategoryReducer(state = {}, action) {
  switch (action.type) {
    case GET_PRODUCT_CATEGORY_REQUEST:
      return { loading: true };
    case GET_PRODUCT_CATEGORY_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_PRODUCT_CATEGORY_FAIL:
      return { loading: false, error: action.payload };
    case GET_PRODUCT_CATEGORY_RESET:
      return {};
    default:
      return state;
  }
}

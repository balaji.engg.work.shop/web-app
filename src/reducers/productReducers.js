import {
  ADD_DAILY_MANUFACTURE_FAIL,
  ADD_DAILY_MANUFACTURE_REQUEST,
  ADD_DAILY_MANUFACTURE_RESET,
  ADD_DAILY_MANUFACTURE_SUCCESS,
  ADD_PRODUCTS_IN_BULK_FAIL,
  ADD_PRODUCTS_IN_BULK_REQUEST,
  ADD_PRODUCTS_IN_BULK_RESET,
  ADD_PRODUCTS_IN_BULK_SUCCESS,
  CREATE_NEW_PRODUCT_FAIL,
  CREATE_NEW_PRODUCT_REQUEST,
  CREATE_NEW_PRODUCT_RESET,
  CREATE_NEW_PRODUCT_SUCCESS,
  DELETE_ALL_PRODUCT_FAIL,
  DELETE_ALL_PRODUCT_REQUEST,
  DELETE_ALL_PRODUCT_RESET,
  DELETE_ALL_PRODUCT_SUCCESS,
  DELETE_PRODUCT_FAIL,
  DELETE_PRODUCT_REQUEST,
  DELETE_PRODUCT_RESET,
  DELETE_PRODUCT_SUCCESS,
  GET_ALL_PRODUCT_FAIL,
  GET_ALL_PRODUCT_REQUEST,
  GET_ALL_PRODUCT_RESET,
  GET_ALL_PRODUCT_SUCCESS,
  GET_DAILY_MANUFACTURE_FAIL,
  GET_DAILY_MANUFACTURE_REQUEST,
  GET_DAILY_MANUFACTURE_RESET,
  GET_DAILY_MANUFACTURE_SUCCESS,
  GET_PRODUCT_FAIL,
  GET_PRODUCT_REQUEST,
  GET_PRODUCT_RESET,
  GET_PRODUCT_SUCCESS,
  UPDATE_PRODUCT_FAIL,
  UPDATE_PRODUCT_REQUEST,
  UPDATE_PRODUCT_RESET,
  UPDATE_PRODUCT_SUCCESS
} from '../constants/productConstants';

export function createNewProductReducer(state = {}, action) {
  switch (action.type) {
    case CREATE_NEW_PRODUCT_REQUEST:
      return { loading: true };
    case CREATE_NEW_PRODUCT_SUCCESS:
      return {
        loading: false,
        success: true,
        product: action.payload
      };
    case CREATE_NEW_PRODUCT_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_NEW_PRODUCT_RESET:
      return {};
    default:
      return state;
  }
}

export function updateProductReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_PRODUCT_REQUEST:
      return { loading: true };
    case UPDATE_PRODUCT_SUCCESS:
      return {
        loading: false,
        success: true,
        product: action.payload
      };
    case UPDATE_PRODUCT_FAIL:
      return { loading: false, error: action.payload };
    case UPDATE_PRODUCT_RESET:
      return {};
    default:
      return state;
  }
}

export function deleteProductReducer(state = {}, action) {
  switch (action.type) {
    case DELETE_PRODUCT_REQUEST:
      return { loading: true };
    case DELETE_PRODUCT_SUCCESS:
      return {
        loading: false,
        success: true,
        product: action.payload
      };
    case DELETE_PRODUCT_FAIL:
      return { loading: false, error: action.payload };
    case DELETE_PRODUCT_RESET:
      return {};
    default:
      return state;
  }
}

export function deleteAllProductReducer(state = {}, action) {
  switch (action.type) {
    case DELETE_ALL_PRODUCT_REQUEST:
      return { loading: true };
    case DELETE_ALL_PRODUCT_SUCCESS:
      return {
        loading: false,
        success: true,
        product: action.payload
      };
    case DELETE_ALL_PRODUCT_FAIL:
      return { loading: false, error: action.payload };
    case DELETE_ALL_PRODUCT_RESET:
      return {};
    default:
      return state;
  }
}

export function getAllProductReducer(state = { products: [] }, action) {
  switch (action.type) {
    case GET_ALL_PRODUCT_REQUEST:
      return { loading: true };
    case GET_ALL_PRODUCT_SUCCESS:
      return {
        loading: false,
        success: true,
        products: action.payload
      };
    case GET_ALL_PRODUCT_FAIL:
      return { loading: false, error: action.payload };
    case GET_ALL_PRODUCT_RESET:
      return {};
    default:
      return state;
  }
}

export function getProductDetailsReducer(state = {}, action) {
  switch (action.type) {
    case GET_PRODUCT_REQUEST:
      return { loading: true };
    case GET_PRODUCT_SUCCESS:
      return {
        loading: false,
        success: true,
        product: action.payload
      };
    case GET_PRODUCT_FAIL:
      return { loading: false, error: action.payload };
    case GET_PRODUCT_RESET:
      return {};
    default:
      return state;
  }
}

export function addProductsInBulkReducer(state = {}, action) {
  switch (action.type) {
    case ADD_PRODUCTS_IN_BULK_REQUEST:
      return { loading: true };
    case ADD_PRODUCTS_IN_BULK_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case ADD_PRODUCTS_IN_BULK_FAIL:
      return { loading: false, error: action.payload };
    case ADD_PRODUCTS_IN_BULK_RESET:
      return {};
    default:
      return state;
  }
}

export function addDailyManufactureProductInBulkReducer(state = {}, action) {
  switch (action.type) {
    case ADD_DAILY_MANUFACTURE_REQUEST:
      return { loading: true };
    case ADD_DAILY_MANUFACTURE_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case ADD_DAILY_MANUFACTURE_FAIL:
      return { loading: false, error: action.payload };
    case ADD_DAILY_MANUFACTURE_RESET:
      return {};
    default:
      return state;
  }
}

export function getDailyManufactureProductListReducer(state = {}, action) {
  switch (action.type) {
    case GET_DAILY_MANUFACTURE_REQUEST:
      return { loading: true };
    case GET_DAILY_MANUFACTURE_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_DAILY_MANUFACTURE_FAIL:
      return { loading: false, error: action.payload };
    case GET_DAILY_MANUFACTURE_RESET:
      return {};
    default:
      return state;
  }
}

import {
  ADD_NEW_PURCHASE_PRODUCTS_DATA_FAIL,
  ADD_NEW_PURCHASE_PRODUCTS_DATA_REQUEST,
  ADD_NEW_PURCHASE_PRODUCTS_DATA_RESET,
  ADD_NEW_PURCHASE_PRODUCTS_DATA_SUCCESS,
  GET_PURCHASE_PRODUCTS_DETAILS_FAIL,
  GET_PURCHASE_PRODUCTS_DETAILS_REQUEST,
  GET_PURCHASE_PRODUCTS_DETAILS_RESET,
  GET_PURCHASE_PRODUCTS_DETAILS_SUCCESS
} from '../constants/purchaseProductConstants';

export function addPurchaseProductsReducer(state = {}, action) {
  switch (action.type) {
    case ADD_NEW_PURCHASE_PRODUCTS_DATA_REQUEST:
      return { loading: true };
    case ADD_NEW_PURCHASE_PRODUCTS_DATA_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case ADD_NEW_PURCHASE_PRODUCTS_DATA_FAIL:
      return { loading: false, error: action.payload };
    case ADD_NEW_PURCHASE_PRODUCTS_DATA_RESET:
      return {};
    default:
      return state;
  }
}

export function getPurchaseProductsDetailsReducer(state = {}, action) {
  switch (action.type) {
    case GET_PURCHASE_PRODUCTS_DETAILS_REQUEST:
      return { loading: true };
    case GET_PURCHASE_PRODUCTS_DETAILS_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_PURCHASE_PRODUCTS_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    case GET_PURCHASE_PRODUCTS_DETAILS_RESET:
      return {};
    default:
      return state;
  }
}

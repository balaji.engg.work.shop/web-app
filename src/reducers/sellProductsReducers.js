import {
  ADD_NEW_SELL_PRODUCTS_DATA_FAIL,
  ADD_NEW_SELL_PRODUCTS_DATA_REQUEST,
  ADD_NEW_SELL_PRODUCTS_DATA_RESET,
  ADD_NEW_SELL_PRODUCTS_DATA_SUCCESS,
  DELETE_SELL_PRODUCTS_DETAIL_FAIL,
  DELETE_SELL_PRODUCTS_DETAIL_REQUEST,
  DELETE_SELL_PRODUCTS_DETAIL_RESET,
  DELETE_SELL_PRODUCTS_DETAIL_SUCCESS,
  GET_COUNT_DATA_FAIL,
  GET_COUNT_DATA_REQUEST,
  GET_COUNT_DATA_RESET,
  GET_COUNT_DATA_SUCCESS,
  GET_SELL_PRODUCTS_DETAIL_FAIL,
  GET_SELL_PRODUCTS_DETAIL_REQUEST,
  GET_SELL_PRODUCTS_DETAIL_RESET,
  GET_SELL_PRODUCTS_DETAIL_SUCCESS
} from '../constants/sellProductConstants';

export function addSellProductsReducer(state = {}, action) {
  switch (action.type) {
    case ADD_NEW_SELL_PRODUCTS_DATA_REQUEST:
      return { loading: true };
    case ADD_NEW_SELL_PRODUCTS_DATA_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case ADD_NEW_SELL_PRODUCTS_DATA_FAIL:
      return { loading: false, error: action.payload };
    case ADD_NEW_SELL_PRODUCTS_DATA_RESET:
      return {};
    default:
      return state;
  }
}

export function getCountDataReducer(state = {}, action) {
  switch (action.type) {
    case GET_COUNT_DATA_REQUEST:
      return { loading: true };
    case GET_COUNT_DATA_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_COUNT_DATA_FAIL:
      return { loading: false, error: action.payload };
    case GET_COUNT_DATA_RESET:
      return {};
    default:
      return state;
  }
}

export function getSellProductDetailsReducer(state = {}, action) {
  switch (action.type) {
    case GET_SELL_PRODUCTS_DETAIL_REQUEST:
      return { loading: true };
    case GET_SELL_PRODUCTS_DETAIL_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_SELL_PRODUCTS_DETAIL_FAIL:
      return { loading: false, error: action.payload };
    case GET_SELL_PRODUCTS_DETAIL_RESET:
      return {};
    default:
      return state;
  }
}

export function deleteSellProductDetailsReducer(state = {}, action) {
  switch (action.type) {
    case DELETE_SELL_PRODUCTS_DETAIL_REQUEST:
      return { loading: true };
    case DELETE_SELL_PRODUCTS_DETAIL_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case DELETE_SELL_PRODUCTS_DETAIL_FAIL:
      return { loading: false, error: action.payload };
    case DELETE_SELL_PRODUCTS_DETAIL_RESET:
      return {};
    default:
      return state;
  }
}

import {
  CREATE_NEW_SELL_RETURN_FAIL,
  CREATE_NEW_SELL_RETURN_REQUEST,
  CREATE_NEW_SELL_RETURN_RESET,
  CREATE_NEW_SELL_RETURN_SUCCESS,
  GET_SELL_RETURN_DETAILS_FAIL,
  GET_SELL_RETURN_DETAILS_REQUEST,
  GET_SELL_RETURN_DETAILS_RESET,
  GET_SELL_RETURN_DETAILS_SUCCESS
} from '../constants/sellReturnConstants';

export function createNewSellReturnReducer(state = {}, action) {
  switch (action.type) {
    case CREATE_NEW_SELL_RETURN_REQUEST:
      return { loading: true };
    case CREATE_NEW_SELL_RETURN_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case CREATE_NEW_SELL_RETURN_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_NEW_SELL_RETURN_RESET:
      return {};
    default:
      return state;
  }
}

export function sellReturnDetailsReducer(state = {}, action) {
  switch (action.type) {
    case GET_SELL_RETURN_DETAILS_REQUEST:
      return { loading: true };
    case GET_SELL_RETURN_DETAILS_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_SELL_RETURN_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    case GET_SELL_RETURN_DETAILS_RESET:
      return {};
    default:
      return state;
  }
}

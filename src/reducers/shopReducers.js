import {
  CREATE_NEW_SHOP_FAIL,
  CREATE_NEW_SHOP_REQUEST,
  CREATE_NEW_SHOP_RESET,
  CREATE_NEW_SHOP_SUCCESS,
  SHOP_DETAILS_FAIL,
  SHOP_DETAILS_REQUEST,
  SHOP_DETAILS_RESET,
  SHOP_DETAILS_SUCCESS,
  UPDATE_SHOP_DETAILS_FAIL,
  UPDATE_SHOP_DETAILS_REQUEST,
  UPDATE_SHOP_DETAILS_RESET,
  UPDATE_SHOP_DETAILS_SUCCESS
} from '../constants/shopConstants';

export function createNewShopReducer(state = {}, action) {
  switch (action.type) {
    case CREATE_NEW_SHOP_REQUEST:
      return { loading: true };
    case CREATE_NEW_SHOP_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case CREATE_NEW_SHOP_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_NEW_SHOP_RESET:
      return {};
    default:
      return state;
  }
}

export function getShopDetailsReducer(state = {}, action) {
  switch (action.type) {
    case SHOP_DETAILS_REQUEST:
      return { loading: true };
    case SHOP_DETAILS_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case SHOP_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    case SHOP_DETAILS_RESET:
      return {};
    default:
      return state;
  }
}

export function updateShopDetailsReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_SHOP_DETAILS_REQUEST:
      return { loading: true };
    case UPDATE_SHOP_DETAILS_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case UPDATE_SHOP_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    case UPDATE_SHOP_DETAILS_RESET:
      return {};
    default:
      return state;
  }
}

import {
  CREATE_NEW_SUPPLIER_FAIL,
  CREATE_NEW_SUPPLIER_REQUEST,
  CREATE_NEW_SUPPLIER_RESET,
  CREATE_NEW_SUPPLIER_SUCCESS,
  GET_ALL_SUPPLIER_FAIL,
  GET_ALL_SUPPLIER_REQUEST,
  GET_ALL_SUPPLIER_RESET,
  GET_ALL_SUPPLIER_SUCCESS,
  GET_SUPPLIER_FAIL,
  GET_SUPPLIER_REQUEST,
  GET_SUPPLIER_RESET,
  GET_SUPPLIER_SUCCESS,
  UPDATE_SUPPLIER_FAIL,
  UPDATE_SUPPLIER_REQUEST,
  UPDATE_SUPPLIER_RESET,
  UPDATE_SUPPLIER_SUCCESS
} from '../constants/supplierConstants';

export function createNewSupplierReducer(state = {}, action) {
  switch (action.type) {
    case CREATE_NEW_SUPPLIER_REQUEST:
      return { loading: true };
    case CREATE_NEW_SUPPLIER_SUCCESS:
      return {
        loading: false,
        success: true,
        supplier: action.payload
      };
    case CREATE_NEW_SUPPLIER_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_NEW_SUPPLIER_RESET:
      return {};
    default:
      return state;
  }
}

export function updateSupplierReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_SUPPLIER_REQUEST:
      return { loading: true };
    case UPDATE_SUPPLIER_SUCCESS:
      return {
        loading: false,
        success: true,
        supplier: action.payload
      };
    case UPDATE_SUPPLIER_FAIL:
      return { loading: false, error: action.payload };
    case UPDATE_SUPPLIER_RESET:
      return {};
    default:
      return state;
  }
}

export function getAllSupplierReducer(state = { suppliers: [] }, action) {
  switch (action.type) {
    case GET_ALL_SUPPLIER_REQUEST:
      return { loading: true };
    case GET_ALL_SUPPLIER_SUCCESS:
      return {
        loading: false,
        success: true,
        suppliers: action.payload
      };
    case GET_ALL_SUPPLIER_FAIL:
      return { loading: false, error: action.payload };
    case GET_ALL_SUPPLIER_RESET:
      return {};
    default:
      return state;
  }
}

export function getSupplierDetailsReducer(state = {}, action) {
  switch (action.type) {
    case GET_SUPPLIER_REQUEST:
      return { loading: true };
    case GET_SUPPLIER_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_SUPPLIER_FAIL:
      return { loading: false, error: action.payload };
    case GET_SUPPLIER_RESET:
      return {};
    default:
      return state;
  }
}

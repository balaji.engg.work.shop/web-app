import {
  GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_FAIL,
  GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_REQUEST,
  GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_RESET,
  GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_SUCCESS,
  GET_CUSTOMER_TRANSACTION_LIST_FAIL,
  GET_CUSTOMER_TRANSACTION_LIST_REQUEST,
  GET_CUSTOMER_TRANSACTION_LIST_RESET,
  GET_CUSTOMER_TRANSACTION_LIST_SUCCESS,
  GET_PURCHASE_TRANSACTION_LIST_FAIL,
  GET_PURCHASE_TRANSACTION_LIST_REQUEST,
  GET_PURCHASE_TRANSACTION_LIST_RESET,
  GET_PURCHASE_TRANSACTION_LIST_SUCCESS,
  GET_SELL_RETURN_TRANSACTION_LIST_FAIL,
  GET_SELL_RETURN_TRANSACTION_LIST_REQUEST,
  GET_SELL_RETURN_TRANSACTION_LIST_RESET,
  GET_SELL_RETURN_TRANSACTION_LIST_SUCCESS,
  GET_SUPPLIER_TRANSACTION_LIST_FAIL,
  GET_SUPPLIER_TRANSACTION_LIST_REQUEST,
  GET_SUPPLIER_TRANSACTION_LIST_RESET,
  GET_SUPPLIER_TRANSACTION_LIST_SUCCESS,
  GET_TRANSACTION_LIST_FAIL,
  GET_TRANSACTION_LIST_REQUEST,
  GET_TRANSACTION_LIST_RESET,
  GET_TRANSACTION_LIST_SUCCESS
} from '../constants/transactionConstant';

export function getTransactionListReducer(state = {}, action) {
  switch (action.type) {
    case GET_TRANSACTION_LIST_REQUEST:
      return { loading: true };
    case GET_TRANSACTION_LIST_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_TRANSACTION_LIST_FAIL:
      return { loading: false, error: action.payload };
    case GET_TRANSACTION_LIST_RESET:
      return {};
    default:
      return state;
  }
}

export function getPurchaseTransactionListReducer(state = {}, action) {
  switch (action.type) {
    case GET_PURCHASE_TRANSACTION_LIST_REQUEST:
      return { loading: true };
    case GET_PURCHASE_TRANSACTION_LIST_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_PURCHASE_TRANSACTION_LIST_FAIL:
      return { loading: false, error: action.payload };
    case GET_PURCHASE_TRANSACTION_LIST_RESET:
      return {};
    default:
      return state;
  }
}

export function getCustomerTransactionListReducer(state = {}, action) {
  switch (action.type) {
    case GET_CUSTOMER_TRANSACTION_LIST_REQUEST:
      return { loading: true };
    case GET_CUSTOMER_TRANSACTION_LIST_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_CUSTOMER_TRANSACTION_LIST_FAIL:
      return { loading: false, error: action.payload };
    case GET_CUSTOMER_TRANSACTION_LIST_RESET:
      return {};
    default:
      return state;
  }
}

export function getSupplierTransactionListReducer(state = {}, action) {
  switch (action.type) {
    case GET_SUPPLIER_TRANSACTION_LIST_REQUEST:
      return { loading: true };
    case GET_SUPPLIER_TRANSACTION_LIST_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_SUPPLIER_TRANSACTION_LIST_FAIL:
      return { loading: false, error: action.payload };
    case GET_SUPPLIER_TRANSACTION_LIST_RESET:
      return {};
    default:
      return state;
  }
}

export function getSellReturnTransactionListReducer(state = {}, action) {
  switch (action.type) {
    case GET_SELL_RETURN_TRANSACTION_LIST_REQUEST:
      return { loading: true };
    case GET_SELL_RETURN_TRANSACTION_LIST_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_SELL_RETURN_TRANSACTION_LIST_FAIL:
      return { loading: false, error: action.payload };
    case GET_SELL_RETURN_TRANSACTION_LIST_RESET:
      return {};
    default:
      return state;
  }
}

export function getCustomerSellReturnTransactionListReducer(state = {}, action) {
  switch (action.type) {
    case GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_REQUEST:
      return { loading: true };
    case GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_SUCCESS:
      return {
        loading: false,
        success: true,
        data: action.payload
      };
    case GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_FAIL:
      return { loading: false, error: action.payload };
    case GET_CUSTOMER_SELL_RETURN_TRANSACTION_LIST_RESET:
      return {};
    default:
      return state;
  }
}

import {
  CREATE_NEW_USER_FAIL,
  CREATE_NEW_USER_REQUEST,
  CREATE_NEW_USER_RESET,
  CREATE_NEW_USER_SUCCESS,
  DELETE_USER_FAIL,
  DELETE_USER_REQUEST,
  DELETE_USER_RESET,
  DELETE_USER_SUCCESS,
  GET_USERS_FAIL,
  GET_USERS_REQUEST,
  GET_USERS_RESET,
  GET_USERS_SUCCESS,
  SIGN_IN_USER_FAIL,
  SIGN_IN_USER_REQUEST,
  SIGN_IN_USER_RESET,
  SIGN_IN_USER_SUCCESS,
  UPDATE_USER_PROFILE_FAIL,
  UPDATE_USER_PROFILE_REQUEST,
  UPDATE_USER_PROFILE_RESET,
  UPDATE_USER_PROFILE_SUCCESS,
  UPDATE_USER_FAIL,
  UPDATE_USER_REQUEST,
  UPDATE_USER_RESET,
  UPDATE_USER_SUCCESS,
  USER_UPDATE_PASSWORD_FAIL,
  USER_UPDATE_PASSWORD_REQUEST,
  USER_UPDATE_PASSWORD_RESET,
  USER_UPDATE_PASSWORD_SUCCESS,
  SEND_EMAIL_TO_USER_REQUEST,
  SEND_EMAIL_TO_USER_SUCCESS,
  SEND_EMAIL_TO_USER_FAIL,
  SEND_EMAIL_TO_USER_RESET
} from '../constants/userConstants';

export function updateUserProfileReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_USER_PROFILE_REQUEST:
      return { loading: true };
    case UPDATE_USER_PROFILE_SUCCESS:
      return {
        loading: false,
        success: true,
        user: action.payload
      };
    case UPDATE_USER_PROFILE_FAIL:
      return { loading: false, error: action.payload };
    case UPDATE_USER_PROFILE_RESET:
      return {};
    default:
      return state;
  }
}

export function UserSignInReducer(state = {}, action) {
  switch (action.type) {
    case SIGN_IN_USER_REQUEST:
      return { loading: true };
    case SIGN_IN_USER_SUCCESS:
      return {
        loading: false,
        success: true,
        userInfo: action.payload
      };
    case SIGN_IN_USER_FAIL:
      return { loading: false, error: action.payload };
    case SIGN_IN_USER_RESET:
      return {};
    default:
      return state;
  }
}

export function createNewUserReducer(state = {}, action) {
  switch (action.type) {
    case CREATE_NEW_USER_REQUEST:
      return { loading: true };
    case CREATE_NEW_USER_SUCCESS:
      return {
        loading: false,
        success: true,
        user: action.payload
      };
    case CREATE_NEW_USER_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_NEW_USER_RESET:
      return {};
    default:
      return state;
  }
}

export function updateUserReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_USER_REQUEST:
      return { loading: true };
    case UPDATE_USER_SUCCESS:
      return {
        loading: false,
        success: true,
        user: action.payload
      };
    case UPDATE_USER_FAIL:
      return { loading: false, error: action.payload };
    case UPDATE_USER_RESET:
      return {};
    default:
      return state;
  }
}

export function deleteUserReducer(state = {}, action) {
  switch (action.type) {
    case DELETE_USER_REQUEST:
      return { loading: true };
    case DELETE_USER_SUCCESS:
      return {
        loading: false,
        success: true,
        user: action.payload
      };
    case DELETE_USER_FAIL:
      return { loading: false, error: action.payload };
    case DELETE_USER_RESET:
      return {};
    default:
      return state;
  }
}

export function getAllUserReducer(state = {}, action) {
  switch (action.type) {
    case GET_USERS_REQUEST:
      return { loading: true };
    case GET_USERS_SUCCESS:
      return {
        loading: false,
        Users: action.payload
      };
    case GET_USERS_FAIL:
      return { loading: false, error: action.payload };
    case GET_USERS_RESET:
      return {};
    default:
      return state;
  }
}

export function userUpdatePasswordReducer(state = {}, action) {
  switch (action.type) {
    case USER_UPDATE_PASSWORD_REQUEST:
      return { loading: true };
    case USER_UPDATE_PASSWORD_SUCCESS:
      return { loading: false, success: true, data: action.payload };
    case USER_UPDATE_PASSWORD_FAIL:
      return { loading: false, error: action.payload };
    case USER_UPDATE_PASSWORD_RESET:
      return {};
    default:
      return state;
  }
}

export function sendEmailReducer(state = {}, action) {
  switch (action.type) {
    case SEND_EMAIL_TO_USER_REQUEST:
      return { loading: true };
    case SEND_EMAIL_TO_USER_SUCCESS:
      return { loading: false, message: action.payload, success: true };
    case SEND_EMAIL_TO_USER_FAIL:
      return { loading: false, error: action.payload };
    case SEND_EMAIL_TO_USER_RESET:
      return {};
    default:
      return state;
  }
}

export function changePasswordReducer(state = {}, action) {
  switch (action.type) {
    case USER_UPDATE_PASSWORD_REQUEST:
      return { loading: true };
    case USER_UPDATE_PASSWORD_SUCCESS:
      return { loading: false, message: action.payload, success: true };
    case USER_UPDATE_PASSWORD_FAIL:
      return { loading: false, error: action.payload };
    case USER_UPDATE_PASSWORD_RESET:
      return {};
    default:
      return state;
  }
}

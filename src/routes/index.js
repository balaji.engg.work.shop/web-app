import { Routes, Route, BrowserRouter } from 'react-router-dom';
import {
  BankAccount,
  CreateAccount,
  Customers,
  DailyManufacturingEntry,
  Products,
  SellProducts,
  Setting,
  ShopDashBoard,
  ShopDetails,
  ShopUserList,
  SignIn,
  DashBoard,
  Transactions,
  SellProductDetails,
  ForgetPassword,
  ChangePassword,
  EstimateForm,
  SellReturn,
  GalleryPage
} from '../pages';
import { NavBar, Page404 } from '../components';
import { Box } from '@chakra-ui/react';
import {
  BANK_ACCOUNT,
  BUY_PRODUCTS,
  CUSTOMERS,
  CUSTOMERS_TRANSACTION,
  DAILY_MANUFACTURE,
  DASHBOARD,
  EDIT_SUPPLIER,
  NOT_FOUND,
  PRODUCTS,
  ROOT,
  SELL_PRODUCT_DETAILS,
  SELL_PRODUCTS,
  SETTING,
  SHOP,
  SIGN_IN,
  SIGN_UP,
  SUPPLIER,
  SUPPLIER_TRANSACTION,
  SELL_TRANSACTION,
  USERS,
  FORGET_PASSWORD,
  CHANGE_PASSWORD,
  ESTIMATE,
  SELL_RETURN,
  SELL_RETURN_PRODUCT_DETAILS,
  PURCHASE_PRODUCT_DETAILS,
  GALLERY
} from '../constants/routeConstant';
import Invoice from '../pages/Invoice';
import ExcelHandler from '../pages/ExcelHandler';
import { CustomerTransaction } from '../pages/Customers/CustomerTransaction';
import { Suppliers } from '../pages/Supplier';
import { SupplierTransaction } from '../pages/Supplier/SupplierTransaction';
import { PurchaseProductEntry } from '../pages/PurchaseProductEntry';
import { SellReturnDetails } from '../pages/SellReturnDetails';
import { PurchaseProductDetails } from '../pages/PurchaseProductDetails';
import NewSellInputForm from '../pages/SellProducts/NewSellInputForm';

export function RouterPage() {
  const path = window.location.pathname;
  return (
    <BrowserRouter>
      <Routes>
        <Route path={SIGN_IN} element={<SignIn />} />
        <Route path={FORGET_PASSWORD} element={<ForgetPassword />} />
        <Route path={CHANGE_PASSWORD} element={<ChangePassword />} />

        <Route path={SIGN_UP} element={<CreateAccount />} />
        <Route
          path={ROOT}
          element={
            <ShopDashBoard>
              <DashBoard />
            </ShopDashBoard>
          }
        />
        <Route
          path={PRODUCTS}
          element={
            <ShopDashBoard>
              <Products />
            </ShopDashBoard>
          }
        />
        <Route
          path={SELL_PRODUCTS}
          element={
            <ShopDashBoard>
              <NewSellInputForm />
            </ShopDashBoard>
          }
        />
        <Route
          path={CUSTOMERS}
          element={
            <ShopDashBoard>
              <Customers />
            </ShopDashBoard>
          }
        />
        <Route
          path={ESTIMATE}
          element={
            <ShopDashBoard>
              <EstimateForm />
            </ShopDashBoard>
          }
        />
        <Route
          path={USERS}
          element={
            <ShopDashBoard>
              <ShopUserList />
            </ShopDashBoard>
          }
        />
        <Route path={'/invoice'} element={<Invoice />} />
        <Route path={'/excel'} element={<ExcelHandler />} />
        <Route
          path={SETTING}
          element={
            <ShopDashBoard>
              <Setting />
            </ShopDashBoard>
          }
        />
        <Route
          path={SHOP}
          element={
            <ShopDashBoard>
              <ShopDetails />
            </ShopDashBoard>
          }
        />
        <Route
          path={DAILY_MANUFACTURE}
          element={
            <ShopDashBoard>
              <DailyManufacturingEntry />
            </ShopDashBoard>
          }
        />
        <Route
          path={DASHBOARD}
          element={
            <ShopDashBoard>
              <DashBoard />
            </ShopDashBoard>
          }
        />
        <Route
          path={BANK_ACCOUNT}
          element={
            <ShopDashBoard>
              <BankAccount />
            </ShopDashBoard>
          }
        />
        <Route
          path={GALLERY}
          element={
            <ShopDashBoard>
              <GalleryPage />
            </ShopDashBoard>
          }
        />
        <Route
          path={CUSTOMERS_TRANSACTION}
          element={
            <ShopDashBoard>
              <CustomerTransaction />
            </ShopDashBoard>
          }
        />
        <Route
          path={SUPPLIER}
          element={
            <ShopDashBoard>
              <Suppliers />
            </ShopDashBoard>
          }
        />
        <Route
          path={SUPPLIER_TRANSACTION}
          element={
            <ShopDashBoard>
              <SupplierTransaction />
            </ShopDashBoard>
          }
        />
        <Route
          path={BUY_PRODUCTS}
          element={
            <ShopDashBoard>
              <PurchaseProductEntry />
            </ShopDashBoard>
          }
        />
        <Route
          path={SELL_TRANSACTION}
          element={
            <ShopDashBoard>
              <Transactions />
            </ShopDashBoard>
          }
        />
        <Route
          path={SELL_PRODUCT_DETAILS}
          element={
            <ShopDashBoard>
              <SellProductDetails />
            </ShopDashBoard>
          }
        />
        <Route
          path={PURCHASE_PRODUCT_DETAILS}
          element={
            <ShopDashBoard>
              <PurchaseProductDetails />
            </ShopDashBoard>
          }
        />
        <Route
          path={SELL_RETURN_PRODUCT_DETAILS}
          element={
            <ShopDashBoard>
              <SellReturnDetails />
            </ShopDashBoard>
          }
        />
        <Route
          path={SELL_RETURN}
          element={
            <ShopDashBoard>
              <SellReturn />
            </ShopDashBoard>
          }
        />
        <Route
          path={NOT_FOUND}
          element={
            <ShopDashBoard>
              <Page404 />
            </ShopDashBoard>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

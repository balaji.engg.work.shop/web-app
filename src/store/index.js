import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import {
  createNewShopReducer,
  getShopDetailsReducer,
  updateShopDetailsReducer
} from '../reducers/shopReducers';
import {
  createNewUserReducer,
  deleteUserReducer,
  getAllUserReducer,
  UserSignInReducer,
  updateUserProfileReducer,
  updateUserReducer,
  userUpdatePasswordReducer,
  sendEmailReducer,
  changePasswordReducer
} from '../reducers/userReducers';
import {
  addDailyManufactureProductInBulkReducer,
  addProductsInBulkReducer,
  createNewProductReducer,
  deleteAllProductReducer,
  deleteProductReducer,
  getAllProductReducer,
  getDailyManufactureProductListReducer,
  getProductDetailsReducer,
  updateProductReducer
} from '../reducers/productReducers';
import {
  createNewCustomerReducer,
  customerDeleteReducer,
  getAllCustomerReducer,
  getCustomerDetailsReducer,
  updateCustomerReducer
} from '../reducers/customerReducers';
import { BALA_JI_USER } from '../constants/localStorageConstatnt';
import {
  addProductCategoryReducer,
  getProductCategoryReducer
} from '../reducers/productCategoryReducers';
import {
  createNewBankAccountReducer,
  deleteBankAccountReducer,
  getAllBankAccountReducer,
  getBankAccountDetailsReducer,
  updateBankAccountReducer
} from '../reducers/bankAccountReducers';
import {
  addSellProductsReducer,
  deleteSellProductDetailsReducer,
  getCountDataReducer,
  getSellProductDetailsReducer
} from '../reducers/sellProductsReducers';
import {
  getCustomerSellReturnTransactionListReducer,
  getCustomerTransactionListReducer,
  getPurchaseTransactionListReducer,
  getSellReturnTransactionListReducer,
  getSupplierTransactionListReducer,
  getTransactionListReducer
} from '../reducers/transactionReducers';
import {
  createNewSupplierReducer,
  getAllSupplierReducer,
  getSupplierDetailsReducer,
  updateSupplierReducer
} from '../reducers/supplierReducer';
import {
  addPurchaseProductsReducer,
  getPurchaseProductsDetailsReducer
} from '../reducers/purchaseProductReducers';
import {
  createNewSellReturnReducer,
  sellReturnDetailsReducer
} from '../reducers/sellReturnReducers';
import {
  createNewGalleryReducer,
  getAllGalleryReducer,
  getGalleryCategoryReducer,
  getGalleryListByShopIdReducer,
  updateGalleryReducer
} from '../reducers/galleryReducers';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const initialState = {
  userSignIn: {
    userInfo: localStorage.getItem(BALA_JI_USER)
      ? JSON.parse(localStorage.getItem(BALA_JI_USER))
      : null
  }
};

const reducer = combineReducers({
  //shop
  addShop: createNewShopReducer,
  shopDetail: getShopDetailsReducer,
  updateShop: updateShopDetailsReducer,

  //user
  userSignIn: UserSignInReducer,
  getUsers: getAllUserReducer,
  createUser: createNewUserReducer,
  updateUser: updateUserReducer,
  deleteUser: deleteUserReducer,
  profileUpdate: updateUserProfileReducer,
  userUpdatePassword: userUpdatePasswordReducer,
  sendEmail: sendEmailReducer,
  passwordChange: changePasswordReducer,

  //product
  createProduct: createNewProductReducer,
  productDelete: deleteProductReducer,
  allProductDelete: deleteAllProductReducer,
  productUpdate: updateProductReducer,
  productList: getAllProductReducer,
  productDetails: getProductDetailsReducer,
  addProducts: addProductsInBulkReducer,
  getDailyManufactureProducts: getDailyManufactureProductListReducer,
  addDailyManufactureProducts: addDailyManufactureProductInBulkReducer,

  //customer
  createCustomer: createNewCustomerReducer,
  customerUpdate: updateCustomerReducer,
  customerList: getAllCustomerReducer,
  customerDetails: getCustomerDetailsReducer,
  customerDelete: customerDeleteReducer,

  // PRODUCT CATEGORY
  getProductCategory: getProductCategoryReducer,
  addProductCategory: addProductCategoryReducer,

  //bank account
  createBankAccount: createNewBankAccountReducer,
  bankAccountDelete: deleteBankAccountReducer,
  bankAccountUpdate: updateBankAccountReducer,
  bankAccountList: getAllBankAccountReducer,
  bankAccountDetail: getBankAccountDetailsReducer,

  // gallery
  createNewGalleryReducer,
  updateGalleryReducer,
  getAllGalleryReducer,
  getGalleryListByShopIdReducer,
  getGalleryCategoryReducer,

  // sell product reducers
  sellProducts: addSellProductsReducer,
  getCount: getCountDataReducer,
  sellProductDetails: getSellProductDetailsReducer,
  deleteSellProduct: deleteSellProductDetailsReducer,

  // purchase product
  purchaseProducts: addPurchaseProductsReducer,
  purchaseProductsDetails: getPurchaseProductsDetailsReducer,

  // Transaction
  transactionList: getTransactionListReducer,
  customerTransactionList: getCustomerTransactionListReducer,
  supplierTransactionList: getSupplierTransactionListReducer,
  getPurchaseTransaction: getPurchaseTransactionListReducer,
  sellReturnTransaction: getSellReturnTransactionListReducer,
  customerSellReturnTransaction: getCustomerSellReturnTransactionListReducer,

  // supplier
  createSupplier: createNewSupplierReducer,
  supplierUpdate: updateSupplierReducer,
  supplierList: getAllSupplierReducer,
  supplierDetails: getSupplierDetailsReducer,

  // SET RETURN
  createNewSellReturn: createNewSellReturnReducer,
  sellReturnDetails: sellReturnDetailsReducer
});
const store = createStore(reducer, initialState, composeEnhancer(applyMiddleware(thunk)));

export default store;

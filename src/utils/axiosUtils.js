export const getAuthorization = (token) => {
  return {
    headers: {
      Authorization: 'Bearer ' + token
    }
  };
};

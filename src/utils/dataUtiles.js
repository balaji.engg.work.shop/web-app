export const productUnits = [
  {
    label: 'Kg',
    value: 'Kg'
  },
  {
    label: 'Tonne',
    value: 'Tonne'
  },
  {
    label: 'Piece',
    value: 'piece'
  },
  {
    label: 'Dozen',
    value: 'dozen'
  },
  {
    label: 'Square feet',
    value: 'square feet'
  },
  {
    label: 'Running feet',
    value: 'running feet'
  },
  {
    label: 'Packet',
    value: 'packet'
  },
  {
    label: 'Gram',
    value: 'gram'
  },
  {
    label: 'Litter',
    value: 'litter'
  },
  {
    label: 'Mile letter',
    value: 'ml'
  }
];

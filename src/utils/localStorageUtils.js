export const addValueOnLocalStorage = (key, value) => {
  localStorage.setItem(key, JSON.stringify(value));
};

export const getValueFromLocalStorage = (key) => {
  const data = localStorage.getItem(key);
  if (data != undefined || data != null) {
    return JSON.parse(data);
  } else {
    return null;
  }
};

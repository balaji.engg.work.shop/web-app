export const processData = (data) => {
  const salesByDate = data.reduce((acc, entry) => {
    const date = entry.date.split('T')[0]; // Extract the date part
    if (!acc[date]) {
      acc[date] = 0;
    }
    acc[date] += entry.amount;
    return acc;
  }, {});

  const labels = Object.keys(salesByDate);
  const amounts = labels.map((date) => salesByDate[date]);

  return { labels, amounts };
};

export const getStock = (data) => {
  if (data) {
    const labels = data.map((singleData) => singleData.description);
    const quantity = data.map((singleData) => singleData.quantity);
    return { labels, quantity };
  } else {
    return { labels: [], quantity: [] };
  }
};

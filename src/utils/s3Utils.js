import axios from 'axios';

const apiClient = process.env.REACT_APP_BLINDS_SERVER;

export async function getSignedUrl({ key, content_type }) {
  const response = await axios.post(`${apiClient}/api/v1/s3/signed_url`, {
    key,
    content_type
  });
  return response.data;
}

export async function uploadFileToSignedUrl(signedUrl, file, contentType, onProgress) {
  try {
    const response = await axios.put(signedUrl, file, {
      onUploadProgress: onProgress,
      headers: {
        'Content-Type': contentType
      }
    });
    return 'success';
  } catch (error) {
    console.log('Error : ', error);
    throw new Error(error);
  }
}

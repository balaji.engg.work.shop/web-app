export const convertNumberToIndianRs = (number) => {
  let value = Number(number);
  return value.toLocaleString('en-IN', {
    maximumFractionDigits: 2,
    currency: 'INR'
  });
};

export const convertNumberToIndian = (number) => {
  let value = Number(number);
  return value.toLocaleString('en-IN', {
    maximumFractionDigits: 2,
    currency: 'INR',
    style: 'currency'
  });
};

var a = [
  '',
  'one ',
  'two ',
  'three ',
  'four ',
  'five ',
  'six ',
  'seven ',
  'eight ',
  'nine ',
  'ten ',
  'eleven ',
  'twelve ',
  'thirteen ',
  'fourteen ',
  'fifteen ',
  'sixteen ',
  'seventeen ',
  'eighteen ',
  'nineteen '
];
var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

export function inWords(num) {
  let num1 = Math.floor(num);
  if ((num1 = num1.toString()).length > 9) return 'overflow';
  let n = ('000000000' + num1).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
  if (!n) return;
  var str = '';
  str += n[1] != 0 ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
  str += n[2] != 0 ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
  str += n[3] != 0 ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
  str += n[4] != 0 ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
  str +=
    n[5] != 0
      ? (str != '' ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only '
      : '';
  return str?.toUpperCase();
}

export const isValidGST = (value) => {
  const gstINPattern = /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/;
  if (!value || gstINPattern.test(value)) {
    return Promise.resolve();
  } else {
    return Promise.reject('Invalid GST IN Number');
  }
};
export const isValidMobileNo = (value) => {
  if (!value || /^\d+$/.test(value)) {
    return Promise.resolve();
  }
  return Promise.reject(new Error('The input is not a valid number!'));
};
export const isValidEmail = (value) => {
  const emailPattern =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (!value || emailPattern.test(value)) {
    return Promise.resolve();
  } else {
    return Promise.reject('Invalid Email format');
  }
};

export const isValidPinCode = (value) => {
  const pinCode = /^[1-9][0-9]{5}$/;
  if (!value || pinCode.test(value)) {
    return Promise.resolve();
  } else {
    return Promise.reject('Invalid pinCode');
  }
};

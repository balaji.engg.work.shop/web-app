import { Web3Storage } from 'web3.storage';

function getAccessToken() {
  return process.env.REACT_APP_WEB3_STORAGE_API_TOKEN;
}

function makeStorageClient() {
  return new Web3Storage({ token: getAccessToken() });
}
// function jsonFile(filename, obj) {
//   return new File([JSON.stringify(obj)], filename);
// }
// function makeGatewayURL(cid, path) {
//   return `https://${cid}.ipfs.dweb.link/${encodeURIComponent(path)}`;
// }

export const addFileOnWeb3 = async (file) => {
  const startTime = performance.now();
  try {
    console.log('storeFiles file : ', file);
    const client = makeStorageClient();
    const cid = await client.put(file);
    console.log('response cid : ', cid);
    let res = await client.get(cid);
    console.log('response : ', res);
    const files = (await res?.files()) || []; // Web3File[]
    console.log('files', files);
    for (const file of files) {
      console.log(`${file.cid} ${file.name} ${file.size}`);
      console.log('time taken to upload file in ms ', performance.now() - startTime);
      return file.cid;
    }
  } catch (error) {
    console.log('Error in uploading file : ', error);
    console.log('time taken to upload file in ms ', performance.now() - startTime);
    throw new Error(error);
  }
};
